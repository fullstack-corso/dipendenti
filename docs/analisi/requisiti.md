# Salus

## La sorveglianza sanitaria
La sorveglianza sanitaria è l'insieme di attività di prevenzione e controllo che vengono effettuate per verificare lo stato di salute dei lavoratori, in relazione al tipo di attività che svolgono e ai rischi a cui sono esposti sul luogo di lavoro.

In Italia, la sorveglianza sanitaria è obbligatoria per tutti i lavoratori, sia dipendenti che autonomi, che svolgono attività che possono comportare rischi per la salute. Questi rischi possono essere di diverso tipo, come ad esempio quelli legati all'esposizione a sostanze chimiche, rumore, vibrazioni, radiazioni ionizzanti o non ionizzanti, agenti biologici, stress lavoro-correlato, movimentazione manuale dei carichi e così via.

La sorveglianza sanitaria è prevista dal Decreto Legislativo n. 81 del 2008, che disciplina la tutela della salute e della sicurezza nei luoghi di lavoro. Secondo questo decreto, l'obbligo di sorveglianza sanitaria spetta al datore di lavoro, che deve garantire ai lavoratori l'accesso a visite mediche periodiche e a controlli specifici in relazione ai rischi presenti sul luogo di lavoro.

In generale, la sorveglianza sanitaria deve essere effettuata prima dell'assunzione, durante l'attività lavorativa e alla fine del rapporto di lavoro. Inoltre, è necessario effettuare controlli specifici in caso di cambiamento dell'attività lavorativa o dell'ambiente di lavoro, nonché in caso di presenza di patologie o di gravidanza.

In conclusione, la sorveglianza sanitaria è obbligatoria per tutti i lavoratori che svolgono attività che comportano rischi per la salute, e il datore di lavoro è tenuto a garantirne l'effettuazione. La sorveglianza sanitaria rappresenta uno strumento fondamentale per prevenire malattie professionali e tutelare la salute dei lavoratori.

### Scadenze e Periodicità
Le scadenze e le periodicità per la sorveglianza sanitaria dipendono dai rischi a cui i lavoratori sono esposti e dalle attività svolte sul luogo di lavoro. In generale, esistono diverse tipologie di controlli che devono essere effettuati con periodicità variabile.

Ad esempio, per le attività lavorative che comportano rischi di esposizione a sostanze chimiche, è necessario effettuare una visita medica prima dell'assunzione e poi con una frequenza che varia da 1 a 3 anni a seconda del tipo di rischio. Per le attività lavorative che comportano rischi di esposizione a radiazioni ionizzanti, è necessario effettuare una visita medica annuale, mentre per le attività che comportano rischi di esposizione a rumore, è necessario effettuare una visita medica annuale e un controllo audiometrico ogni 3 anni.

In generale, la sorveglianza sanitaria deve essere effettuata anche in caso di cambiamento dell'attività lavorativa o dell'ambiente di lavoro, nonché in caso di presenza di patologie o di gravidanza. In questi casi, il datore di lavoro è tenuto ad effettuare i controlli specifici necessari per garantire la tutela della salute dei lavoratori.

In conclusione, le scadenze e le periodicità per la sorveglianza sanitaria dipendono dalle attività svolte sul luogo di lavoro e dai rischi a cui i lavoratori sono esposti. Il datore di lavoro è tenuto a garantire l'effettuazione dei controlli necessari per la tutela della salute dei lavoratori, rispettando le periodicità previste dalla normativa.

### Operatore Videoterminalista
l'operatore videoterminalista è definito come "un lavoratore che utilizza per almeno tre ore al giorno il videoterminale, inteso come apparecchio dotato di schermo alfanumerico o grafico". Tuttavia, esistono alcune casistiche in cui non è considerato operatore videoterminalista, ad esempio:

- Lavoratori che utilizzano il videoterminale per meno di tre ore al giorno;
- Lavoratori che utilizzano il videoterminale solo sporadicamente e non in modo continuativo;
- Lavoratori che utilizzano il videoterminale solo per scopi accessori e non come attività principale;
- Lavoratori che utilizzano il videoterminale solo per la consultazione di dati o la lettura di documenti brevi e semplici.
In ogni caso, è importante tenere presente che l'utilizzo prolungato del videoterminale può comportare alcuni rischi per la salute dei lavoratori, come ad esempio la sindrome da stress lavoro-correlato, disturbi muscolo-scheletrici, affaticamento visivo e disturbi del sonno. Pertanto, è sempre consigliabile adottare misure di prevenzione e protezione, come la riduzione dei tempi di utilizzo del videoterminale, la corretta ergonomia della postazione di lavoro e la formazione dei lavoratori sui rischi correlati all'utilizzo del videoterminale.

## Il Processo di sorveglianza sanitaria in Azienda
La Azienda ha tra gli adempimenti di competenza la sorveglianza sanitaria sul personale dell’Azienda e delle persone somministrate che prestano servizio presso l’Azienda.
Per le direzioni regionali la sorveglianza è delegata ad un referente di SPP del posto incaricato dal Responsabile della Sicurezza che organizza e gestisce autonomamente la scelta del medico competente e tutti gli adempimenti connessi alla normativa.
In Direzione Generale monitora e controlla il processo delle regionali.
Per quanto concerne la sorveglianza sanitaria ogni struttura è obbligata ad organizzare le visite mediche e gestire i certificati a seconda dei dettami legistlativi; monitora quindi le scadenze che la normativa impone rispetto le quali si richiede che vengano effettuati gli adempimnenti.
La scadenza impone l’obbligo di effettuare la visita entro il mese di scadenza, anche se rientra nella lista solo un dipendente. 

## La Gestione del processo
L’entrata in Azienda della risorsa dipendente o esterna, viene comunicata alla struttura, attraverso un’email che contiene nominativo, data di ingresso in Azienda, struttura di assegnazione se dipendente o destinazione in caso di risorsa esterna.
Le informazioni aggiuntive che vengono aggiunte dalla struttura sono tipo contratto determinato o indeterminato (in particolare se è apprendista), il titolo di studio, la data di assunzione, la struttura di appartenenza in Direzione Centrale, Direzione, Direzione Regionale, UO (CDR) e Ubicazione (ufficio).

Per ciascuna persona (sia interna, sia esterna) la struttura deve provvedere, entro 60 giorni dalla data di assunzione, ad effettuare la vistita medica (nell’ambito della sorveglianza sanitaria).

### Dirigente non videoterminalista
L’adempimento legislativo nei confronti dei dirigenti si differenzia dal personale dipendente;
se il dirigente dichiara di stare meno di 20 ore settimanali al video terminale per la struttura SSP non scatta l’obbligo di sorveglianza sanitaria (rilascio dichiarazione scritta).

## Periodicità della visita medica
A far data dell’assunzione del dipendente in Azienda od ingresso della risorsa somministrata le scadenze degli adempimenti sono biennali, se l’età della risorsa supera i 50 anni o quinquennali se l’età è inferiori ai 50 anni.
Dette scadenze però variano se subentrano altre condizioni:
 - Il ruolo che ricopre il dipendente se amministrativo o tecnico determina differenzazione e la durata della scadenza della visita  da effettuare.
 - ove c’è un’assenza prolungata che supera i 60 gg (prima del rientro è obbligatoria la visita) o in caso di infortunio (tale segnalazione viene fornita da Paghe e contributi)
 - prescrizione medica derivante da una visita medica sul lavoro che ne detta una scadenza diversa che va annotata.


### Casi particolari
### Precedente assunzione
Per quanto concerne le risorse a tempo determinato e gli interinali la struttura deve verificare, nel caso in cui queste siano state precedentemente presenti in Azienda, se per loro sono stati già gestiti gli eventi visita medica e formazione.
La risorsa interinale ha la stessa sorveglianza sanitaria del determinato.
L’obbligo per gli interinali o a tempo determinato è a decorrere dalla prima data di immissione in Azienda. Ciò significa che deve far fede la prima data di immissione per determinare la scadenza della prossima visita sia se ci sono interruzioni per proroga o chiamata su nuovo progetto/commessa sia se il somministrato viene assunto in Azienda.
La data di primo ingresso in Azienda è da considerarsi la data in cui la risorsa ha fatto ingresso la prima volta in Azienda e da cui parte il conteggio della scadenza delle visite mediche sia questa come Somministrato e poi diventato dipendente; sia se è entrato come dipendente a tempo Indeterminato; sia come dipendente a tempo determinato.
Nel caso in cui il somministrato ha prestato servizio in più contratti la data primo Ingresso in Azienda è la data del primo contratto; dal secondo contratto il somministrato avrà una data di primo Ingresso in Azienda (che risale al primo contratto) ed inoltre una data inzio e fine contratto di somministrazione (più la commessa);
Nel caso in cui il suddetto somministrato è diventato dipendente dell’Azienda la risorsa avrà una data prima ingresso in Azienda (che risale al primo contratto) e una data assunzione nel’Azienda come dipendente.
Si avrà il seguente schema:
- I° contratto somministrato 1-gen-2011- 01/06/2011
- II° contratto 01/06/2011 – 31/12/2011
- Assunzione 03/02/2012

### Distacco
Il distacco sospende la scadenza della visita nell’Azienda. Se nell’arco di tempo in cui il dipendente è in distacco effettua la visita presso la PA di desrtinazione la PA dovrebbe inviare prescrizione all’Azienda che a sua volta aggiornerà l’anagrafica delle visite.
Al rientro del distacco la cartella verrà reinviata all’Azienda. 
Quando il dipendente rientra dal distacco viene riavviato il contatore della scadenza.

### Maternità e Assenza di lunga durata
Durante l’attività quotidiana l’addetto di SPP gestisce le comunicazioni di assenze di lunga durata, le maternità, i congedi parentali che arrivano da Gestione Paghe e contributi affinchè sospendino temporaneamente la scadenza delle visite.
Tali casistiche sospendono l’obbligo di SPP di adempiere al controlo sanitario  fino a data del rientro. 
Pertanto è necessario che arrivi l’informativa all’addetto che escluda dalle convocazioni.
Le informazioni dovranno essere gestite manualmente da utente.
Dall’anagrafica del dipendente devono essere inserite: 
 - Causale di certificazione
 - Data inizio certificato
 - Data fine certificato
 - Causale
  
Le causali da considerare sono:
 - MATERNITA ANTICIPATA
 - INFORTUNIO
 - MALATTIA
 - MATERNITA FACOLTATIVA
 - TERAPIA SALVAVITA
 - MATERNITA OBBLIGATORIA
 - CONGEDO

## Fascicolo Sanitario
Ciascun dipendente ha a disposizione un proprio “fascicolo sanitario” composto da tutti i certificati di idoneità e prescrizioni mediche e la cartella sanitaria: 
Quest’ultima segue la vita del dipendente e viene conservata dall’addetto SPP in busta chiusa da consegnare al dipendente in fase di cessazione.
La cartella clinica quindi è conservata in maniera cartacea dall’addetto di SPP che si preoccupa di gestirla o di trasferirla di competenza ad un nuovo addetto SPP in caso di mobilità da una strutttura ad un’altra interna all’Azienda.

### Il Medical Day
Il medical day è la giornata in cui il medico di sede effettua la visita medica.
I dipendenti che rientrano nel range di scadenza della visita vengono proposti dal sistema all’addetto della sicurezza per la programmazione della giornata delle visite (medical day).
Ciascun dipendente potrà accedere all’applicazione per prenotare l’orario della visita; altrimenti può intervenire direttamente l’addetto SPP per la prenotazione e completare tutte le prenotazioni

### Anagrafica medici
Ci può essere il caso in cui il contratto stipulato sia a favore di una società e non direttamente con un medico; 
in tal caso ci può essere l’eventualità che i medici siano diversi nell’ambito della scdenza contrattuale. 
Va considerata la data nomina del medico.

### Processo di gestione delle visite
Almeno 2 mesi prima l’addetto predispone le attività di programmazione della visita medica.
Consulta le scadenze delle visite nelle sedi di competenza del mese di ossevazione; considera 25-30 persone al giorno; raccoglie disponibiltà del medico e le date a seconda delle giornate del medical day da programmare in funzione delle persone che dovranno eseguire la visita medica.
Almeno due settimane prima del medical Day deve essere inviata notifica ai dipendenti di convocazione.
Il dipendente riceve direttamente dal sistema la convocazione ad effettuare la visita, in alternativa l’addetto invia comunicazione per conoscenza alla segreteria di direzione.
A seguito della visita il dipendente firma il foglio della presenza.
Il giudizio all’idoneità e se ci sono prescrizioni mediche rilasciato dal medico viene notificata al responsabile (ad esempio fare una pausa di 10 miniuti dal terminale).
Il certificato di idoneità viene inviato al capo della risorsa e a gestione del personale.

### Report: Proposta elenco dipendenti in scadenza del Medical Day
Per la creazione di un medical day è necessario selezionare la data in cui le visite verranno effettuate.
Una volta selezionata la data il sistema dovrà proporre in elenco prioritariamente i dipendenti che hanno una scadenza passata, più tutti i dipendneti che hanno una scadenza nel mese della visita.

### Dashboard: Cruscotto dell’addetto SPPdi DG e DR lazio e delle DD RR
Ciascun addetto lavora per competenza territoriale.
    1) La necessità di avere almeno due mesi prima le scadenze per mese in dettaglio elenco dei dipendenti in scadenza 
    2) Trend delle scadenze nell’anno (grafico e dettagli)
    3) Messaggio di notifica scadenza contratto medico (almeno 20 giorni prima)
