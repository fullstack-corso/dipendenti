Certamente, ecco una versione migliorata:

# Salus - Software per la gestione della sorveglianza sanitaria obbligatoria

## Introduzione
La sorveglianza sanitaria è un insieme di attività preventive e di controllo che devono essere effettuate per verificare lo stato di salute dei lavoratori, in relazione al tipo di attività svolte e ai rischi a cui sono esposti sul luogo di lavoro. In Italia, la sorveglianza sanitaria è obbligatoria per tutti i lavoratori, sia dipendenti che autonomi, che svolgono attività che possono comportare rischi per la salute. 

Per garantire la corretta esecuzione delle visite mediche e dei controlli specifici in relazione ai rischi presenti sul luogo di lavoro, è fondamentale disporre di un software dedicato alla gestione della sorveglianza sanitaria obbligatoria, come Salus.

## Funzionalità di Salus
Salus è un software intuitivo ed efficiente che consente di gestire in modo completo e preciso tutti gli aspetti della sorveglianza sanitaria obbligatoria, comprese le scadenze e le periodicità dei controlli.

Grazie alla sua interfaccia semplice ed intuitiva, Salus consente di registrare e monitorare i dati relativi ai lavoratori, ai rischi presenti sul luogo di lavoro e alle visite mediche effettuate, garantendo una visione d'insieme chiara e immediata della situazione.

In particolare, Salus permette di:
- Registrare i dati anagrafici dei lavoratori e assegnare loro i rischi presenti sul luogo di lavoro;
- Programmare le visite mediche periodiche in base alle scadenze e alle periodicità previste dalla normativa;
- Tenere traccia delle visite mediche effettuate e dei relativi esiti;
- Creare report personalizzati per monitorare la situazione generale della sorveglianza sanitaria obbligatoria in azienda;
- Ricevere notifiche automatiche per le visite mediche in scadenza, per garantire la massima tempestività nell'esecuzione dei controlli.

## Vantaggi di Salus
Salus consente di semplificare e ottimizzare la gestione della sorveglianza sanitaria obbligatoria in azienda, garantendo la massima efficienza e tempestività nell'esecuzione dei controlli.

In particolare, Salus offre i seguenti vantaggi:
- Consente di rispettare gli obblighi previsti dalla normativa in materia di sorveglianza sanitaria obbligatoria;
- Riduce al minimo i rischi di errori e di omissioni nella programmazione e nella gestione delle visite mediche periodiche;
- Garantisce una visione d'insieme chiara e immediata della situazione generale della sorveglianza sanitaria obbligatoria in azienda;
- Ottimizza l'organizzazione del lavoro, semplificando la gestione dei dati e riducendo i tempi di lavoro.

In conclusione, Salus rappresenta uno strumento indispensabile per garantire la tutela della salute dei lavoratori e la corretta esecuzione della sorveglianza sanitaria obbligatoria in azienda.


## Medical Day
è possibile organizzare la visita medica; programmare le visite secondo le scadenze proposte; convocare dipendente alla visita; gestire il post medical day da parte del ASPP di DR e di DRUO-SPP (certificato idoneità, eventuale prescrizione e prossima scadenza visita)

### Fasi e Stati
Un medical day si struttura in 4 fasi distinte: 
 - Proposta di pianificazione;
 - Prenotazione dell’orario 
 - Gestione del post visita 
 - Completamento lavorazione del medical day da parte dell’addetto ASPP (certificato idoneità e/o prescrizione)

### Proposta di pianificazione
Selezionare la provincia dopodiché il medico. 
Inserire la data della visita E’ possibile inserire singoli orario (esempio 09:00*; 09:15, ecc) e poi premere Invio. 
Il sistema si predispone secondo gli slot disponibili proponendo i dipendenti da Medicalday-Propostadi pianificazione disponibili proponendo i dipendenti da convocare alla visita. Gestire la pianificazione delle visite secondo le scadenze proposte. Con il pulsante CREA si crea il medical day.
`NOTA*: è possibile impostare  l’orario scrivendo semplicemente 09 e premere invio il sistema creerà automaticamente l’orario  09:00`
In alternativa è possibile impostare una fascia oraria attraverso l’opzione “Oppure scegli una intera fascia oraria”. Inserire l’orario di inizio e fine della giornata e impostare la durata media della visita; alla selezione del pulsante Pianifica tutti gli slotil sistema si predispone secondo DSP-ICT e DRUO-SPPSorveglianza Sanitaria8 gli slotil sistema si predispone secondo gli slot disponibili proponendo i dipendenti da convocare alla visita. Gestire la pianificazione delle risorse e poi le visite secondo le scadenze proposte. Con il pulsante CREA si crea il medical day.
Questa fase crea il Medical day che sarà nello nello stato: `IN PRENOTAZIONE`

### Prenotazione dell’orario
Sarà necessario prenotare gli orari della visita per ciascuna risorsa in elenco per la convocazione. 
Per la prenotazione è necessario selezionare l’icona Per il giorno è possibile stampare il Foglio firme da fornire al medico competente con l’elenco delle risorse convocate.
Non appena viene prenotata la prima visita il Medical day passa allo stato: `IN LAVORAZIONE`


### Gestione del post visita
Gestire il post medical day da parte del ASPP di DR e di DRUO-SPP inserendo se la visita è stata effettuata o non

### Completamento lavorazione
Gestire il post medical day da parte del ASPP di DR e di DRUO-SPP Caricare il certificato di idoneità selezionando l’icona  Eventualmente inserire una prescrizione e una data di prossima scadenza visita qualora sia prescritta dal medico
Quanto tute le visite mediche avranno terminato il loro iter, allegando il certificato di idoneità oppure dichiarando la visita "Non Effettuata" il Medical day passa allo stato: `COMPLETO`

### Annullamento del medical day
E’ possibile gestire l’annullamento del medical day in caso si sia erroneamente inserita una data non corretta della visita da parte del ASPP di DR e di DRUO-SPP. In tal caso solo se la visita è in stato eseguita e dopo aver preventivamente eliminato l’eventuale certificato di idoneità e prescrizione effettuare Annulla visita selezionando l’icona 
