package it.fullstack.salus.domain;

import static it.fullstack.salus.services.mapper.DipendenteMapper.toUpdateDTO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.fullstack.salus.Application;
import it.fullstack.salus.domain.enumeration.TipologiaEvento;
import it.fullstack.salus.repository.DipendenteRepository;
import it.fullstack.salus.repository.EventoRepository;
import it.fullstack.salus.repository.MedicalDayRepository;
import it.fullstack.salus.services.DipendenteService;
import it.fullstack.salus.services.VisitaMedicaService;
import it.fullstack.salus.services.dto.UpdateDipendenteDTO;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaDTO;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = Application.class)
@AutoConfigureMockMvc
public class EventoControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private EventoRepository repositoryEvento;

	@Autowired
	private DipendenteRepository repositoryDipendente;	

	@Autowired
	private DipendenteService dipendenteService;	

	@Autowired
	private VisitaMedicaService visitaMedicaService;	

	@Autowired
	private MedicalDayRepository medicalDayRepository;	
	

	@DirtiesContext
	@Test
	public void testFindAllByCriteriaData_Status_200()
			throws Exception, JsonProcessingException
	{
		testFindAllByCriteria("dataDal=2020-05-29T10:30:00")
		.andExpect(jsonPath("$.content",hasSize(2)));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_Cognome_Evento_Status200()
			throws Exception, JsonProcessingException
	{
		testFindAllByCriteria("cognome=VER")
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].dipendente.cognome",is("Verdi")));
	} //TODO perché non funziona?

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_Evento_Evento_Status200()
			throws Exception, JsonProcessingException
	{
		testFindAllByCriteria("nomeEvento=SCADENZA_VISITA_MODIFICATA")
		.andExpect(jsonPath("$.content",hasSize(2)))
		.andExpect(jsonPath("$.content[0].tipologiaEvento", is("SCADENZA_VISITA_MODIFICATA")))
		.andExpect(jsonPath("$.content[1].tipologiaEvento", is("SCADENZA_VISITA_MODIFICATA")));
	}

	@DirtiesContext
	@Test
	public void testFindAll_Status200()
			throws Exception, JsonProcessingException
	{
		testFindAllByCriteria("")
		.andExpect(jsonPath("$.content",hasSize(2)));
	}


	@DirtiesContext
	@Test
	public void testEventoModifica_Scadenza_Dipendente()
			throws Exception, JsonProcessingException
	{
		Dipendente dipedente=createDipendenti();

		Long id=dipedente.getId();
		int intId=Long.valueOf(id).intValue();

		dipedente.setScadenzaVisita(LocalDate.now().plusYears(2));

		UpdateDipendenteDTO dipDTO=toUpdateDTO(dipedente);

		dipendenteService.update(id, dipDTO);
		List<Evento> evento= repositoryEvento.findAll();

		assertThat(evento.size()).isEqualTo(1);
		assertThat(evento.get(0).getTipologiaEvento()).isEqualTo(TipologiaEvento.SCADENZA_VISITA_MODIFICATA);
		assertThat(evento.get(0).getDipendente().getId()).isEqualTo(intId);
		assertThat(evento.get(0).getDipendente().getCodiceFiscale()).isEqualTo(dipedente.getCodiceFiscale());
		assertThat(evento.get(0).getDipendente().getCognome()).isEqualTo(dipedente.getCognome());
		assertThat(evento.get(0).getDipendente().getNome()).isEqualTo(dipedente.getNome());
		assertThat(evento.get(0).getDipendente().getScadenzaVisita()).isEqualTo(dipedente.getScadenzaVisita());
	}
	
	@DirtiesContext
	@Test
	public void testEvento_AssegnaMedicalDay()
			throws Exception, JsonProcessingException
	{
		//recupero uno dei dipendenti creati
		Dipendente dipedente=createDipendenti();
		Long idDipendente=dipedente.getId();
		int idDipendenteInt=Long.valueOf(idDipendente).intValue();
		
		MedicalDay medicalDay=new MedicalDay();
		medicalDay.setData(LocalDate.now());
		Long idMedical=medicalDayRepository.save(medicalDay).getId();
		int idMedicalInt=Long.valueOf(idMedical).intValue();

		UpdateVisitaMedicaDTO visitaMedicaCreata=new UpdateVisitaMedicaDTO();
		visitaMedicaCreata.setOraVisita(LocalTime.of(12, 00));
		visitaMedicaCreata.setIdDipendente(dipedente.getId());
		visitaMedicaCreata.setIdMedicalDay(idMedical);
	
		visitaMedicaService.create(visitaMedicaCreata);

		List<Evento> evento= repositoryEvento.findAll();

		assertThat(evento.size()).isEqualTo(1);
		assertThat(evento.get(0).getTipologiaEvento()).isEqualTo(TipologiaEvento.ASSEGNATO_IL_MEDICALDAY);
		assertThat(evento.get(0).getDipendente().getId()).isEqualTo(idDipendenteInt);
		assertThat(evento.get(0).getMedicalDay().getId()).isEqualTo(idMedicalInt);
		assertThat(evento.get(0).getDipendente().getCodiceFiscale()).isEqualTo(dipedente.getCodiceFiscale());
		assertThat(evento.get(0).getDipendente().getCognome()).isEqualTo(dipedente.getCognome());
		assertThat(evento.get(0).getDipendente().getNome()).isEqualTo(dipedente.getNome());
	}
	
	
	private ResultActions testFindAllByCriteria(String criteria)
			throws Exception, JsonProcessingException
	{
		Dipendente dipendenteMario = new Dipendente();
		dipendenteMario.setCognome("Mario");
		dipendenteMario.setNome("Rossi");
		dipendenteMario.setDataNascita(LocalDate.now().minusYears(25));
		dipendenteMario.setCodiceFiscale("DMN");
		dipendenteMario.scadenzaVisita(LocalDate.now().plusYears(3));
		repositoryDipendente.save(dipendenteMario);
		
		Long id1=repositoryDipendente.save(dipendenteMario).getId();
		dipendenteMario.setScadenzaVisita(LocalDate.now().plusYears(5));
		UpdateDipendenteDTO dipDTO1=toUpdateDTO(dipendenteMario);
		dipendenteService.update(id1, dipDTO1);

		Dipendente dipendenteLuca = new Dipendente();
		dipendenteLuca.setCognome("Verdi");
		dipendenteLuca.setNome("Luca");
		dipendenteLuca.setDataNascita(LocalDate.now().minusYears(30));
		dipendenteLuca.setCodiceFiscale("BBCC");
		dipendenteLuca.scadenzaVisita(LocalDate.now().plusYears(3));
		
		Long id=repositoryDipendente.save(dipendenteLuca).getId();
		dipendenteLuca.setScadenzaVisita(LocalDate.now().plusYears(5));
		UpdateDipendenteDTO dipDTO=toUpdateDTO(dipendenteLuca);
		dipendenteService.update(id, dipDTO);
		
		return mvc.perform(get("/api/evento?"+ criteria)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

	}

	private Dipendente createDipendenti()
			throws Exception, JsonProcessingException
	{
		Dipendente dipendenteMario = new Dipendente();
		dipendenteMario.setCognome("Mario");
		dipendenteMario.setNome("Rossi");
		dipendenteMario.setDataNascita(LocalDate.now().minusYears(30));
		dipendenteMario.setCodiceFiscale("AA");
		dipendenteMario.setUltimaVisita(LocalDate.now().minusMonths(2));
		dipendenteMario.setScadenzaVisita(LocalDate.now().plusYears(3));
		repositoryDipendente.save(dipendenteMario);

		Dipendente dipendenteLuca = new Dipendente();
		dipendenteLuca.setCognome("Verdi");
		dipendenteLuca.setNome("Luca");
		dipendenteLuca.setDataNascita(LocalDate.now().minusYears(30));
		dipendenteLuca.setUltimaVisita(LocalDate.now().minusMonths(2));
		dipendenteLuca.setScadenzaVisita(LocalDate.now().plusYears(3));
		dipendenteLuca.setCodiceFiscale("MM");

		return repositoryDipendente.save(dipendenteLuca);

	}
}