package it.fullstack.salus.domain;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.Application;
import it.fullstack.salus.repository.DipendenteRepository;
import it.fullstack.salus.repository.SedeRepository;
import it.fullstack.salus.services.dto.SedeDTO;
import it.fullstack.salus.services.mapper.SedeMapper;
import jakarta.persistence.EntityManager;


@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = Application.class)
@AutoConfigureMockMvc
@WithMockUser
class SedeTest {

    private static final String DEFAULT_DENOMINAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DENOMINAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/sede";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    
    @Autowired
    private SedeRepository sedeRepository;


    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSedeMockMvc;

    private Sede sede;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sede createEntity(EntityManager em) {
        Sede sede = new Sede();
        		sede.setDenominazione(DEFAULT_DENOMINAZIONE);
        		sede.setProvincia(DEFAULT_PROVINCIA);
        		
        return sede;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sede createUpdatedEntity(EntityManager em) {
        Sede sede = new Sede();
        		sede.setDenominazione(UPDATED_DENOMINAZIONE);
        		sede.setProvincia(UPDATED_PROVINCIA);
        return sede;
    }

    @BeforeEach
    public void initTest() {
        sede = createEntity(em);
    }

    @Test
    @Transactional
    void createSede() throws Exception {
        int databaseSizeBeforeCreate = sedeRepository.findAll().size();
        // Create the Sede
        SedeDTO sedeDTO = SedeMapper.toDTO(sede);
        restSedeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sedeDTO)))
            .andExpect(status().isOk());

        // Validate the Sede in the database
        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeCreate + 1);
        Sede testSede = sedeList.get(sedeList.size() - 1);
        assertThat(testSede.getDenominazione()).isEqualTo(DEFAULT_DENOMINAZIONE);
        assertThat(testSede.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
    }

    @Test
    @Transactional
    void checkDenominazioneIsRequired() throws Exception {
        int databaseSizeBeforeTest = sedeRepository.findAll().size();
        // set the field null
        sede.setDenominazione(null);

        // Create the Sede, which fails.
        SedeDTO sedeDTO = SedeMapper.toDTO(sede);

        restSedeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
            		.content(TestUtil.convertObjectToJsonBytes(sedeDTO)))
            .andExpect(status().isBadRequest());

        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkProvinciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = sedeRepository.findAll().size();
        // set the field null
        sede.setProvincia(null);

        // Create the Sede, which fails.
        SedeDTO sedeDTO = SedeMapper.toDTO(sede);

        restSedeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sedeDTO)))
            .andExpect(status().isBadRequest());

        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllSedes() throws Exception {
        // Initialize the database
        sedeRepository.saveAndFlush(sede);

        // Get all the sedeList
        restSedeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sede.getId().intValue())))
            .andExpect(jsonPath("$.[*].denominazione").value(hasItem(DEFAULT_DENOMINAZIONE)))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA)));
    }

    @Test
    @Transactional
    void getSede() throws Exception {
        // Initialize the database
        sedeRepository.saveAndFlush(sede);

        // Get the sede
        restSedeMockMvc
            .perform(get(ENTITY_API_URL_ID, sede.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sede.getId().intValue()))
            .andExpect(jsonPath("$.denominazione").value(DEFAULT_DENOMINAZIONE))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA));
    }

   
    @Test
    @Transactional
    void getNonExistingSede() throws Exception {
        // Get the sede
        restSedeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSede() throws Exception {
        // Initialize the database
        sedeRepository.saveAndFlush(sede);

        int databaseSizeBeforeUpdate = sedeRepository.findAll().size();

        // Update the sede
        Sede updatedSede = sedeRepository.findById(sede.getId()).get();
        // Disconnect from session so that the updates on updatedSede are not directly saved in db
        em.detach(updatedSede);
        updatedSede.setDenominazione(UPDATED_DENOMINAZIONE);
        updatedSede.setProvincia(UPDATED_PROVINCIA);
        SedeDTO sedeDTO = SedeMapper.toDTO(updatedSede);

        restSedeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sedeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sedeDTO))
            )
            .andExpect(status().isOk());

        // Validate the Sede in the database
        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeUpdate);
        Sede testSede = sedeList.get(sedeList.size() - 1);
        assertThat(testSede.getDenominazione()).isEqualTo(UPDATED_DENOMINAZIONE);
        assertThat(testSede.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
    }

    @Test
    @Transactional
    void putNonExistingSede() throws Exception {
        int databaseSizeBeforeUpdate = sedeRepository.findAll().size();
        sede.setId(count.incrementAndGet());

        // Create the Sede
        SedeDTO sedeDTO = SedeMapper.toDTO(sede);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSedeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sedeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sedeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sede in the database
        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSede() throws Exception {
        int databaseSizeBeforeUpdate = sedeRepository.findAll().size();
        sede.setId(count.incrementAndGet());

        // Create the Sede
        SedeDTO sedeDTO = SedeMapper.toDTO(sede);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSedeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sedeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sede in the database
        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSede() throws Exception {
        int databaseSizeBeforeUpdate = sedeRepository.findAll().size();
        sede.setId(count.incrementAndGet());

        // Create the Sede
        SedeDTO sedeDTO = SedeMapper.toDTO(sede);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSedeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sedeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sede in the database
        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSede() throws Exception {
        // Initialize the database
        sedeRepository.saveAndFlush(sede);

        int databaseSizeBeforeDelete = sedeRepository.findAll().size();

        // Delete the sede
        restSedeMockMvc
            .perform(delete(ENTITY_API_URL_ID, sede.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        // Validate the database contains one less item
        List<Sede> sedeList = sedeRepository.findAll();
        assertThat(sedeList).hasSize(databaseSizeBeforeDelete - 1);
    }

}
