package it.fullstack.salus.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.Application;
import it.fullstack.salus.domain.enumeration.Sesso;
import it.fullstack.salus.repository.ContrattoRepository;
import it.fullstack.salus.repository.MedicoRepository;
import it.fullstack.salus.services.dto.MedicoDTO;
import it.fullstack.salus.services.dto.UpdateMedicoDTO;
import it.fullstack.salus.services.mapper.MedicoMapper;
import jakarta.persistence.EntityManager;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = Application.class)
@AutoConfigureMockMvc
@WithMockUser
public class MedicoTest{

	private static final String DEFAULT_NUMERO_TELEFONO = "AAAAAAAAAA";
	private static final String UPDATED_NUMERO_TELEFONO = "BBBBBBBBBB";

	private static final Boolean DEFAULT_SOCIETA = false;
	private static final Boolean UPDATED_SOCIETA = true;

	private static final String DEFAULT_NOME = "AAAAAAAAAA";
	private static final String UPDATED_NOME = "BBBBBBBBBB";

	private static final String DEFAULT_COGNOME = "AAAAAAAAAA";
	private static final String UPDATED_COGNOME = "BBBBBBBBBB";

	private static final LocalDate DEFAULT_DATA_NASCITA = LocalDate.ofEpochDay(0L);
	private static final LocalDate UPDATED_DATA_NASCITA = LocalDate.now(ZoneId.systemDefault());
	private static final LocalDate SMALLER_DATA_NASCITA = LocalDate.ofEpochDay(-1L);

	private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
	private static final String UPDATED_EMAIL = "BBBBBBBBBB";

	private static final String DEFAULT_CODICE_FISCALE = "AAAAAAAAAA";
	private static final String UPDATED_CODICE_FISCALE = "BBBBBBBBBB";

	private static final Sesso DEFAULT_SESSO = Sesso.UOMO;
	private static final Sesso UPDATED_SESSO = Sesso.DONNA;

	private static final String ENTITY_API_URL = "/api/medico";
	private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

	private static Random random = new Random();
	private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

	@Autowired
	private MedicoRepository medicoRepository;

	@Autowired
	private ContrattoRepository contrattoRepository;
	
	@Autowired
	private EntityManager em;

	@Autowired
	private MockMvc restMedicoMockMvc;

	private Medico medico;

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static Medico createEntityMedico(EntityManager em) {
		Medico medico = new Medico();
		medico.setNumeroTelefono(DEFAULT_NUMERO_TELEFONO);
		medico.setSocieta(DEFAULT_SOCIETA);
		medico.setNome(DEFAULT_NOME);
		medico.setCognome(DEFAULT_COGNOME);
		medico.setDataNascita(DEFAULT_DATA_NASCITA);
		medico.setEmail(DEFAULT_EMAIL);
		medico.setCodiceFiscale(DEFAULT_CODICE_FISCALE);
		medico.setSesso(DEFAULT_SESSO);
		return medico;
	}
	
	//FIXME aspettare merge di alessandro
	public Contratto createContratto(Sede sede) {
		Contratto contratto = new Contratto();
		if (TestUtil.findAll(em, Contratto.class).isEmpty()) {
			contratto = ContrattoTest.createEntity(em);
			em.persist(contratto);
			em.flush();
		}
		contratto.setDataInizio(LocalDate.now().minusYears(30));
		contratto.setDataFine(LocalDate.now().plusYears(50));
		contratto.setSede(sede);
		return contratto;
	}

	public Sede createSede() {
		Sede sede = new Sede();
		if (TestUtil.findAll(em, Sede.class).isEmpty()) {
			sede = SedeTest.createEntity(em);
			em.persist(sede);
			em.flush();
		} else {
			sede = TestUtil.findAll(em, Sede.class).get(0);
		}
		return sede;
	}
	
	public static Medico createUpdatedEntity(EntityManager em) {
		Medico medico = new Medico();
		medico.setNumeroTelefono(DEFAULT_NUMERO_TELEFONO);
		medico.setSocieta(DEFAULT_SOCIETA);
		medico.setNome(DEFAULT_NOME);
		medico.setCognome(DEFAULT_COGNOME);
		medico.setDataNascita(DEFAULT_DATA_NASCITA);
		medico.setEmail(DEFAULT_EMAIL);
		medico.setCodiceFiscale(DEFAULT_CODICE_FISCALE);
		medico.setSesso(DEFAULT_SESSO);
		return medico;
	}

	@BeforeEach
	public void initTest() {
		medico = createEntityMedico(em);
	}

	@Test
	@Transactional
	void createMedico() throws Exception {
		int databaseSizeBeforeCreate = medicoRepository.findAll().size();
		// Create the Medico
		MedicoDTO medicoDTO = MedicoMapper.toDTO(medico);
		restMedicoMockMvc
		.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicoDTO)))
		.andExpect(status().isOk());
		// Validate the Medico in the database
		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeCreate + 1);
		Medico testMedico = medicoList.get(medicoList.size() - 1);
		assertThat(testMedico.getNumeroTelefono()).isEqualTo(DEFAULT_NUMERO_TELEFONO);
		assertThat(testMedico.getSocieta()).isEqualTo(DEFAULT_SOCIETA);
		assertThat(testMedico.getNome()).isEqualTo(DEFAULT_NOME);
		assertThat(testMedico.getCognome()).isEqualTo(DEFAULT_COGNOME);
		assertThat(testMedico.getDataNascita()).isEqualTo(DEFAULT_DATA_NASCITA);
		assertThat(testMedico.getEmail()).isEqualTo(DEFAULT_EMAIL);
		assertThat(testMedico.getCodiceFiscale()).isEqualTo(DEFAULT_CODICE_FISCALE);
		assertThat(testMedico.getSesso()).isEqualTo(DEFAULT_SESSO);
	}

	@Test
	@Transactional
	void checkNomeIsRequired() throws Exception {
		int databaseSizeBeforeTest = medicoRepository.findAll().size();
		// set the field null
		medico.setNome(null);

		// Create the Medico, which fails.
		MedicoDTO medicoDTO = MedicoMapper.toDTO(medico);

		restMedicoMockMvc
		.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicoDTO)))
		.andExpect(status().isBadRequest());

		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	void checkCognomeIsRequired() throws Exception {
		int databaseSizeBeforeTest = medicoRepository.findAll().size();
		// set the field null
		medico.setCognome(null);

		// Create the Medico, which fails.
		MedicoDTO medicoDTO = MedicoMapper.toDTO(medico);

		restMedicoMockMvc.perform(post(ENTITY_API_URL)
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(medicoDTO)))
		.andExpect(status().isBadRequest());

		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	void checkDataNascitaIsRequired() throws Exception {
		int databaseSizeBeforeTest = medicoRepository.findAll().size();
		// set the field null
		medico.setDataNascita(null);

		// Create the Medico, which fails.
		UpdateMedicoDTO medicoDTO = MedicoMapper.toUpdateDTO(medico);

		restMedicoMockMvc
		.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(medicoDTO)))
		.andExpect(status().isBadRequest());

		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	void checkCodiceFiscaleIsRequired() throws Exception {
		int databaseSizeBeforeTest = medicoRepository.findAll().size();
		// set the field null
		medico.setCodiceFiscale(null);

		// Create the Medico, which fails.
		MedicoDTO medicoDTO = MedicoMapper.toDTO(medico);

		restMedicoMockMvc
		.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicoDTO)))
		.andExpect(status().isBadRequest());

		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	void checkSessoIsRequired() throws Exception {
		int databaseSizeBeforeTest = medicoRepository.findAll().size();
		// set the field null
		medico.setSesso(null);

		// Create the Medico, which fails.
		MedicoDTO medicoDTO = MedicoMapper.toDTO(medico);

		restMedicoMockMvc
		.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicoDTO)))
		.andExpect(status().isBadRequest());

		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	void getAllMedicos() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);
		
		// Get all the medicoList
		restMedicoMockMvc
		.perform(get(ENTITY_API_URL + "?sort=id,desc"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(jsonPath("$.content[*].id").value(hasItem(medico.getId().intValue())))
		.andExpect(jsonPath("$.content[*].numeroTelefono").value(hasItem(DEFAULT_NUMERO_TELEFONO)))
		.andExpect(jsonPath("$.content[*].societa").value(hasItem(DEFAULT_SOCIETA.booleanValue())))
		.andExpect(jsonPath("$.content[*].nome").value(hasItem(DEFAULT_NOME)))
		.andExpect(jsonPath("$.content[*].cognome").value(hasItem(DEFAULT_COGNOME)))
		.andExpect(jsonPath("$.content[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
		.andExpect(jsonPath("$.content[*].email").value(hasItem(DEFAULT_EMAIL)))
		.andExpect(jsonPath("$.content[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE)))
		.andExpect(jsonPath("$.content[*].sesso").value(hasItem(DEFAULT_SESSO.toString())));
	}

	@Test
	@Transactional
	void getMedico() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get the medico
		restMedicoMockMvc
		.perform(get(ENTITY_API_URL_ID, medico.getId()))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(jsonPath("$.id").value(medico.getId().intValue()))
		.andExpect(jsonPath("$.numeroTelefono").value(DEFAULT_NUMERO_TELEFONO))
		.andExpect(jsonPath("$.societa").value(DEFAULT_SOCIETA.booleanValue()))
		.andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
		.andExpect(jsonPath("$.cognome").value(DEFAULT_COGNOME))
		.andExpect(jsonPath("$.dataNascita").value(DEFAULT_DATA_NASCITA.toString()))
		.andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
		.andExpect(jsonPath("$.codiceFiscale").value(DEFAULT_CODICE_FISCALE))
		.andExpect(jsonPath("$.sesso").value(DEFAULT_SESSO.toString()));
	}

	@Test
	@Transactional
	void getAllMedicosByNomeIsEqualToSomething() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where nome equals to DEFAULT_NOME
		defaultMedicoShouldBeFound("nome.equals=" + DEFAULT_NOME);

	}
	
	@Test
	@Transactional
	void getAllMedicosBySedeIdIsEqualToSomething() throws Exception {
		// Initialize the database
		List<Contratto> contratti = new ArrayList<>();
		Sede sede = createSede();
		medicoRepository.saveAndFlush(medico);
		Contratto contratto = createContratto(sede);
		contratti.add(contratto);
		medico.setContratti(contratti);
		contratto.setMedico(medico);
		contrattoRepository.save(contratto);
		
		// Get all the medicoList where nome equals to DEFAULT_NOME
		defaultMedicoShouldBeFound("sedeId.equals=" + 1);

	}

	@Test
	@Transactional
	void getAllMedicosByNomeIsInShouldWork() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where nome in DEFAULT_NOME or UPDATED_NOME
		defaultMedicoShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

	}

	@Test
	@Transactional
	void getAllMedicosByNomeIsNullOrNotNull() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where nome is not null
		defaultMedicoShouldBeFound("nome.specified=true");

	}

	@Test
	@Transactional
	void getAllMedicosByNomeContainsSomething() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where nome contains DEFAULT_NOME
		defaultMedicoShouldBeFound("nome.contains=" + DEFAULT_NOME);

	}

	@Test
	@Transactional
	void getAllMedicosByNomeNotContainsSomething() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);


		// Get all the medicoList where nome does not contain UPDATED_NOME
		defaultMedicoShouldBeFound("nome.doesNotContain=" + UPDATED_NOME);
	}

	@Test
	@Transactional
	void getAllMedicosByCognomeIsEqualToSomething() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where cognome equals to DEFAULT_COGNOME
		defaultMedicoShouldBeFound("cognome.equals=" + DEFAULT_COGNOME);

	}

	@Test
	@Transactional
	void getAllMedicosByCognomeIsInShouldWork() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where cognome in DEFAULT_COGNOME or UPDATED_COGNOME
		defaultMedicoShouldBeFound("cognome.in=" + DEFAULT_COGNOME + "," + UPDATED_COGNOME);

	}

	@Test
	@Transactional
	void getAllMedicosByCognomeIsNullOrNotNull() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where cognome is not null
		defaultMedicoShouldBeFound("cognome.specified=true");

	}

	@Test
	@Transactional
	void getAllMedicosByCognomeContainsSomething() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where cognome contains DEFAULT_COGNOME
		defaultMedicoShouldBeFound("cognome.contains=" + DEFAULT_COGNOME);

	}

	@Test
	@Transactional
	void getAllMedicosByCognomeNotContainsSomething() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		// Get all the medicoList where cognome does not contain UPDATED_COGNOME
		defaultMedicoShouldBeFound("cognome.doesNotContain=" + UPDATED_COGNOME);
	}

	/**
	 * Executes the search, and checks that the default entity is returned.
	 */
	private void defaultMedicoShouldBeFound(String filter) throws Exception {
		restMedicoMockMvc
		.perform(get(ENTITY_API_URL + "?" + filter))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(jsonPath("$.content[*].id").value(hasItem(medico.getId().intValue())))
		.andExpect(jsonPath("$.content[*].numeroTelefono").value(hasItem(DEFAULT_NUMERO_TELEFONO)))
		.andExpect(jsonPath("$.content[*].societa").value(hasItem(DEFAULT_SOCIETA.booleanValue())))
		.andExpect(jsonPath("$.content[*].nome").value(hasItem(DEFAULT_NOME)))
		.andExpect(jsonPath("$.content[*].cognome").value(hasItem(DEFAULT_COGNOME)))
		.andExpect(jsonPath("$.content[*].dataNascita").value(hasItem(DEFAULT_DATA_NASCITA.toString())))
		.andExpect(jsonPath("$.content[*].email").value(hasItem(DEFAULT_EMAIL)))
		.andExpect(jsonPath("$.content[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE)))
		.andExpect(jsonPath("$.content[*].sesso").value(hasItem(DEFAULT_SESSO.toString())));

	}

	@Test
	@Transactional
	void getNonExistingMedico() throws Exception {
		// Get the medico
		restMedicoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE))
				.andExpect(status().isBadRequest());
	}

	@Test
	@Transactional
	void putExistingMedico() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		int databaseSizeBeforeUpdate = medicoRepository.findAll().size();

		// Update the medico
		Medico updatedMedico = medicoRepository.findById(medico.getId()).get();
		// Disconnect from session so that the updates on updatedMedico are not directly saved in db
		em.detach(updatedMedico);
		updatedMedico.setNumeroTelefono(UPDATED_NUMERO_TELEFONO);
		updatedMedico.setSocieta(UPDATED_SOCIETA);
		updatedMedico.setNome(UPDATED_NOME);
		updatedMedico.setCognome(UPDATED_COGNOME);
		updatedMedico.setDataNascita(UPDATED_DATA_NASCITA);
		updatedMedico.setEmail(UPDATED_EMAIL);
		updatedMedico.setCodiceFiscale(UPDATED_CODICE_FISCALE);
		updatedMedico.setSesso(UPDATED_SESSO);
		UpdateMedicoDTO medicoDTO = MedicoMapper.toUpdateDTO(updatedMedico);
		
		restMedicoMockMvc
		.perform(put(ENTITY_API_URL_ID, updatedMedico.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(medicoDTO))
				)
		.andExpect(status().isOk());

		// Validate the Medico in the database
		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeUpdate);
		Medico testMedico = medicoList.get(medicoList.size() - 1);
		assertThat(testMedico.getNumeroTelefono()).isEqualTo(UPDATED_NUMERO_TELEFONO);
		assertThat(testMedico.getSocieta()).isEqualTo(UPDATED_SOCIETA);
		assertThat(testMedico.getNome()).isEqualTo(UPDATED_NOME);
		assertThat(testMedico.getCognome()).isEqualTo(UPDATED_COGNOME);
		assertThat(testMedico.getDataNascita()).isEqualTo(UPDATED_DATA_NASCITA);
		assertThat(testMedico.getEmail()).isEqualTo(UPDATED_EMAIL);
		assertThat(testMedico.getCodiceFiscale()).isEqualTo(UPDATED_CODICE_FISCALE);
		assertThat(testMedico.getSesso()).isEqualTo(UPDATED_SESSO);
	}

	@Test
	@Transactional
	void putNonExistingMedico() throws Exception {
		int databaseSizeBeforeUpdate = medicoRepository.findAll().size();
		medico.setId(count.incrementAndGet());

		// Create the Medico
		UpdateMedicoDTO medicoDTO = MedicoMapper.toUpdateDTO(medico);

		// If the entity doesn't have an ID, it will throw BadRequestAlertException
		restMedicoMockMvc
		.perform(put(ENTITY_API_URL_ID, medico.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(medicoDTO))
				)
		.andExpect(status().isBadRequest());

		// Validate the Medico in the database
		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@Transactional
	void putWithIdMismatchMedico() throws Exception {
		int databaseSizeBeforeUpdate = medicoRepository.findAll().size();
		medico.setId(count.incrementAndGet());

		// Create the Medico
		UpdateMedicoDTO medicoDTO = MedicoMapper.toUpdateDTO(medico);

		// If url ID doesn't match entity ID, it will throw BadRequestAlertException
		restMedicoMockMvc
		.perform(
				put(ENTITY_API_URL_ID, count.incrementAndGet())
				.contentType(MediaType.APPLICATION_JSON)
				.content(TestUtil.convertObjectToJsonBytes(medicoDTO))
				)
		.andExpect(status().isBadRequest());

		// Validate the Medico in the database
		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@Transactional
	void putWithMissingIdPathParamMedico() throws Exception {
		int databaseSizeBeforeUpdate = medicoRepository.findAll().size();
		medico.setId(count.incrementAndGet());

		// Create the Medico
		UpdateMedicoDTO medicoDTO = MedicoMapper.toUpdateDTO(medico);

		// If url ID doesn't match entity ID, it will throw BadRequestAlertException
		restMedicoMockMvc
		.perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicoDTO)))
		.andExpect(status().isMethodNotAllowed());

		// Validate the Medico in the database
		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeUpdate);
	}

	@Test
	@DirtiesContext
	void deleteMedico() throws Exception {
		// Initialize the database
		medicoRepository.saveAndFlush(medico);

		int databaseSizeBeforeDelete = medicoRepository.findAll().size();
		// Delete the medico
		restMedicoMockMvc
		.perform(delete(ENTITY_API_URL_ID, medico.getId())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

		// Validate the database contains one less item
		List<Medico> medicoList = medicoRepository.findAll();
		assertThat(medicoList).hasSize(databaseSizeBeforeDelete - 1);
	}
}