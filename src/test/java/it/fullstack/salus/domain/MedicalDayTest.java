package it.fullstack.salus.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.Application;
import it.fullstack.salus.domain.enumeration.StatoMedicalDay;
import it.fullstack.salus.repository.MedicalDayRepository;
import it.fullstack.salus.services.dto.MedicalDayDTO;
import it.fullstack.salus.services.dto.UpdateMedicalDayDTO;
import it.fullstack.salus.services.mapper.MedicalDayMapper;
import jakarta.persistence.EntityManager;

/**
 * Integration tests for the {@link MedicalDayResource} REST controller.
 */

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Application.class)
//@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MedicalDayTest {

    private static final LocalDate DEFAULT_DATA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA = LocalDate.ofEpochDay(-1L);

    private static final StatoMedicalDay DEFAULT_STATO_MEDICAL_DAY = StatoMedicalDay.IN_LAVORAZIONE;
    private static final StatoMedicalDay UPDATED_STATO_MEDICAL_DAY = StatoMedicalDay.COMPLETO;

    private static final String ENTITY_API_URL = "/api/medical-day";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MedicalDayRepository medicalDayRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMedicalDayMockMvc;

    private MedicalDay medicalDay;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MedicalDay createEntity(EntityManager em) {
        MedicalDay medicalDay = new MedicalDay();
        medicalDay.setData(UPDATED_DATA.plusDays(20));
        // Add required entity
        Contratto contratto;
        if (TestUtil.findAll(em, Contratto.class).isEmpty()) {
            contratto = ContrattoTest.createEntity(em);
            em.persist(contratto);
            em.flush();
        } else {
            contratto = TestUtil.findAll(em, Contratto.class).get(0);
        }
        medicalDay.setContratto(contratto);
        return medicalDay;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MedicalDay createUpdatedEntity(EntityManager em) {
        MedicalDay medicalDay = new MedicalDay();
        medicalDay.setData(UPDATED_DATA);
        // Add required entity
        Contratto contratto;
        if (TestUtil.findAll(em, Contratto.class).isEmpty()) {
            contratto = ContrattoTest.createUpdatedEntity(em);
            em.persist(contratto);
            em.flush();
        } else {
            contratto = TestUtil.findAll(em, Contratto.class).get(0);
        }
        medicalDay.setContratto(contratto);
        return medicalDay;
    }

    @BeforeEach
    public void initTest() {
        medicalDay = createEntity(em);
    }

    @Test
    @Transactional
    void createMedicalDay() throws Exception {
        int databaseSizeBeforeCreate = medicalDayRepository.findAll().size();
        // Create the MedicalDay
        UpdateMedicalDayDTO updateMedicalDayDTO = MedicalDayMapper.toUpdateDTO(medicalDay);
        restMedicalDayMockMvc
            .perform(post(ENTITY_API_URL)
            		.contentType(MediaType.APPLICATION_JSON)
            		.content(TestUtil.convertObjectToJsonBytes(updateMedicalDayDTO)))
            .andExpect(status().isOk());

        // Validate the MedicalDay in the database
//        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
//        assertThat(medicalDayList).hasSize(databaseSizeBeforeCreate + 1);
//        MedicalDay testMedicalDay = medicalDayList.get(medicalDayList.size() - 1);
//        assertThat(testMedicalDay.getData()).isEqualTo(DEFAULT_DATA);
//        assertThat(testMedicalDay.getStatoMedicalDay()).isEqualTo(DEFAULT_STATO_MEDICAL_DAY);
    }

    @Test
    @Transactional
    void createMedicalDayWithExistingId() throws Exception {
        // Create the MedicalDay with an existing ID
    	
        medicalDay.setId(1L);
        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);

        int databaseSizeBeforeCreate = medicalDayRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMedicalDayMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicalDayDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MedicalDay in the database
        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkStatoMedicalDayIsRequired() throws Exception {
        int databaseSizeBeforeTest = medicalDayRepository.findAll().size();
        // set the field null

        // Create the MedicalDay, which fails.
        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);

        restMedicalDayMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicalDayDTO)))
            .andExpect(status().isBadRequest());

        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMedicalDays() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList
        restMedicalDayMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.content[*].id").value(hasItem(medicalDay.getId().intValue())))
            .andExpect(jsonPath("$.content[*].data").value(hasItem(UPDATED_DATA.plusDays(20).toString())));
//            .andExpect(jsonPath("$.[*].statoMedicalDay").value(hasItem(DEFAULT_STATO_MEDICAL_DAY.toString())));
    }

    @Test
    @Transactional
    void getMedicalDay() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get the medicalDay
        restMedicalDayMockMvc
            .perform(get(ENTITY_API_URL_ID, medicalDay.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(medicalDay.getId().intValue()))
            .andExpect(jsonPath("$.data").value(UPDATED_DATA.plusDays(20).toString()));
//            .andExpect(jsonPath("$.statoMedicalDay").value(DEFAULT_STATO_MEDICAL_DAY.toString()));
    }

    @Test
    @Transactional
    void getMedicalDaysByIdFiltering() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        Long id = medicalDay.getId();

        defaultMedicalDayShouldBeFound("id.equals=" + id);
//        defaultMedicalDayShouldNotBeFound("id.notEquals=" + id);

        defaultMedicalDayShouldBeFound("id.greaterThanOrEqual=" + id);
//        defaultMedicalDayShouldNotBeFound("id.greaterThan=" + id);

        defaultMedicalDayShouldBeFound("id.lessThanOrEqual=" + id);
//        defaultMedicalDayShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByDataIsEqualToSomething() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where data equals to DEFAULT_DATA
        defaultMedicalDayShouldBeFound("data.equals=" + DEFAULT_DATA);

        // Get all the medicalDayList where data equals to UPDATED_DATA
//        defaultMedicalDayShouldNotBeFound("data.equals=" + UPDATED_DATA);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByDataIsInShouldWork() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where data in DEFAULT_DATA or UPDATED_DATA
        defaultMedicalDayShouldBeFound("data.in=" + DEFAULT_DATA + "," + UPDATED_DATA);

        // Get all the medicalDayList where data equals to UPDATED_DATA
//        defaultMedicalDayShouldNotBeFound("data.in=" + UPDATED_DATA);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByDataIsNullOrNotNull() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where data is not null
        defaultMedicalDayShouldBeFound("data.specified=true");

        // Get all the medicalDayList where data is null
//        defaultMedicalDayShouldNotBeFound("data.specified=false");
    }

    @Test
    @Transactional
    void getAllMedicalDaysByDataIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where data is greater than or equal to DEFAULT_DATA
        defaultMedicalDayShouldBeFound("data.greaterThanOrEqual=" + DEFAULT_DATA);

        // Get all the medicalDayList where data is greater than or equal to UPDATED_DATA
//        defaultMedicalDayShouldNotBeFound("data.greaterThanOrEqual=" + UPDATED_DATA);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByDataIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where data is less than or equal to DEFAULT_DATA
        defaultMedicalDayShouldBeFound("data.lessThanOrEqual=" + DEFAULT_DATA);

        // Get all the medicalDayList where data is less than or equal to SMALLER_DATA
//        defaultMedicalDayShouldNotBeFound("data.lessThanOrEqual=" + SMALLER_DATA);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByDataIsLessThanSomething() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where data is less than DEFAULT_DATA
//        defaultMedicalDayShouldNotBeFound("data.lessThan=" + DEFAULT_DATA);

        // Get all the medicalDayList where data is less than UPDATED_DATA
        defaultMedicalDayShouldBeFound("data.lessThan=" + UPDATED_DATA);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByDataIsGreaterThanSomething() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where data is greater than DEFAULT_DATA
//        defaultMedicalDayShouldNotBeFound("data.greaterThan=" + DEFAULT_DATA);

        // Get all the medicalDayList where data is greater than SMALLER_DATA
        defaultMedicalDayShouldBeFound("data.greaterThan=" + SMALLER_DATA);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByStatoMedicalDayIsEqualToSomething() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where statoMedicalDay equals to DEFAULT_STATO_MEDICAL_DAY
        defaultMedicalDayShouldBeFound("statoMedicalDay.equals=" + DEFAULT_STATO_MEDICAL_DAY);

        // Get all the medicalDayList where statoMedicalDay equals to UPDATED_STATO_MEDICAL_DAY
//        defaultMedicalDayShouldNotBeFound("statoMedicalDay.equals=" + UPDATED_STATO_MEDICAL_DAY);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByStatoMedicalDayIsInShouldWork() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where statoMedicalDay in DEFAULT_STATO_MEDICAL_DAY or UPDATED_STATO_MEDICAL_DAY
        defaultMedicalDayShouldBeFound("statoMedicalDay.in=" + DEFAULT_STATO_MEDICAL_DAY + "," + UPDATED_STATO_MEDICAL_DAY);

        // Get all the medicalDayList where statoMedicalDay equals to UPDATED_STATO_MEDICAL_DAY
//        defaultMedicalDayShouldNotBeFound("statoMedicalDay.in=" + UPDATED_STATO_MEDICAL_DAY);
    }

    @Test
    @Transactional
    void getAllMedicalDaysByStatoMedicalDayIsNullOrNotNull() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        // Get all the medicalDayList where statoMedicalDay is not null
        defaultMedicalDayShouldBeFound("statoMedicalDay.specified=true");

        // Get all the medicalDayList where statoMedicalDay is null
//        defaultMedicalDayShouldNotBeFound("statoMedicalDay.specified=false");
    }

//    @Test
//    @Transactional
//    void getAllMedicalDaysByVisiteMedicheIsEqualToSomething() throws Exception {
//        VisitaMedica visiteMediche;
//        if (TestUtil.findAll(em, VisitaMedica.class).isEmpty()) {
//            medicalDayRepository.saveAndFlush(medicalDay);
//            visiteMediche = VisitaMedicaTest.createEntity(em);
//        } else {
//            visiteMediche = TestUtil.findAll(em, VisitaMedica.class).get(0);
//        }
//        em.persist(visiteMediche);
//        em.flush();
//        medicalDay.getVisiteMediche().add(visiteMediche);
//        medicalDayRepository.saveAndFlush(medicalDay);
//        Long visiteMedicheId = visiteMediche.getId();
//
//        // Get all the medicalDayList where visiteMediche equals to visiteMedicheId
//        defaultMedicalDayShouldBeFound("visiteMedicheId.equals=" + visiteMedicheId);
//
//        // Get all the medicalDayList where visiteMediche equals to (visiteMedicheId + 1)
//        defaultMedicalDayShouldNotBeFound("visiteMedicheId.equals=" + (visiteMedicheId + 1));
//    }

    @Test
    @Transactional
    void getAllMedicalDaysByContrattoIsEqualToSomething() throws Exception {
        Contratto contratto;
        if (TestUtil.findAll(em, Contratto.class).isEmpty()) {
            medicalDayRepository.saveAndFlush(medicalDay);
            contratto = ContrattoTest.createEntity(em);
        } else {
            contratto = TestUtil.findAll(em, Contratto.class).get(0);
        }
        em.persist(contratto);
        em.flush();
        medicalDay.setContratto(contratto);
        medicalDayRepository.saveAndFlush(medicalDay);
        Long contrattoId = contratto.getId();

        // Get all the medicalDayList where contratto equals to contrattoId
        defaultMedicalDayShouldBeFound("contrattoId.equals=" + contrattoId);

        // Get all the medicalDayList where contratto equals to (contrattoId + 1)
//        defaultMedicalDayShouldNotBeFound("contrattoId.equals=" + (contrattoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMedicalDayShouldBeFound(String filter) throws Exception {
        restMedicalDayMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.content[*].id").value(hasItem(medicalDay.getId().intValue())))
            .andExpect(jsonPath("$.content[*].data").value(hasItem(UPDATED_DATA.plusDays(20).toString())));
//            .andExpect(jsonPath("$.[*].statoMedicalDay").value(hasItem(DEFAULT_STATO_MEDICAL_DAY.toString())));

        // Check, that the count call also returns 1
//        restMedicalDayMockMvc
//            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMedicalDayShouldNotBeFound(String filter) throws Exception {
        restMedicalDayMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
//            .andExpect(jsonPath("$.content").isArray())
//            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMedicalDayMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMedicalDay() throws Exception {
        // Get the medicalDay
        restMedicalDayMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE))
        .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void putExistingMedicalDay() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();

        // Update the medicalDay
        MedicalDay updatedMedicalDay = medicalDayRepository.findById(medicalDay.getId()).get();
        // Disconnect from session so that the updates on updatedMedicalDay are not directly saved in db
        em.detach(updatedMedicalDay);
        updatedMedicalDay.setData(UPDATED_DATA.plusDays(30));
        UpdateMedicalDayDTO updateMedicalDayDTO = MedicalDayMapper.toUpdateDTO(updatedMedicalDay);

        restMedicalDayMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMedicalDay.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updateMedicalDayDTO))
            )
            .andExpect(status().isOk());

        // Validate the MedicalDay in the database
        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
        MedicalDay testMedicalDay = medicalDayList.get(medicalDayList.size() - 1);
        assertThat(testMedicalDay.getData()).isEqualTo(UPDATED_DATA.plusDays(30));
        //FIXME serve passare per la service?
//        assertThat(testMedicalDay.getStatoMedicalDay()).isEqualTo(DEFAULT_STATO_MEDICAL_DAY);
    }

    @Test
    @Transactional
    void putNonExistingMedicalDay() throws Exception {
        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
        medicalDay.setId(count.incrementAndGet());

        // Create the MedicalDay
        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMedicalDayMockMvc
            .perform(
                put(ENTITY_API_URL_ID, medicalDayDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(medicalDayDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MedicalDay in the database
        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMedicalDay() throws Exception {
        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
        medicalDay.setId(count.incrementAndGet());

        // Create the MedicalDay
        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMedicalDayMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(medicalDayDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MedicalDay in the database
        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMedicalDay() throws Exception {
        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
        medicalDay.setId(count.incrementAndGet());

        // Create the MedicalDay
        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMedicalDayMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medicalDayDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MedicalDay in the database
        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
    }

//    @Test
//    @Transactional
//    void partialUpdateMedicalDayWithPatch() throws Exception {
//        // Initialize the database
//        medicalDayRepository.saveAndFlush(medicalDay);
//
//        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
//
//        // Update the medicalDay using partial update
//        MedicalDay partialUpdatedMedicalDay = new MedicalDay();
//        partialUpdatedMedicalDay.setId(medicalDay.getId());
//
//        partialUpdatedMedicalDay.setData(UPDATED_DATA);
//
//        restMedicalDayMockMvc
//            .perform(
//                patch(ENTITY_API_URL_ID, partialUpdatedMedicalDay.getId())
//                    .contentType("application/merge-patch+json")
//                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMedicalDay))
//            )
//            .andExpect(status().isOk());
//
//        // Validate the MedicalDay in the database
//        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
//        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
//        MedicalDay testMedicalDay = medicalDayList.get(medicalDayList.size() - 1);
//        assertThat(testMedicalDay.getData()).isEqualTo(UPDATED_DATA);
//        assertThat(testMedicalDay.getStatoMedicalDay()).isEqualTo(UPDATED_STATO_MEDICAL_DAY);
//    }

//    @Test
//    @Transactional
//    void fullUpdateMedicalDayWithPatch() throws Exception {
//        // Initialize the database
//        medicalDayRepository.saveAndFlush(medicalDay);
//
//        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
//
//        // Update the medicalDay using partial update
//        MedicalDay partialUpdatedMedicalDay = new MedicalDay();
//        partialUpdatedMedicalDay.setId(medicalDay.getId());
//
//        partialUpdatedMedicalDay.setData(UPDATED_DATA);
//
//        restMedicalDayMockMvc
//            .perform(
//                patch(ENTITY_API_URL_ID, partialUpdatedMedicalDay.getId())
//                    .contentType("application/merge-patch+json")
//                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMedicalDay))
//            )
//            .andExpect(status().isOk());
//
//        // Validate the MedicalDay in the database
//        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
//        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
//        MedicalDay testMedicalDay = medicalDayList.get(medicalDayList.size() - 1);
//        assertThat(testMedicalDay.getData()).isEqualTo(UPDATED_DATA);
//        assertThat(testMedicalDay.getStatoMedicalDay()).isEqualTo(UPDATED_STATO_MEDICAL_DAY);
//    }

//    @Test
//    @Transactional
//    void patchNonExistingMedicalDay() throws Exception {
//        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
//        medicalDay.setId(count.incrementAndGet());
//
//        // Create the MedicalDay
//        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restMedicalDayMockMvc
//            .perform(
//                patch(ENTITY_API_URL_ID, medicalDayDTO.getId())
//                    .contentType("application/merge-patch+json")
//                    .content(TestUtil.convertObjectToJsonBytes(medicalDayDTO))
//            )
//            .andExpect(status().isBadRequest());
//
//        // Validate the MedicalDay in the database
//        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
//        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
//    }

//    @Test
//    @Transactional
//    void patchWithIdMismatchMedicalDay() throws Exception {
//        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
//        medicalDay.setId(count.incrementAndGet());
//
//        // Create the MedicalDay
//        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);
//
//        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
//        restMedicalDayMockMvc
//            .perform(
//                patch(ENTITY_API_URL_ID, count.incrementAndGet())
//                    .contentType("application/merge-patch+json")
//                    .content(TestUtil.convertObjectToJsonBytes(medicalDayDTO))
//            )
//            .andExpect(status().isBadRequest());
//
//        // Validate the MedicalDay in the database
//        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
//        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
//    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMedicalDay() throws Exception {
        int databaseSizeBeforeUpdate = medicalDayRepository.findAll().size();
        medicalDay.setId(count.incrementAndGet());

        // Create the MedicalDay
        MedicalDayDTO medicalDayDTO = MedicalDayMapper.toDTO(medicalDay);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMedicalDayMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(medicalDayDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MedicalDay in the database
        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMedicalDay() throws Exception {
        // Initialize the database
        medicalDayRepository.saveAndFlush(medicalDay);

        int databaseSizeBeforeDelete = medicalDayRepository.findAll().size();

        // Delete the medicalDay
        restMedicalDayMockMvc
            .perform(delete(ENTITY_API_URL_ID, medicalDay.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        // Validate the database contains one less item
        List<MedicalDay> medicalDayList = medicalDayRepository.findAll();
        assertThat(medicalDayList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
