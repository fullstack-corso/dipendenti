package it.fullstack.salus.domain;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import it.fullstack.salus.Application;
import it.fullstack.salus.domain.enumeration.Sesso;
import it.fullstack.salus.domain.enumeration.StatoLavorativo;
import it.fullstack.salus.domain.enumeration.TipoContrattoDipendente;
import it.fullstack.salus.repository.DipendenteRepository;
import it.fullstack.salus.services.DipendenteService;
import it.fullstack.salus.services.dto.UpdateDipendenteDTO;
import jakarta.persistence.EntityManager;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = Application.class)
@AutoConfigureMockMvc
class DipendenteControllerTest {

	@Autowired
	private MockMvc clientSimulator;

	@Autowired 
	DipendenteRepository repository;

	@Autowired
	DipendenteService dipendenteService;
	
	@Autowired
	EntityManager em;
	
	public static Dipendente createEntity(EntityManager em) {
        Dipendente dipendente = new Dipendente();
        dipendente.setCognome("Rossi");
		dipendente.setNome("Mario");
		dipendente.setCodiceFiscale("AAA");
		dipendente.setDataNascita(LocalDate.now().minusYears(60));
		dipendente.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dipendente.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dipendente.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dipendente.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dipendente.setSesso(Sesso.UOMO);
		dipendente.setScadenzaVisita(LocalDate.now().plusYears(2));
		dipendente.setEmail("aaabbb@gmail.com");
        em.persist(dipendente);
        return dipendente;
    } 



	private static  ObjectMapper mapper = createObjectMapper();

	private static ObjectMapper createObjectMapper() {
		ObjectMapper mapper =  new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.registerModule(new JavaTimeModule());
		return mapper;
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_Cognome_Dipendente_Status200() throws Exception, JsonProcessingException{
		testFindAllByCriteria("cognome.starts=ROS")
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].nome", is("Mario")))
		.andExpect(jsonPath("$.content[0].cognome", is("Rossi")))
		.andExpect(jsonPath("$.content[0].codiceFiscale", is("AAA")));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_Nome_Dipendente_Status200() throws Exception, JsonProcessingException{
		testFindAllByCriteria("nome.ends=sca")
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].nome", is("Francesca")))
		.andExpect(jsonPath("$.content[0].cognome", is("Bianchi")))
		.andExpect(jsonPath("$.content[0].codiceFiscale", is("BBB")));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_CF_Dipendente_Status200() throws Exception, JsonProcessingException{
		testFindAllByCriteria("codiceFiscale.equals=BBB")
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].nome", is("Francesca")))
		.andExpect(jsonPath("$.content[0].cognome", is("Bianchi")))
		.andExpect(jsonPath("$.content[0].codiceFiscale", is("BBB")));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_OVER_Dipendente_Status200() throws Exception, JsonProcessingException{
		testFindAllByCriteria("over=50")
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].nome", is("Mario")))
		.andExpect(jsonPath("$.content[0].cognome", is("Rossi")))
		.andExpect(jsonPath("$.content[0].codiceFiscale", is("AAA")));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_SCADENZA_AL_Dipendente_Status200() throws Exception, JsonProcessingException{
		testFindAllByCriteria("scadenzaVisitaAl.lessThanOrEqual="+LocalDate.now().plusYears(2).plusMonths(2).toString())
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].nome", is("Mario")))
		.andExpect(jsonPath("$.content[0].cognome", is("Rossi")))
		.andExpect(jsonPath("$.content[0].codiceFiscale", is("AAA")));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_SCADENZA_DAL_Dipendente_Status200() throws Exception, JsonProcessingException{
		testFindAllByCriteria("scadenzaVisitaDal.greaterThanOrEqual="+LocalDate.now().plusYears(2).plusMonths(2).toString())
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].nome", is("Francesca")))
		.andExpect(jsonPath("$.content[0].cognome", is("Bianchi")))
		.andExpect(jsonPath("$.content[0].codiceFiscale", is("BBB")));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_SCADENZA_DAL_AL_Dipendente_Status200() throws Exception, JsonProcessingException{
		testFindAllByCriteria("scadenzaVisitaDal.greaterThanOrEqual="+LocalDate.now().plusYears(2).plusMonths(2).toString()+"&scadenzaVisitaAl.lessThanOrEqual="+LocalDate.now().plusYears(5).plusMonths(2).toString())
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].nome", is("Francesca")))
		.andExpect(jsonPath("$.content[0].cognome", is("Bianchi")))
		.andExpect(jsonPath("$.content[0].codiceFiscale", is("BBB")));
	}

	@DirtiesContext
	@Test
	public void testFindAllByCriteria_CognomeInesistente() throws Exception, JsonProcessingException{
		testFindAllByCriteria("cognome.contains=NonEsiste")
		.andExpect(jsonPath("$.content",empty()));
	}


	@Test		//FIXME discrepanze tra UpdateDipendenteDTO e DipendenteDTO
	@DirtiesContext
	public void testCreate_HAPPY() throws Exception {
		UpdateDipendenteDTO dto = new UpdateDipendenteDTO();
		dto.setCognome("Rossi");
		dto.setNome("Mario");
		dto.setCodiceFiscale("AAA");
		dto.setDataNascita(LocalDate.now().minusYears(40));
		dto.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dto.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dto.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dto.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dto.setSesso(Sesso.UOMO);;

		clientSimulator.perform(post("/api/dipendente")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto)))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.cognome", is(dto.getCognome())))
		.andExpect(jsonPath("$.scadenzaVisita", is(LocalDate.now().toString())))
		.andExpect(jsonPath("$.codiceFiscale", is(dto.getCodiceFiscale())))
		.andExpect(jsonPath("$.dataNascita", is(dto.getDataNascita().toString())))
		.andExpect(jsonPath("$.nome", is(dto.getNome())))
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.dataAssunzioneDipendente", is(dto.getDataAssunzioneDipendente().toString())))
		.andExpect(jsonPath("$.dataPrimoContratto", is(dto.getDataPrimoContratto().toString())))
		//.andExpect(jsonPath("$.contratto", is(dto.getContratto())))  //controlla fixme sopra
		//.andExpect(jsonPath("$.statoAssunzione", is(dto.getStatoAssunzione())))
		.andExpect(jsonPath("$.sesso", is(dto.getSesso().toString())));
	}


	@Test		
	@DirtiesContext
	public void testCreate_UNHAPPY() throws Exception {
		UpdateDipendenteDTO dto = new UpdateDipendenteDTO();
		dto.setCognome("Rossi");
		dto.setNome("Mario");
		dto.setCodiceFiscale("AAA");
		//dto.setDataNascita(LocalDate.now().minusYears(40)); //unhappy test 
		dto.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dto.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dto.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dto.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dto.setSesso(Sesso.UOMO);;

		clientSimulator.perform(post("/api/dipendente")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto)))
		.andExpect(status().is4xxClientError());
	}

	@Test
	@DirtiesContext
	public void testUpdate() throws Exception {
		UpdateDipendenteDTO dto = new UpdateDipendenteDTO();
		dto.setCognome("Rossi");
		dto.setNome("Mario");
		dto.setCodiceFiscale("AAA");
		dto.setDataNascita(LocalDate.now().minusYears(40));
		dto.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dto.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dto.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dto.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dto.setSesso(Sesso.UOMO);;

		Long dipendenteIdSaved  = dipendenteService.save(dto).getId();

		dto.setCognome("NuovoCognome");

		clientSimulator.perform(put("/api/dipendente/" + dipendenteIdSaved)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto)))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id", is(dipendenteIdSaved.intValue())))
		.andExpect(jsonPath("$.cognome", is(dto.getCognome())))
		.andExpect(jsonPath("$.scadenzaVisita", is(LocalDate.now().toString())));
	}

	
	@Test		
	@DirtiesContext
	public void testDELETE_HAPPY() throws Exception {
		UpdateDipendenteDTO dto = new UpdateDipendenteDTO();
		dto.setCognome("Rossi");
		dto.setNome("Mario");
		dto.setCodiceFiscale("AAA");
		dto.setDataNascita(LocalDate.now().minusYears(40));
		dto.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dto.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dto.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dto.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dto.setSesso(Sesso.UOMO);;

		Long dipendenteIdSaved  = dipendenteService.save(dto).getId();
		
		clientSimulator.perform(delete("/api/dipendente/"+dipendenteIdSaved)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
		
		clientSimulator.perform(get("/api/dipendente/"+dipendenteIdSaved)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError());
		
		clientSimulator.perform(get("/api/dipendente")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.content",hasSize(0)));
	}
	
	@Test		
	@DirtiesContext
	public void testDELETE_UNHAPPY() throws Exception {
		UpdateDipendenteDTO dto = new UpdateDipendenteDTO();
		dto.setCognome("Rossi");
		dto.setNome("Mario");
		dto.setCodiceFiscale("AAA");
		dto.setDataNascita(LocalDate.now().minusYears(40));
		dto.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dto.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dto.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dto.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dto.setSesso(Sesso.UOMO);;

		Long dipendenteIdSaved  = dipendenteService.save(dto).getId()+1L; //setto id diverso dal reale
		
		clientSimulator.perform(delete("/api/dipendente/"+dipendenteIdSaved)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
		
		
		clientSimulator.perform(get("/api/dipendente")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.content",hasSize(1)));
	}
	

	public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
		return mapper.writeValueAsBytes(object);
	}

	private ResultActions testFindAllByCriteria(String criteria) throws Exception, JsonProcessingException{

		Dipendente dipendente = new Dipendente();
		dipendente.setCognome("Rossi");
		dipendente.setNome("Mario");
		dipendente.setCodiceFiscale("AAA");
		dipendente.setDataNascita(LocalDate.now().minusYears(60));
		dipendente.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dipendente.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dipendente.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dipendente.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dipendente.setSesso(Sesso.UOMO);
		dipendente.setScadenzaVisita(LocalDate.now().plusYears(2));
		repository.save(dipendente);

		Dipendente dipendente2 = new Dipendente();
		dipendente2.setCognome("Bianchi");
		dipendente2.setNome("Francesca");
		dipendente2.setCodiceFiscale("BBB");
		dipendente2.setDataNascita(LocalDate.now().minusYears(20));
		dipendente2.setDataAssunzioneDipendente(LocalDate.now().minusYears(2));
		dipendente2.setDataPrimoContratto(LocalDate.now().minusYears(3));
		dipendente2.setContratto(TipoContrattoDipendente.APPRENDISTATO);
		dipendente2.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dipendente2.setSesso(Sesso.ALTRO);
		dipendente2.setScadenzaVisita(LocalDate.now().plusYears(5));
		repository.save(dipendente2);

		return clientSimulator.perform(get("/api/dipendente?"+ criteria)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

	}
}