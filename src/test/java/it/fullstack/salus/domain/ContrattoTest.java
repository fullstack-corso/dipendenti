package it.fullstack.salus.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;


import it.fullstack.salus.Application;
import it.fullstack.salus.repository.ContrattoRepository;
import it.fullstack.salus.services.ContrattoService;
import it.fullstack.salus.services.dto.UpdateContrattoDTO;
import it.fullstack.salus.services.mapper.ContrattoMapper;
import jakarta.persistence.EntityManager;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Application.class)
//@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
public class ContrattoTest {

	private static final LocalDate DEFAULT_DATA_INIZIO = LocalDate.ofEpochDay(0L);
	private static final LocalDate UPDATED_DATA_INIZIO = LocalDate.now(ZoneId.systemDefault());
	private static final LocalDate SMALLER_DATA_INIZIO = LocalDate.ofEpochDay(-1L);

	private static final LocalDate DEFAULT_DATA_FINE = LocalDate.ofEpochDay(0L);
	private static final LocalDate UPDATED_DATA_FINE = LocalDate.now().plusDays(1);
	private static final LocalDate SMALLER_DATA_FINE = LocalDate.ofEpochDay(-1L);

	private static final LocalDateTime DEFAULT_DATA_CANCELLAZIONE = LocalDateTime.of(LocalDate.ofEpochDay(0L), LocalTime.MIDNIGHT);
	private static final LocalDateTime UPDATED_DATA_CANCELLAZIONE = LocalDateTime.now(ZoneId.systemDefault());
	private static final LocalDateTime SMALLER_DATA_CANCELLAZIONE = LocalDateTime.of(LocalDate.ofEpochDay(-1L), LocalTime.MIDNIGHT);

	private static final String ENTITY_API_URL = "/api/contratto/";
	private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

	private static Random random = new Random();
	private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

	@Autowired
	private ContrattoRepository contrattoRepository;

	@Autowired
	private ContrattoService contrattoService;

	@Autowired
	private EntityManager em;

	@Autowired
	private MockMvc restContrattoMockMvc;
	
	private Contratto contratto;

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static Contratto createEntity(EntityManager em) {
		Contratto contratto = new Contratto();
		contratto.setDataInizio(DEFAULT_DATA_INIZIO);
		contratto.setDataFine(UPDATED_DATA_FINE);
		// Add required entity
		Medico medico;
//		if (TestUtil.findAll(em, Medico.class).isEmpty()) {
			//            medico = MedicoResourceIT.createEntity(em);
			//            em.persist(medico);
			//            em.flush();
			//        } else {
			//          //  medico = TestUtil.findAll(em, Medico.class).get(0);
			//        }
			medico = new Medico();
			medico.setCodiceFiscale("AAAAAAA");
			medico.setNome("Daniele");
			medico.setCognome("Rossi");
			medico.setDataNascita(DEFAULT_DATA_INIZIO);
			em.persist(medico);
			em.flush();
			contratto.setMedico(medico);
			// Add required entity
			Sede sede;
			if (TestUtil.findAll(em, Sede.class).isEmpty()) {
				sede = SedeTest.createEntity(em);
				em.persist(sede);
				em.flush();
			} else {
				sede = TestUtil.findAll(em, Sede.class).get(0);
			}
			contratto.setSede(sede);
			return contratto;
		}
	
	
	public static UpdateContrattoDTO createUpdateContrattoDTO(EntityManager em) {
		UpdateContrattoDTO updateContrattoDTO = new UpdateContrattoDTO();
		updateContrattoDTO.setDataInizio(DEFAULT_DATA_INIZIO);
		updateContrattoDTO.setDataFine(UPDATED_DATA_FINE);
		Sede sede;
		if (TestUtil.findAll(em, Sede.class).isEmpty()) {
			sede = SedeTest.createEntity(em);
			em.persist(sede);
			em.flush();
		} else {
			sede = TestUtil.findAll(em, Sede.class).get(0);
		}
		updateContrattoDTO.setSedeId(sede.getId());

		Medico medico = new Medico();
		medico.setCodiceFiscale("AAA");
		medico.setDataNascita(DEFAULT_DATA_FINE);
		medico.setNome("Alessio");
		medico.setCognome("Erb");
		em.persist(medico);
		em.flush();
		updateContrattoDTO.setMedicoId(medico.getId());

		return updateContrattoDTO;
	}


	/**
	 * Create an updated entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static Contratto createUpdatedEntity(EntityManager em) {
		Contratto contratto = new Contratto();
				contratto.setDataInizio(UPDATED_DATA_INIZIO);
				contratto.setDataFine(UPDATED_DATA_FINE);
				//contratto.setDataCancellazione(UPDATED_DATA_CANCELLAZIONE);
		// Add required entity
		Medico medico;
//		if (TestUtil.findAll(em, Medico.class).isEmpty()) {
//			medico = MedicoResourceIT.createUpdatedEntity(em);
//			em.persist(medico);
//			em.flush();
//		} else {
//			medico = TestUtil.findAll(em, Medico.class).get(0);
//		}
		medico = new Medico();
		medico.setCodiceFiscale("BBBB");
		medico.setNome("Daniele");
		medico.setCognome("Verdi");
		medico.setDataNascita(DEFAULT_DATA_INIZIO);
		em.persist(medico);
		em.flush();
		contratto.setMedico(medico);
		// Add required entity
		Sede sede;
		if (TestUtil.findAll(em, Sede.class).isEmpty()) {
			sede = SedeTest.createUpdatedEntity(em);
			em.persist(sede);
			em.flush();
		} else {
			sede = TestUtil.findAll(em, Sede.class).get(0);
		}
		contratto.setSede(sede);
		return contratto;
	}

	@BeforeEach
	public void initTest() {
		contratto = createEntity(em);
	}

	@Test
	@Transactional
	void createContratto() throws Exception {
		int databaseSizeBeforeCreate = contrattoRepository.findAll().size();
		// Create the Contratto
		UpdateContrattoDTO contrattoDTO = createUpdateContrattoDTO(em);
		restContrattoMockMvc
		.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrattoDTO)))
		.andExpect(status().isOk());

		// Validate the Contratto in the database
		List<Contratto> contrattoList = contrattoRepository.findAll();
		assertThat(contrattoList).hasSize(databaseSizeBeforeCreate + 1);
		Contratto testContratto = contrattoList.get(contrattoList.size() - 1);
		assertThat(testContratto.getDataInizio()).isEqualTo(DEFAULT_DATA_INIZIO);
		assertThat(testContratto.getDataFine()).isEqualTo(UPDATED_DATA_FINE);
		// assertThat(testContratto.getDataCancellazione()).isEqualTo(DEFAULT_DATA_CANCELLAZIONE);
	}


	@Test
    @Transactional
    void getAllContrattos() throws Exception {
        // Initialize the database
        contrattoRepository.saveAndFlush(contratto);

        // Get all the contrattoList
        restContrattoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.content[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.content[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())));
           // .andExpect(jsonPath("$.[*].dataCancellazione").value(hasItem(DEFAULT_DATA_CANCELLAZIONE.toString())));
    }
	
	@Test
    @Transactional
    void getContratto() throws Exception {
        // Initialize the database
        contrattoRepository.saveAndFlush(contratto);

        // Get the contratto
        restContrattoMockMvc
            .perform(get(ENTITY_API_URL_ID, contratto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contratto.getId().intValue()))
            .andExpect(jsonPath("$.dataInizio").value(DEFAULT_DATA_INIZIO.toString()))
            .andExpect(jsonPath("$.dataFine").value(DEFAULT_DATA_FINE.toString()));
           // .andExpect(jsonPath("$.dataCancellazione").value(DEFAULT_DATA_CANCELLAZIONE.toString()));
    }
	
	private void defaultContrattoShouldBeFound(String filter) throws Exception {
        restContrattoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.content[*].id").value(hasItem(contratto.getId().intValue())))
            .andExpect(jsonPath("$.content[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.content[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())));
            //.andExpect(jsonPath("$.[*].dataCancellazione").value(hasItem(DEFAULT_DATA_CANCELLAZIONE.toString())));
    }
	
	 @Test
	    @Transactional
	    void getAllContrattosBySedeIsEqualToSomething() throws Exception {
	        Sede sede;
	        if (TestUtil.findAll(em, Sede.class).isEmpty()) {
	            contrattoRepository.saveAndFlush(contratto);
	            sede = SedeTest.createEntity(em);
	        } else {
	            sede = TestUtil.findAll(em, Sede.class).get(0);
	        }
	        em.persist(sede);
	        em.flush();
	        contratto.setSede(sede);
	        contrattoRepository.saveAndFlush(contratto);
	        Long sedeId = sede.getId();

	        // Get all the contrattoList where sede equals to sedeId
	        defaultContrattoShouldBeFound("sedeId.equals=" + sedeId);
	    }
 
	 @Test
	    @Transactional
	    void getNonExistingContratto() throws Exception {
	        // Get the contratto
	        restContrattoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isBadRequest());
	    }
	 
	 @Test
	    @Transactional
	    void putExistingContratto() throws Exception {
	        // Initialize the database
	        contrattoRepository.saveAndFlush(contratto);

	        int databaseSizeBeforeUpdate = contrattoRepository.findAll().size();

	        // Update the contratto
	        Contratto updatedContratto = contrattoRepository.findById(contratto.getId()).get();
	        // Disconnect from session so that the updates on updatedContratto are not directly saved in db
	        em.detach(updatedContratto);
	        updatedContratto.setDataInizio(UPDATED_DATA_INIZIO);
	        updatedContratto.setDataFine(UPDATED_DATA_FINE);//aggiungere data cancellazione;
	        UpdateContrattoDTO updateContrattoDTO = ContrattoMapper.toUpdateDTO(updatedContratto);  
	        
	        restContrattoMockMvc
	            .perform(
	                put(ENTITY_API_URL_ID, updatedContratto.getId())
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(TestUtil.convertObjectToJsonBytes(updateContrattoDTO))
	            )
	            .andExpect(status().isOk());

	        // Validate the Contratto in the database
	        List<Contratto> contrattoList = contrattoRepository.findAll();
	        assertThat(contrattoList).hasSize(databaseSizeBeforeUpdate);
	        Contratto testContratto = contrattoList.get(contrattoList.size() - 1);
	        assertThat(testContratto.getDataInizio()).isEqualTo(UPDATED_DATA_INIZIO);
	        assertThat(testContratto.getDataFine()).isEqualTo(UPDATED_DATA_FINE);
	        //assertThat(testContratto.getDataCancellazione()).isEqualTo(UPDATED_DATA_CANCELLAZIONE);
	    }
	 
	 
	 @Test
	    @Transactional
	    void putNonExistingContratto() throws Exception {
	        int databaseSizeBeforeUpdate = contrattoRepository.findAll().size();
	        contratto.setId(count.incrementAndGet());

	        // Create the Contratto
	        UpdateContrattoDTO contrattoDTO = ContrattoMapper.toUpdateDTO(contratto);

	        // If the entity doesn't have an ID, it will throw BadRequestAlertException
	        restContrattoMockMvc
	            .perform(
	                put(ENTITY_API_URL_ID, contratto.getId())
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(TestUtil.convertObjectToJsonBytes(contrattoDTO))
	            )
	            .andExpect(status().isBadRequest());

	        // Validate the Contratto in the database
	        List<Contratto> contrattoList = contrattoRepository.findAll();
	        assertThat(contrattoList).hasSize(databaseSizeBeforeUpdate);
	    }
	 
	 @Test
	    @Transactional
	    void putWithIdMismatchContratto() throws Exception {
	        int databaseSizeBeforeUpdate = contrattoRepository.findAll().size();
	        contratto.setId(count.incrementAndGet());

	        // Create the Contratto
	        UpdateContrattoDTO contrattoDTO = ContrattoMapper.toUpdateDTO(contratto);

	        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
	        restContrattoMockMvc
	            .perform(
	                put(ENTITY_API_URL_ID, count.incrementAndGet())
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(TestUtil.convertObjectToJsonBytes(contrattoDTO))
	            )
	            .andExpect(status().isBadRequest());

	        // Validate the Contratto in the database
	        List<Contratto> contrattoList = contrattoRepository.findAll();
	        assertThat(contrattoList).hasSize(databaseSizeBeforeUpdate);
	    }
	 
	 @Test
	    @Transactional
	    void putWithMissingIdPathParamContratto() throws Exception {
	        int databaseSizeBeforeUpdate = contrattoRepository.findAll().size();
	        contratto.setId(count.incrementAndGet());

	        // Create the Contratto
	        UpdateContrattoDTO contrattoDTO = ContrattoMapper.toUpdateDTO(contratto);

	        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
	        restContrattoMockMvc
	            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrattoDTO)))
	            .andExpect(status().isMethodNotAllowed());

	        // Validate the Contratto in the database
	        List<Contratto> contrattoList = contrattoRepository.findAll();
	        assertThat(contrattoList).hasSize(databaseSizeBeforeUpdate);
	    }
	 @Test
	    @Transactional
	    void deleteContratto() throws Exception {
	        // Initialize the database
	        contrattoRepository.saveAndFlush(contratto);

	        int databaseSizeBeforeDelete = contrattoRepository.findAll().size();

	        // Delete the contratto
	        restContrattoMockMvc
	            .perform(delete(ENTITY_API_URL_ID, contratto.getId()).accept(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk());

	        // Validate the database contains one less item
	        List<Contratto> contrattoList = contrattoRepository.findAll();
	        assertThat(contrattoList).hasSize(databaseSizeBeforeDelete - 1);
	    }
	 
	
	}
