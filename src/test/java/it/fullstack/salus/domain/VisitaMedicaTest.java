package it.fullstack.salus.domain;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.fullstack.salus.Application;
import it.fullstack.salus.domain.enumeration.StatoLavorativo;
import it.fullstack.salus.domain.enumeration.StatoVisitaMedica;
import it.fullstack.salus.domain.enumeration.TipoContrattoDipendente;
import it.fullstack.salus.repository.ContrattoRepository;
import it.fullstack.salus.repository.DipendenteRepository;
import it.fullstack.salus.repository.MedicalDayRepository;
import it.fullstack.salus.repository.MedicoRepository;
import it.fullstack.salus.repository.SedeRepository;
import it.fullstack.salus.services.VisitaMedicaService;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaDTO;
import it.fullstack.salus.services.dto.VisitaMedicaDTO;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = Application.class)
@AutoConfigureMockMvc
class VisitaMedicaTest {
	@Autowired
	private MockMvc clientSimulator;
	
	@Autowired
	private DipendenteRepository dipendenteRepository;
	
	@Autowired
	private MedicalDayRepository medicalDayRepository;
	
	@Autowired 
	private ContrattoRepository contrattoRepository;
	
	@Autowired 
	private MedicoRepository repositoryMedico;

	@Autowired
	private SedeRepository repositorySede;
	
	@Autowired
	private VisitaMedicaService visitaMedicaService;
	
	@Autowired
	private ObjectMapper mapper;
	
	
	public UpdateVisitaMedicaDTO setup() { 
		MedicalDay medicalDay = medicalDayCreate();
		UpdateVisitaMedicaDTO vm = new UpdateVisitaMedicaDTO();
		vm.setIdMedicalDay(medicalDay.getId());
		vm.setIdDipendente(1L);
		vm.setOraVisita(LocalTime.of(10,30,30));
		return vm;
	}
	
	public MedicalDay medicalDayCreate() {
		Dipendente dip = new Dipendente();
		dip.setNome("Giacomo");
		dip.setCognome("Ferrara");
		dip.setCodiceFiscale("GCM");
		dip.setDataNascita(LocalDate.now().minusYears(30));
		dip.setDataPrimoContratto(LocalDate.now().minusYears(30));
		dip.setDataAssunzioneDipendente(LocalDate.now().minusYears(30));
		dip.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dip.setContratto(TipoContrattoDipendente.INDETERMINATO);
		dipendenteRepository.save(dip);
		
		Dipendente dip2 = new Dipendente();
		dip2.setNome("Giacomo");
		dip2.setCognome("Ferrara");
		dip2.setCodiceFiscale("SSS");
		dip2.setDataNascita(LocalDate.now().minusYears(60));
		dip2.setDataPrimoContratto(LocalDate.now().minusYears(30));
		dip2.setDataAssunzioneDipendente(LocalDate.now().minusYears(30));
		dip2.setStatoAssunzione(StatoLavorativo.ATTIVO);
		dip2.setContratto(TipoContrattoDipendente.INDETERMINATO);
		dipendenteRepository.save(dip2);
		
		Medico medico =  new Medico();
		medico.setNome("Bello");
		medico.setCognome("Bello");
		medico.setCodiceFiscale("BBB");
		medico.setDataNascita(LocalDate.now().minusYears(30));
		repositoryMedico.save(medico);
		
		Sede sede = new Sede();
		sede.setProvincia("Claudio");
		sede.setDenominazione("Sede bella");
		repositorySede.save(sede);
		
		Contratto contratto = new Contratto();
		contratto.setDataInizio(LocalDate.now().minusYears(3));
		contratto.setDataFine(LocalDate.now().plusYears(15));
		contratto.setMedico(medico);
		contratto.setSede(sede);
		contrattoRepository.save(contratto);
		
		MedicalDay medicalDay = new MedicalDay();
		medicalDay.setData(LocalDate.now().plusDays(5));
		medicalDay.setContratto(contratto);
		medicalDayRepository.save(medicalDay);
		
		return medicalDay;
	}
	
	@Test 
	@DirtiesContext
	public void testCreate_Status200() throws JsonProcessingException, Exception {
		UpdateVisitaMedicaDTO vm = setup();
		
		clientSimulator.perform(post("/api/visita-medica")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(vm)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.oraVisita",is("10:30:30")))
				.andExpect(jsonPath("$.statoVisitaMedica",is("INSERITA")))
				.andExpect(jsonPath("$.medicalDay.statoMedicalDay",is("IN_LAVORAZIONE")));
	}
	
	@Test
	@DirtiesContext
	public void testFindAllByCriteriaDataInizio_Status200() throws Exception {
		UpdateVisitaMedicaDTO vm = setup();
		visitaMedicaService.create(vm);
		clientSimulator.perform(get("/api/visita-medica?dataMedicalDayInizio=" + LocalDate.now().plusDays(5))
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].dipendente.nome",is("Giacomo")));
	}
	
	@Test
	@DirtiesContext
	public void testDelete_Status200() throws Exception {
		UpdateVisitaMedicaDTO vm = setup();
		VisitaMedicaDTO visitaMedica = visitaMedicaService.create(vm);
		
		clientSimulator.perform(delete("/api/visita-medica/" + visitaMedica.getId())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());	
	}
	
	@Test
	@DirtiesContext
	public void testDelete_MedicalDayTerminato_BadRequest400() throws Exception {
		UpdateVisitaMedicaDTO vm = setup();
		VisitaMedicaDTO visitaMedica = visitaMedicaService.create(vm);
		vm.setStatoVisitaMedica(StatoVisitaMedica.EFFETTUATA);
		visitaMedicaService.update(vm, visitaMedica.getId());
		
		clientSimulator.perform(delete("/api/visita-medica/" + visitaMedica.getId())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());			
	}
	
	@Test
	@DirtiesContext
	public void testDelete_CambioStatoPrecedente_Status200() throws Exception {
		
		//inseriamo una prima visita medica
		UpdateVisitaMedicaDTO vm = setup();
		VisitaMedicaDTO visitaDTO = visitaMedicaService.create(vm);
		//Rimuoviamo la visita medica inserita
		visitaMedicaService.deleteById(visitaDTO.getId());
		
		//Verifichiamo il cambiamento del MedicalDay in IN_PRENOTAZIONE
		clientSimulator.perform(get("/api/medical-day/" + vm.getIdMedicalDay())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.visiteMediche", hasSize(0)))
		.andExpect(jsonPath("$.statoMedicalDay",is("IN_LAVORAZIONE")));
	}
	
	@Test
	@DirtiesContext
	public void testMedicalDayRimaneStato_Status200() throws Exception {
		UpdateVisitaMedicaDTO vm = setup();
		
		//Aggiungiamo due visite mediche
		visitaMedicaService.create(vm);
		visitaMedicaService.create(vm);
		
		//Rimuoviamo una visita medica
		visitaMedicaService.deleteById(1L);
		
		//Verifichiamo che il MedicalDay rimanga IN_LAVORAZIONE
		clientSimulator.perform(get("/api/medical-day/" + vm.getIdMedicalDay())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.visiteMediche", hasSize(1)))
		.andExpect(jsonPath("$.statoMedicalDay",is("IN_LAVORAZIONE")));
	}
	
	@Test
	@DirtiesContext
	public void testCambioStatoVisitaMedicaEffettuata_Status200() throws Exception {

		UpdateVisitaMedicaDTO vm = setup();
		VisitaMedicaDTO visitaDTO = visitaMedicaService.create(vm);
		vm.setStatoVisitaMedica(StatoVisitaMedica.EFFETTUATA);
		LocalDate dataNascitaCinquantAnniFa = LocalDate.now().minusYears(50);

		//condizioni
		clientSimulator.perform(put("/api/visita-medica/" + visitaDTO.getId())
				.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(vm)))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.statoVisitaMedica",is("EFFETTUATA")))
		.andExpect(jsonPath("$.dipendente.dataNascita", is(greaterThanOrEqualTo((dataNascitaCinquantAnniFa).toString()))))
		.andExpect(jsonPath("$.dipendente.scadenzaVisita",is(LocalDate.now().plusYears(5).toString())));
		
		clientSimulator.perform(get("/api/evento?" + visitaDTO.getDipendente().getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.content[0].dipendente.nome",is("Giacomo")));
	}
	
	@Test
	@DirtiesContext
	public void testCambioStatoVisitaMedicaEffettuata_Over50_Status200() throws Exception {
		//precondizioni
		UpdateVisitaMedicaDTO vm = setup();
		vm.setStatoVisitaMedica(StatoVisitaMedica.EFFETTUATA);
		vm.setIdDipendente(2L);
		VisitaMedicaDTO visitaDTO = visitaMedicaService.create(vm);
		LocalDate dataNascitaCinquantAnniFa = LocalDate.now().minusYears(50);


		//condizioni
		clientSimulator.perform(put("/api/visita-medica/" + visitaDTO.getId())
				.contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(vm)))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.statoVisitaMedica",is("EFFETTUATA")))
		.andExpect(jsonPath("$.dipendente.dataNascita", is(lessThanOrEqualTo((dataNascitaCinquantAnniFa).toString()))))
		.andExpect(jsonPath("$.dipendente.scadenzaVisita",is(LocalDate.now().plusYears(2).toString())));
	}
		
	@Test
	@DirtiesContext
	public void testCambioStato_MedicalDayCompleto_Status200() throws Exception {
		UpdateVisitaMedicaDTO vm = setup();
		VisitaMedicaDTO visitaMedica = visitaMedicaService.create(vm);
		vm.setStatoVisitaMedica(StatoVisitaMedica.EFFETTUATA);
		visitaMedicaService.update(vm, visitaMedica.getId());
		
		//verifichiamo il cambiamento di stato del MedicalDay in COMPLETO
		clientSimulator.perform(get("/api/medical-day/" + vm.getIdMedicalDay())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.statoMedicalDay",is("COMPLETO")));
	}
	
	@Test
	@DirtiesContext
	public void testFindAllByCriteriaStato_Status200() throws Exception {
		UpdateVisitaMedicaDTO vm = setup();
		visitaMedicaService.create(vm);
		
		clientSimulator.perform(get("/api/visita-medica?statoVisitaMedica=INSERITA")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.content",hasSize(1)))
		.andExpect(jsonPath("$.content[0].statoVisitaMedica",is("INSERITA")));	
	}
	
	public void testFindAll_Status200() throws Exception {
		UpdateVisitaMedicaDTO vm = setup();
		visitaMedicaService.create(vm);
		clientSimulator.perform(get("/api/visita-medica")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
	}
	
	@Test
	void testOra() {
		MedicalDay medicalDay = new MedicalDay();
		medicalDay.setData(LocalDate.now().plusDays(3));
		
		VisitaMedica visita = new VisitaMedica();
		visita.setMedicalDay(medicalDay);
		visita.setOraVisita(LocalTime.now().plusHours(2));
		
		LocalDateTime dataInDb = visita.getData();
		LocalDateTime risultato = LocalDateTime.now().plusDays(3).plusHours(2);
		assertEquals(risultato, dataInDb);
	}
}