INSERT INTO sede (cap,citta,civico,denominazione,provincia,via) VALUES
     ('00100','Roma','50','Roma I','Roma','Via Tasso'),
     ('80100','Napoli','1','Napoli II','Napoli','Via Chiaia');

INSERT INTO dipendente (codice_fiscale,cognome,data_nascita,email,nome,sesso,contratto,data_assunzione_dipendente,data_primo_contratto,scadenza_visita,stato_assunzione, distacco_id, mansione_id, sede_id, ultima_visita) VALUES
     ('BBB','Rossi','1971-01-01',NULL,'Mario','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2024-01-01','ATTIVO',NULL,NULL,1, '2021-06-25'),
     ('DDD','Bianchi','2000-01-01',NULL,'Luca','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2023-06-01','ATTIVO',NULL,NULL,2, '2018-01-01'),
     ('EEE','Verdi','1973-01-22',NULL,'Marco','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2023-06-23','ATTIVO',NULL,NULL,2, '2022-09-23'),
     ('FFF','Gialli','1973-01-22',NULL,'Francesco','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2026-06-23','ATTIVO',NULL,NULL,2, '2022-09-23'),
     ('RRR','Viola','1988-01-01',NULL,'Mario','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2024-01-01','ATTIVO',NULL,NULL,2, '2021-06-25'),
     ('SSS','Rosa','2000-01-01',NULL,'Luca','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2023-06-01','ATTIVO',NULL,NULL,2, '2018-01-01'),
     ('TTT','Marrone','1973-01-22',NULL,'Marco','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2023-06-23','ATTIVO',NULL,NULL,2, '2022-09-23'),
     ('QQQ','Blu','1973-01-22',NULL,'Francesco','UOMO','INDETERMINATO','2020-01-01','2020-01-01','2026-06-23','ATTIVO',NULL,NULL,2, '2022-09-23');
     


INSERT INTO medico (codice_fiscale,cognome,data_nascita,email,nome,numero_telefono,sesso,societa) VALUES
     ('XXX','Dr. Gialli','1970-01-01',NULL,'Fabrizio','060606','UOMO',0),
     ('AAA','Dr. Verdi','1975-01-01',NULL,'Maria','060606','DONNA',0);

INSERT INTO contratto (data_fine,data_inizio,medico_id,sede_id) VALUES
     ('2024-01-01','2020-01-01',1,1),
     ('2024-01-01','2020-01-01',1,2),
     ('2024-01-01','2020-01-01',2,1),
     ('2025-01-01','2021-01-01',2,2);





INSERT INTO medical_day (data, stato_medical_day, contratto_id) VALUES
     ('2027-03-23', 'IN_LAVORAZIONE', 1),
     ('2024-04-24', 'IN_LAVORAZIONE', 2),
     ('2024-05-25', 'IN_LAVORAZIONE', 1),
     ('2023-05-30', 'COMPLETO', 4),
     ('2023-05-27', 'COMPLETO', 4),
     ('2023-05-26', 'IN_LAVORAZIONE', 4),
     ('2023-05-25', 'IN_LAVORAZIONE', 4);

--INSERT INTO visita_medica (idoneo, ora_fine_visita,ora_visita, dipendente_id, stato_visita_medica,certificato_idoneita_id,medical_day_id,prescrizione_id) values
--	(1,'12:30:00','10:30:00',5, 'INSERITA',null,1,null),
--	(1,'11:30:00',6, 'INSERITA',1,6,null),
--	(1,'12:30:00',7, 'NON_EFFETTUATA',1,6,null),
--    (1,'13:30:00',8, 'EFFETTUATA',1,6,null),
--    (1,'13:30:00',7, 'EFFETTUATA',1,6,null),
--    (1,'13:30:00',7, 'EFFETTUATA',1,6,null);
--      
--INSERT INTO certificato_idoneita (idoneo, patologia,visita_medica_id) VALUES
--     ( true, 'asma',1);


     

	
     
     
     
