package it.fullstack.salus.repository;

import org.springframework.data.jpa.domain.Specification;

import it.fullstack.salus.domain.Contratto;
import it.fullstack.salus.domain.Contratto_;
import it.fullstack.salus.domain.Medico;
import it.fullstack.salus.domain.Medico_;
import it.fullstack.salus.domain.Sede;
import it.fullstack.salus.domain.Sede_;
import jakarta.persistence.criteria.Join;



public class ContrattoSpecifications {

	public static Specification<Contratto> likeMedicoCognome(String cognome){
		
		return (root,query,cb) ->{
			Join <Contratto,Medico> join=root.join(Contratto_.MEDICO);			
			return cb.like(join.get(Medico_.COGNOME), "%"+cognome+"%");
		};
	}
	
	public static Specification<Contratto> likeDenominazioneSede(String nomeSede){
		
		return (root,query,cb) ->{
			Join<Contratto,Sede> join=root.join(Contratto_.SEDE);			
			return cb.like(join.get(Sede_.DENOMINAZIONE), "%"+nomeSede+"%");
		};
	}
	
	public static Specification<Contratto> likeProvinciaSede(String provinciaSede){
		
		return (root,query,cb) ->{
			Join<Contratto,Sede> join=root.join(Contratto_.SEDE);			
			return cb.like(join.get(Sede_.PROVINCIA), "%"+provinciaSede+"%");
		};
	}
	
	
	
}
