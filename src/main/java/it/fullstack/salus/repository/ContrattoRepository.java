package it.fullstack.salus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import it.fullstack.salus.domain.Contratto;


@Repository
public interface ContrattoRepository extends JpaRepository<Contratto, Long>,JpaSpecificationExecutor<Contratto> {

}
