package it.fullstack.salus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.fullstack.salus.domain.Dipendente;

@Repository
public interface DipendenteRepository extends JpaRepository<Dipendente, Long>, JpaSpecificationExecutor<Dipendente> {
	
	@Modifying
	@Query("UPDATE Dipendente d SET d.scadenzaVisita = null")
    void resetScadenzaVisita();
}
