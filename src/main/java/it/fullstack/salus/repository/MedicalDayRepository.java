package it.fullstack.salus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fullstack.salus.domain.MedicalDay;


@Repository
public interface MedicalDayRepository extends JpaRepository<MedicalDay, Long>,JpaSpecificationExecutor<MedicalDay> {

}
