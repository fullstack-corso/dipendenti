package it.fullstack.salus.services;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import it.fullstack.salus.domain.Dipendente;
import it.fullstack.salus.domain.MedicalDay;
import it.fullstack.salus.domain.MedicalDay_;
import it.fullstack.salus.domain.VisitaMedica;
import it.fullstack.salus.domain.VisitaMedica_;
import it.fullstack.salus.domain.enumeration.StatoVisitaMedica;
import it.fullstack.salus.domain.enumeration.TipologiaEvento;
import it.fullstack.salus.repository.DipendenteRepository;
import it.fullstack.salus.repository.MedicalDayRepository;
import it.fullstack.salus.repository.VisitaMedicaRepository;
import it.fullstack.salus.services.dto.DocumentDTO;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaCertificatoDTO;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCaledarioDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCertificatoDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCriteria;
import it.fullstack.salus.services.dto.VisitaMedicaDTO;
import it.fullstack.salus.services.mapper.VisitaMedicaMapper;
import it.fullstack.salus.web.rest.DocumentClient;
import it.fullstack.salus.web.rest.errors.BadRequestException;
import it.fullstack.sdk.core.service.QueryService;


@Service
public class VisitaMedicaService extends QueryService<VisitaMedica>{

	@Autowired
	VisitaMedicaRepository visiteMedicheRepository;

	@Autowired
	MedicalDayRepository medicalDayRepository;

	@Autowired
	DipendenteRepository dipendenteRepository;

	@Autowired
	EventoService eventoService;

	@Autowired
	DocumentClient documentClient;

	// FIXME Controllo sull'orario se è già occupato
	@Transactional
	public VisitaMedicaDTO create(UpdateVisitaMedicaDTO visitaMedicaDTO) {
		// Passare a entity il DTO
		VisitaMedica visitaMedica = VisitaMedicaMapper.toEntity(visitaMedicaDTO);
		// Settare i valori, o eccezione

		MedicalDay medicalDay = medicalDayRepository
				.findById(visitaMedicaDTO.getIdMedicalDay())
				.orElseThrow(() -> new BadRequestException("Il medical day con id %s non esiste",
						visitaMedicaDTO.getIdMedicalDay()));

		visitaMedica.setDipendente(dipendenteRepository.findById(visitaMedicaDTO.getIdDipendente())
				.orElseThrow(() -> new BadRequestException("Il dipendente con id %s non esiste",
						visitaMedicaDTO.getIdDipendente())));

		visitaMedica.setMedicalDay(medicalDay);

		visitaMedica.setStatoVisitaMedica(StatoVisitaMedica.INSERITA);

		VisitaMedica savedVisita = visiteMedicheRepository.save(visitaMedica);

		medicalDay.aggiornamentoStato();

		medicalDayRepository.save(medicalDay);

		//CREAZIONE evento, nuovo medical day assegnato all'utente
		eventoService.createEvento(visitaMedica.getMedicalDay(), visitaMedica.getDipendente(), TipologiaEvento.ASSEGNATO_IL_MEDICALDAY);

		return VisitaMedicaMapper.toDTO(savedVisita);
	}

	@Transactional(readOnly = true)
	public Page<VisitaMedicaCaledarioDTO> findAllCalendario(VisitaMedicaCriteria criteria, Pageable page) {
		return visiteMedicheRepository.findAll(createSpecification(criteria), page)
				.map(VisitaMedicaMapper::toDTOCalendario);
	}

	@Transactional(readOnly = true)
	public Page<VisitaMedicaDTO> findAll(VisitaMedicaCriteria criteria, Pageable page) {
		return visiteMedicheRepository.findAll(createSpecification(criteria), page)
				.map(VisitaMedicaMapper::toDTO);
	}

	@Transactional
	public VisitaMedicaDTO update(UpdateVisitaMedicaDTO visitaMedicaDTO, Long id) {
		VisitaMedica visitaToSave = VisitaMedicaMapper.toEntity(visitaMedicaDTO);

		visitaToSave.setDipendente(dipendenteRepository.findById(visitaMedicaDTO.getIdDipendente())
				.orElseThrow(() -> new BadRequestException("Il dipendente con id %s non esiste", visitaMedicaDTO.getIdDipendente())));

		MedicalDay medicalDay = medicalDayRepository.findById(visitaMedicaDTO.getIdMedicalDay())
				.orElseThrow(() -> new BadRequestException("Il medical day con id %s non esiste", visitaMedicaDTO.getIdMedicalDay()));


		visitaToSave.setMedicalDay(medicalDay);
		visitaToSave.setId(id);

		if(visitaToSave.getStatoVisitaMedica() != StatoVisitaMedica.INSERITA) {
			eventoService.createEvento(visitaToSave.getMedicalDay(), visitaToSave.getDipendente(), TipologiaEvento.VISITA_MEDICA_CONCLUSA);
		}

		VisitaMedica savedVisita = visiteMedicheRepository.save(visitaToSave);

		medicalDay.aggiornamentoStato();

		Dipendente dipendente = dipendenteRepository.findById(visitaMedicaDTO.getIdDipendente()).get();

		if(visitaMedicaDTO.getStatoVisitaMedica() != null && visitaMedicaDTO.getStatoVisitaMedica() != StatoVisitaMedica.INSERITA) {

			dipendente.scadenzaVisita(LocalDate.now().plusYears(dipendente.getIntervalloScadenza()));
		}

		eventoService.createEvento(medicalDay, dipendente, TipologiaEvento.SCADENZA_VISITA_MODIFICATA);

		medicalDayRepository.save(medicalDay);

		return VisitaMedicaMapper.toDTO(savedVisita);
	}

	@Transactional
	public void deleteById(Long id) {

		VisitaMedica visitaMedicaDaCancellare = visiteMedicheRepository.findById(id)
				.orElseThrow(() -> new BadRequestException("Visita con id %s non presente sul database"));

		MedicalDay medicalDay = visitaMedicaDaCancellare.getMedicalDay();

		if (visitaMedicaDaCancellare.getStatoVisitaMedica() != StatoVisitaMedica.INSERITA) {
			throw new BadRequestException("Impossibile eliminare la visita medica con id %s se è in stato effettuata o non effettuata");
		}

		medicalDay.getVisiteMediche().remove(visitaMedicaDaCancellare);

		medicalDay.aggiornamentoStato();

		visiteMedicheRepository.deleteById(id);

		medicalDayRepository.save(medicalDay);

		eventoService.createEvento(medicalDay, visitaMedicaDaCancellare.getDipendente(), TipologiaEvento.VISITA_MEDICA_ANNULLATA);
	}

	private Specification<VisitaMedica> createSpecification(VisitaMedicaCriteria criteria) {
		Specification<VisitaMedica> specification = Specification.where(null);
		if (criteria != null) {

			if (criteria.getDataMedicalDayInizio() != null) {
				specification = specification.and((root, query, cb) -> {
					return cb.and(cb.greaterThanOrEqualTo(root.get(VisitaMedica_.MEDICAL_DAY).get(MedicalDay_.DATA),
							criteria.getDataMedicalDayInizio()));
				});
			}

			if (criteria.getDataMedicalDayFine() != null) {
				specification = specification.and((root, query, cb) -> {
					return cb.and(cb.lessThanOrEqualTo(root.get(VisitaMedica_.MEDICAL_DAY).get(MedicalDay_.DATA),
							criteria.getDataMedicalDayFine()));
				});
			}

			if (criteria.getStatoVisitaMedica() != null) {
				return specification = specification.and((visitaMedica, query, criteriaBuilder) -> {
					return criteriaBuilder.equal(visitaMedica.get(VisitaMedica_.STATO_VISITA_MEDICA),
							criteria.getStatoVisitaMedica());
				});
			}

			if (criteria.getIdCertificato() != null) {
				return specification = specification.and((visitaMedica, query, criteriaBuilder) -> {
					return criteriaBuilder.equal(visitaMedica.get(VisitaMedica_.ID_CERTIFICATO),
							criteria.getIdCertificato());
				});
			}
		}
		return specification;
	}

	@Transactional
	public VisitaMedicaCertificatoDTO insertDocument(UpdateVisitaMedicaCertificatoDTO certificatoUpdateDTO, long visitaId, MultipartFile file) {
		//controllo se c'è la visita
		
		VisitaMedica visita=visiteMedicheRepository
				.findById(visitaId)
				.orElseThrow(() ->  new BadRequestException("id visita non valido"));
	    
		if(visita.hasCertificato()) {
			throw new BadRequestException("La visita ha già un certificato");
		}
		
		VisitaMedicaMapper.toEntityCertificato(certificatoUpdateDTO, visita);
		
		//salvo il documento con il microservizio
		ResponseEntity<DocumentDTO> response = documentClient.insertDocument(file);
		
		//salvo il certificato anche su db salus
		visita.setIdCertificato(response.getBody().getId());
		visiteMedicheRepository.save(visita);
		
		//creo l'evento
		eventoService.createEvento(visita.getMedicalDay(), visita.getDipendente(), TipologiaEvento.ALLEGATO_CERTIFICATO_MEDICO);
		return VisitaMedicaMapper.toDTOCertificato(visita, response.getBody());
	}

	@Transactional
	public void deleteCertificato(Long id) {
		VisitaMedica visita=visiteMedicheRepository
				.findById(id)
				.orElseThrow(() ->  new BadRequestException("id visita non valido"));
	    
		if(!visita.hasCertificato()) {
			throw new BadRequestException("La visita non ha il certificato");
		}
		
		//documento remoto cancellato
		String idCertificatoToDelete = visita.getIdCertificato();
		documentClient.delete(idCertificatoToDelete);

		visita.setIdCertificato(null);
		visiteMedicheRepository.save(visita);
		
		eventoService.createEvento(visita.getMedicalDay(), visita.getDipendente(), TipologiaEvento.ELIMINATO_CERTIFICATO_MEDICO);

	}

	public ResponseEntity<Resource> downloadDocument(Long id) {
		VisitaMedica visita=visiteMedicheRepository
				.findById(id)
				.orElseThrow(() ->  new BadRequestException("id visita non valido"));
	    
		if(!visita.hasCertificato()) {
			throw new BadRequestException("La visita non ha il certificato");
		}

		return documentClient.downloadDocument(visita.getIdCertificato());
	}
}