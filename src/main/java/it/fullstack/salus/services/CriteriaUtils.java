package it.fullstack.salus.services;

import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;

	public interface CriteriaUtils {
		
		public static <T> Specification<T> like(String campo, String criteria) {
			return (root, query, cb) -> {
				Expression<String> toLower = cb.lower(root.get(campo));
				
				return cb.like(toLower, "%" + criteria.toLowerCase() + "%");
		};
	}
		public static <T,K> Predicate like(Join<T,K> join, CriteriaBuilder cb, String campo, String criteria) {
		    Expression<String> toLower = cb.lower(join.get(campo));
		    return cb.like(toLower, "%" + criteria.toLowerCase() + "%");
		}
}