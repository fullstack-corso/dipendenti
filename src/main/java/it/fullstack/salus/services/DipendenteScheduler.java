package it.fullstack.salus.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.config.Constants;
import it.fullstack.salus.domain.Dipendente;
import it.fullstack.salus.domain.enumeration.TipologiaEvento;
import it.fullstack.salus.repository.DipendenteRepository;
import it.fullstack.salus.services.dto.DipendenteCriteria;

@Component
public class DipendenteScheduler {

	@Autowired
	private DipendenteService dipendenteService;
	
	@Autowired
	DipendenteRepository repository;
	
	@Autowired
	EventoService eventoService;
	
	@Transactional
	@Scheduled(cron = "${application.scheduler.scadenza-visita}")
	public void controlloScadenzeVisita() {
		//TODO rivedere questo scheduler
		DipendenteCriteria criteria = new DipendenteCriteria();
		criteria.setSenzaScadenza(true);
		List<Dipendente> dipendenti = dipendenteService.findAll(criteria);
		dipendenti
			.stream()
			.map(dipendente -> dipendente.scadenzaVisita(LocalDate.now().plusYears(dipendente.getIntervalloScadenza()))) 
			.forEach(dip -> {
				repository.save(dip);
				eventoService.createEvento(null, dip, TipologiaEvento.SCADENZA_VISITA_MODIFICATA);
			});
	}
	
	//TODO togliere il transactional allo scheduler e metterlo ad un metodo separato 
		//per chiamare il save del dipendente e creazione evento associata
	@Transactional
	@Scheduled(cron = "${application.scheduler.50-anni-test}")
	public void cambiaScadenza() {
		
		DipendenteCriteria criteria = new DipendenteCriteria();
		criteria.setOver(Constants.ETA_LIMITE);
		List<Dipendente> dipendenti = dipendenteService.findByCriteria(criteria);
		
		//TODO fare controllo sulla scadenza del dipendente se necessita di essere cambiata
		dipendenti.forEach(dip -> {
			dip.setScadenzaVisita(dip.getUltimaVisita().plusYears(dip.getIntervalloScadenza()));
			repository.save(dip);
			eventoService.createEvento(null, dip, TipologiaEvento.SCADENZA_VISITA_MODIFICATA);
		});			
		//salva su db il set visita medica e che sia 2 anni dall'ultima
	}
}