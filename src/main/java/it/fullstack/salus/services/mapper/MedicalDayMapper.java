package it.fullstack.salus.services.mapper;

import it.fullstack.salus.domain.MedicalDay;
import it.fullstack.salus.services.dto.CreateMedicalDayDTO;
import it.fullstack.salus.services.dto.MedicalDayDTO;
import it.fullstack.salus.services.dto.UpdateMedicalDayDTO;

public interface MedicalDayMapper {

	public static MedicalDay toEntity(UpdateMedicalDayDTO updateMedicalDayDTO) {
		MedicalDay medicalDay = new MedicalDay();
		medicalDay.setData(updateMedicalDayDTO.getData());

		return medicalDay;
	}

	public static MedicalDay toEntity(CreateMedicalDayDTO createMedicalDayDTO) {
		MedicalDay medicalDay = new MedicalDay();
		medicalDay.setData(createMedicalDayDTO.getData());

		return medicalDay;
	}


	static MedicalDay mergeEntity(UpdateMedicalDayDTO dto, MedicalDay fromDB) {
		fromDB.setData(dto.getData());
		return fromDB;
	}


	public static MedicalDayDTO toDTO(MedicalDay medicalDay) {
		MedicalDayDTO medicalDayDTO = new MedicalDayDTO();
		medicalDayDTO.setId(medicalDay.getId());
		medicalDayDTO.setData(medicalDay.getData());
		medicalDayDTO.setContratto(medicalDay.getContratto());
		medicalDayDTO.setStatoMedicalDay(medicalDay.getStatoMedicalDay());
		return medicalDayDTO;
	}
	
	public static UpdateMedicalDayDTO toUpdateDTO(MedicalDay medicalDay) {
		UpdateMedicalDayDTO updateMedicalDayDTO = new UpdateMedicalDayDTO();
		updateMedicalDayDTO.setData(medicalDay.getData());
		updateMedicalDayDTO.setContrattoId(medicalDay.getContratto().getId());
		return updateMedicalDayDTO;
	}
}
