package it.fullstack.salus.services.mapper;

import it.fullstack.salus.domain.Dipendente;
import it.fullstack.salus.services.dto.DipendenteDTO;
import it.fullstack.salus.services.dto.UpdateDipendenteDTO;

public interface DipendenteMapper {

	static Dipendente toEntity(UpdateDipendenteDTO dto) {
		Dipendente entity = new Dipendente();
		
		entity.setNome(dto.getNome());
		entity.setCognome(dto.getCognome());
		entity.setDataNascita(dto.getDataNascita());
		entity.setEmail(dto.getEmail());

		entity.setCodiceFiscale(dto.getCodiceFiscale());
		entity.setSesso(dto.getSesso());
		entity.setDataPrimoContratto(dto.getDataPrimoContratto());
		entity.setDataAssunzioneDipendente(dto.getDataAssunzioneDipendente());
		entity.setUltimaVisita(dto.getUltimaVisita());
		entity.setStatoAssunzione(dto.getStatoAssunzione());
		entity.setDistacco(dto.getDistacco());
		entity.setSede(dto.getSede());
		entity.setMansione(dto.getMansione());
		entity.setContratto(dto.getContratto());
		
		return entity;
	}
	
	static Dipendente mergeEntity(UpdateDipendenteDTO dto, Dipendente fromDB) {
		
		fromDB.setNome(dto.getNome());
		fromDB.setCognome(dto.getCognome());
		fromDB.setDataNascita(dto.getDataNascita());
		fromDB.setEmail(dto.getEmail());
		fromDB.setCodiceFiscale(dto.getCodiceFiscale());
		fromDB.setSesso(dto.getSesso());
		fromDB.setDataPrimoContratto(dto.getDataPrimoContratto());
		fromDB.setDataAssunzioneDipendente(dto.getDataAssunzioneDipendente());
		fromDB.setUltimaVisita(dto.getUltimaVisita());
		fromDB.setStatoAssunzione(dto.getStatoAssunzione());
		fromDB.setDistacco(dto.getDistacco());
		fromDB.setSede(dto.getSede());
		fromDB.setMansione(dto.getMansione());
		fromDB.setContratto(dto.getContratto());
		
		return fromDB;
	}
	
	static DipendenteDTO toDTO(Dipendente entity) {
		DipendenteDTO dto = new DipendenteDTO();
		dto.setId(entity.getId());
		dto.setNome(entity.getNome());
		dto.setCognome(entity.getCognome());
		dto.setDataNascita(entity.getDataNascita());
		dto.setEmail(entity.getEmail());
		dto.setCodiceFiscale(entity.getCodiceFiscale());
		dto.setSesso(entity.getSesso());
		dto.setUltimaVisita(entity.getUltimaVisita());
		dto.setDataPrimoContratto(entity.getDataPrimoContratto());
		dto.setDataAssunzioneDipendente(entity.getDataAssunzioneDipendente());
		dto.setScadenzaVisita(entity.getScadenzaVisita());
		dto.setSede(entity.getSede());
		
		return dto;
	}
	
	static Dipendente toEntity(DipendenteDTO dipendenteDTO) {
		Dipendente entity = new Dipendente();
		entity.setId(dipendenteDTO.getId());
		entity.setNome(dipendenteDTO.getNome());
		entity.setCognome(dipendenteDTO.getCognome());
		entity.setDataNascita(dipendenteDTO.getDataNascita());
		entity.setEmail(dipendenteDTO.getEmail());
		entity.setCodiceFiscale(dipendenteDTO.getCodiceFiscale());
		entity.setSesso(dipendenteDTO.getSesso());
		entity.setDataPrimoContratto(dipendenteDTO.getDataPrimoContratto());
		entity.setDataAssunzioneDipendente(dipendenteDTO.getDataAssunzioneDipendente());
		entity.setScadenzaVisita(dipendenteDTO.getScadenzaVisita());
		entity.setSede(dipendenteDTO.getSede());
		
		return entity;
	}
	
	static UpdateDipendenteDTO toUpdateDTO(Dipendente entity) {
		UpdateDipendenteDTO updateDto = new UpdateDipendenteDTO();
		
		updateDto.setNome(entity.getNome());
		updateDto.setCognome(entity.getCognome());
		updateDto.setDataNascita(entity.getDataNascita());
		updateDto.setEmail(entity.getEmail());
		updateDto.setCodiceFiscale(entity.getCodiceFiscale());
		updateDto.setSesso(entity.getSesso());
		updateDto.setDataPrimoContratto(entity.getDataPrimoContratto());
		updateDto.setDataAssunzioneDipendente(entity.getDataAssunzioneDipendente());
		updateDto.setUltimaVisita(entity.getUltimaVisita());
		updateDto.setStatoAssunzione(entity.getStatoAssunzione());
		updateDto.setDistacco(entity.getDistacco());
		updateDto.setSede(entity.getSede());
		updateDto.setMansione(entity.getMansione());
		updateDto.setContratto(entity.getContratto());

		return updateDto;
	}
}