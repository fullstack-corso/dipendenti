package it.fullstack.salus.services.mapper;

import it.fullstack.salus.domain.VisitaMedica;
import it.fullstack.salus.services.dto.DocumentDTO;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaCertificatoDTO;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCaledarioDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCertificatoDTO;
import it.fullstack.salus.services.dto.VisitaMedicaDTO;

public interface VisitaMedicaMapper {

	public static VisitaMedica toEntity(UpdateVisitaMedicaDTO visitaMedica) {
		VisitaMedica visita = new VisitaMedica();
		visita.setOraVisita(visitaMedica.getOraVisita());
		visita.setStatoVisitaMedica(visitaMedica.getStatoVisitaMedica());
		return visita;
	}
	
	public static VisitaMedicaDTO toDTO(VisitaMedica visitaMedica) {
		VisitaMedicaDTO visitaDTO = new VisitaMedicaDTO();
		visitaDTO.setId(visitaMedica.getId());
		visitaDTO.setOraVisita(visitaMedica.getOraVisita());
		visitaDTO.setDipendente(visitaMedica.getDipendente());
		visitaDTO.setMedicalDay(visitaMedica.getMedicalDay());
		visitaDTO.setStatoVisitaMedica(visitaMedica.getStatoVisitaMedica()); 
		return visitaDTO;
	}
	
	public static VisitaMedicaCaledarioDTO toDTOCalendario(VisitaMedica visitaMedica) {
		VisitaMedicaCaledarioDTO visitaDTO = new VisitaMedicaCaledarioDTO();
		visitaDTO.setDataOraVisita(visitaMedica.getData());
		visitaDTO.setNome(visitaMedica.getDipendente().getNome());
		visitaDTO.setCognome(visitaMedica.getDipendente().getCognome());
		return visitaDTO;
	}
	
	//Gestione documento visita
	
	public static UpdateVisitaMedicaCertificatoDTO toUpdateDTOCertificato(VisitaMedica visitaMedica) {
		UpdateVisitaMedicaCertificatoDTO certificatoIdoneitaDTO = new UpdateVisitaMedicaCertificatoDTO();
		certificatoIdoneitaDTO.setPrescrizione(visitaMedica.getPrescrizione());
		certificatoIdoneitaDTO.setIdoneo(visitaMedica.isIdoneo());
		certificatoIdoneitaDTO.setRinnovoScadenzaIdoneita(visitaMedica.getRinnovoScadenzaIdoneita());
		return certificatoIdoneitaDTO;
	}

	public static VisitaMedica toEntityCertificato(UpdateVisitaMedicaCertificatoDTO certificatoIdoneitaDTO, VisitaMedica visitaMedica) {
		visitaMedica.setPrescrizione(certificatoIdoneitaDTO.getPrescrizione());
		visitaMedica.setIdoneo(certificatoIdoneitaDTO.isIdoneo());
		visitaMedica.setRinnovoScadenzaIdoneita(certificatoIdoneitaDTO.getRinnovoScadenzaIdoneita());
		return visitaMedica;
	}
	
	public static VisitaMedicaCertificatoDTO toDTOCertificato(VisitaMedica visitaMedica, DocumentDTO documentDTO) {
		VisitaMedicaCertificatoDTO certificatoIdoneitaDTO = new VisitaMedicaCertificatoDTO();
		certificatoIdoneitaDTO.setDocumentDTO(documentDTO);
		certificatoIdoneitaDTO.setIdCertificato(visitaMedica.getIdCertificato());
		certificatoIdoneitaDTO.setIdoneo(visitaMedica.isIdoneo());
		certificatoIdoneitaDTO.setPrescrizione(visitaMedica.getPrescrizione());
		certificatoIdoneitaDTO.setRinnovoScadenzaIdoneita(visitaMedica.getRinnovoScadenzaIdoneita());
		return certificatoIdoneitaDTO;
	}
	
}
