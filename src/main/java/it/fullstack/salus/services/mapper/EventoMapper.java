package it.fullstack.salus.services.mapper;

import it.fullstack.salus.domain.Evento;
import it.fullstack.salus.services.dto.EventoDTO;

public interface EventoMapper {

	public static Evento toEntity(EventoDTO eventoDTO) {
		Evento evento = new Evento();
		evento.setDipendente(eventoDTO.getDipendente());
		evento.setId(eventoDTO.getId());
		evento.setMedicalDay(eventoDTO.getMedicalDay());
		evento.setTimestamp(eventoDTO.getTimestamp());
		evento.setTipologiaEvento(eventoDTO.getTipologiaEvento());
		return evento;
	}
	
	public static EventoDTO toDTO(Evento evento) {
		EventoDTO eventoDTO = new EventoDTO();
		eventoDTO.setDipendente(evento.getDipendente());
		eventoDTO.setId(evento.getId());
		eventoDTO.setMedicalDay(evento.getMedicalDay());
		eventoDTO.setTimestamp(evento.getTimestamp());
		eventoDTO.setTipologiaEvento(evento.getTipologiaEvento());
		return eventoDTO;
		
		
		
		
	}
}
