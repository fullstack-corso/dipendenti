package it.fullstack.salus.services.mapper;

import it.fullstack.salus.domain.Sede;
import it.fullstack.salus.services.dto.SedeDTO;
import it.fullstack.salus.services.dto.UpdateSedeDTO;

public interface SedeMapper {

	public static SedeDTO toDTO(Sede sede) {
		SedeDTO sedeConvertitaDTO = new SedeDTO();
		sedeConvertitaDTO.setId(sede.getId());
		sedeConvertitaDTO.setDenominazione(sede.getDenominazione());
		sedeConvertitaDTO.setVia(sede.getVia());
		sedeConvertitaDTO.setCivico(sede.getCivico());
		sedeConvertitaDTO.setCap(sede.getCap());
		sedeConvertitaDTO.setProvincia(sede.getProvincia());
		sedeConvertitaDTO.setCitta(sede.getCitta());
		return sedeConvertitaDTO;
	}
	
	public static Sede toEntity(UpdateSedeDTO sedeDTO) {
		Sede sedeConvertitaEntity = new Sede();
		sedeConvertitaEntity.setDenominazione(sedeDTO.getDenominazione());
		sedeConvertitaEntity.setVia(sedeDTO.getVia());
		sedeConvertitaEntity.setCivico(sedeDTO.getCivico());
		sedeConvertitaEntity.setCap(sedeDTO.getCap());
		sedeConvertitaEntity.setProvincia(sedeDTO.getProvincia());
		sedeConvertitaEntity.setCitta(sedeDTO.getCitta());
		return sedeConvertitaEntity;
	}
}
