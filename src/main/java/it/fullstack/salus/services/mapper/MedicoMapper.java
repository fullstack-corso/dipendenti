package it.fullstack.salus.services.mapper;

import it.fullstack.salus.domain.Medico;
import it.fullstack.salus.services.dto.MedicoDTO;
import it.fullstack.salus.services.dto.UpdateMedicoDTO;

public interface MedicoMapper {
	
	public static MedicoDTO toDTO (Medico medico) {
		MedicoDTO medicoDTO = new MedicoDTO();
		medicoDTO.setId(medico.getId());
		medicoDTO.setNome(medico.getNome());
		medicoDTO.setCognome(medico.getCognome());
		medicoDTO.setCodiceFiscale(medico.getCodiceFiscale());
		medicoDTO.setDataNascita(medico.getDataNascita());
		medicoDTO.setEmail(medico.getEmail());
		medicoDTO.setNumeroTelefono(medico.getNumeroTelefono());
		medicoDTO.setSesso(medico.getSesso());
		medicoDTO.setSocieta(medico.getSocieta());

		return medicoDTO;
	}
	
	public static Medico toEntity (UpdateMedicoDTO updateMedicoDTO) {
		Medico medico = new Medico();
		medico.setNome(updateMedicoDTO.getNome());
		medico.setCognome(updateMedicoDTO.getCognome());
		medico.setCodiceFiscale(updateMedicoDTO.getCodiceFiscale());
		medico.setDataNascita(updateMedicoDTO.getDataNascita());
		medico.setEmail(updateMedicoDTO.getEmail());
		medico.setNumeroTelefono(updateMedicoDTO.getNumeroTelefono());
		medico.setSesso(updateMedicoDTO.getSesso());
		medico.setSocieta(updateMedicoDTO.getSocieta());
		
		return medico;
	}
	
	public static UpdateMedicoDTO toUpdateDTO(Medico medico) {
		UpdateMedicoDTO medicoDTO = new UpdateMedicoDTO();
		medicoDTO.setNome(medico.getNome());
		medicoDTO.setCognome(medico.getCognome());
		medicoDTO.setCodiceFiscale(medico.getCodiceFiscale());
		medicoDTO.setDataNascita(medico.getDataNascita());
		medicoDTO.setEmail(medico.getEmail());
		medicoDTO.setNumeroTelefono(medico.getNumeroTelefono());
		medicoDTO.setSesso(medico.getSesso());
		medicoDTO.setSocieta(medico.getSocieta());
		return medicoDTO;
	}
	
}
