package it.fullstack.salus.services.mapper;

import it.fullstack.salus.domain.Contratto;
import it.fullstack.salus.services.dto.UpdateContrattoDTO;
import it.fullstack.salus.services.dto.ContrattoDTO;

public interface ContrattoMapper {

  public static ContrattoDTO toDTO(Contratto contratto) {
	    ContrattoDTO contrattoConvertitoDTO = new ContrattoDTO();
	    contrattoConvertitoDTO.setId(contratto.getId());
	    contrattoConvertitoDTO.setDataInizio(contratto.getDataInizio());
	    contrattoConvertitoDTO.setDataFine(contratto.getDataFine());
	    contrattoConvertitoDTO.setMedico(contratto.getMedico());
	    contrattoConvertitoDTO.setSede(contratto.getSede());
	    return contrattoConvertitoDTO;
	  }

  public static Contratto toEntity(UpdateContrattoDTO contrattoDTOSave) {
	    Contratto contratto = new Contratto();
	    contratto.setDataInizio(contrattoDTOSave.getDataInizio());
	    contratto.setDataFine(contrattoDTOSave.getDataFine());
	    return contratto;
	  }
  
  public static UpdateContrattoDTO toUpdateDTO (Contratto contratto) {
	  UpdateContrattoDTO contrattoUpdateDTO = new UpdateContrattoDTO();
	  contrattoUpdateDTO.setDataInizio(contratto.getDataInizio());
	  contrattoUpdateDTO.setDataFine(contratto.getDataFine());
	  contrattoUpdateDTO.setMedicoId(contratto.getMedico().getId());
	  contrattoUpdateDTO.setSedeId(contratto.getSede().getId());
	  return contrattoUpdateDTO;
  }
  
}
