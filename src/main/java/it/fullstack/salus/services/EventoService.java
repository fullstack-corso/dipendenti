package it.fullstack.salus.services;

import java.time.LocalDateTime;

import static it.fullstack.salus.services.CriteriaUtils.like;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.domain.Dipendente;
import it.fullstack.salus.domain.Dipendente_;
import it.fullstack.salus.domain.Evento;
import it.fullstack.salus.domain.Evento_;
import it.fullstack.salus.domain.MedicalDay;
import it.fullstack.salus.domain.enumeration.TipologiaEvento;
import it.fullstack.salus.repository.EventoRepository;
import it.fullstack.salus.services.dto.EventoCriteria;
import it.fullstack.salus.services.dto.EventoDTO;
import it.fullstack.salus.services.mapper.EventoMapper;
import jakarta.persistence.criteria.Join;

@Service
public class EventoService {

	@Autowired
	EventoRepository repository;

	@Transactional(readOnly = true)
	public Page<EventoDTO> findAll(EventoCriteria criteria, Pageable pageable) {		
		return repository
				.findAll(createSpecification(criteria),pageable)
				.map(EventoMapper::toDTO);
	}
	
	public void createEvento(MedicalDay medicalDay, Dipendente dipendente, TipologiaEvento tipologiaEvento) {
		Evento evento = new Evento();
		evento.setMedicalDay(medicalDay);
		evento.setTimestamp(LocalDateTime.now());
		evento.setDipendente(dipendente);
		evento.setTipologiaEvento(tipologiaEvento);
		repository.save(evento);
	}
	
	public Specification<Evento> createSpecification(EventoCriteria criteria){
		Specification<Evento> specification = Specification.where(null);

		if(criteria!=null) {

			if(criteria.getIdDipendente()!=null) {
				specification = specification.and(hasDipendenteId(criteria.getIdDipendente()));

			}
			
			if(criteria.getNome()!=null) {
				specification = specification.and(hasDipendenteNome(criteria.getNome()));
			}
			
			if(criteria.getCognome()!=null) {
				specification = specification.and(hasDipendenteCognome(criteria.getCognome()));
			}
			
			if(criteria.getCodiceFiscale()!=null) {
				specification = specification.and(hasDipendenteCodiceFiscale(criteria.getCodiceFiscale()));
			}
			
			if(criteria.getNomeEvento()!=null) {
				specification = specification.and((evento,root,criteriaBuilder)->{
					return criteriaBuilder.equal(evento.get(Evento_.TIPOLOGIA_EVENTO), criteria.getNomeEvento());
				});
			}

			if(criteria.getTimestamp()!=null) {
				specification = specification.and((evento,query,criteriaBuilder)->{
					return criteriaBuilder.equal(evento.get(Evento_.TIMESTAMP), criteria.getTimestamp());
				});
			}

			if(criteria.getDataDal()!= null) {
				specification = specification.and((evento,query,criteriaBuilder)->{
					return criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(evento.get(Evento_.TIMESTAMP), criteria.getDataDal()));
				});
			}
			
			if(criteria.getDataAl()!= null) {
				specification = specification.and((evento,query,criteriaBuilder)->{
					return criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(evento.get(Evento_.TIMESTAMP), criteria.getDataAl()));
				});
			}
			
		}
		return specification;
	}

	public static Specification<Evento> hasDipendenteId(Long dipendenteId){
		return (evento,query, criteriaBuilder) -> {
			Join<Evento, Dipendente> dipendente = evento.join(Evento_.DIPENDENTE);
			return criteriaBuilder.equal(dipendente.get(Dipendente_.ID), dipendenteId);
		};
	}
	
	public static Specification<Evento> hasDipendenteNome(String nome){
		return (evento,query, criteriaBuilder) -> {
			Join<Evento, Dipendente> dipendente = evento.join(Evento_.DIPENDENTE);
			return criteriaBuilder.and(like(dipendente,criteriaBuilder,Dipendente_.NOME, nome));
		};
	}
	
	public static Specification<Evento> hasDipendenteCognome(String cognome){
		return (evento,query, criteriaBuilder) -> {
			Join<Evento, Dipendente> dipendente = evento.join(Evento_.DIPENDENTE);
			return criteriaBuilder.and(like(dipendente,criteriaBuilder,Dipendente_.COGNOME, cognome));
		};
	}
	
	public static Specification<Evento> hasDipendenteCodiceFiscale(String codiceFiscale){
		return (evento,query, criteriaBuilder) -> {
			Join<Evento, Dipendente> dipendente = evento.join(Evento_.DIPENDENTE);
			return criteriaBuilder.equal(dipendente.get(Dipendente_.CODICE_FISCALE), codiceFiscale);
		};
	}
}