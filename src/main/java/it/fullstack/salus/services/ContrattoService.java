package it.fullstack.salus.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.domain.Contratto;
import it.fullstack.salus.domain.Contratto_;
import it.fullstack.salus.domain.Sede;
import it.fullstack.salus.domain.Sede_;
import it.fullstack.salus.repository.ContrattoRepository;
import it.fullstack.salus.repository.MedicoRepository;
import it.fullstack.salus.repository.SedeRepository;
import it.fullstack.salus.services.dto.ContrattoCriteria;
import it.fullstack.salus.services.dto.ContrattoDTO;
import it.fullstack.salus.services.dto.UpdateContrattoDTO;
import it.fullstack.salus.services.mapper.ContrattoMapper;
import it.fullstack.salus.web.rest.errors.BadRequestException;
import it.fullstack.sdk.core.service.QueryService;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;

@Service
public class ContrattoService extends QueryService<Contratto> {

	@Autowired
	MedicoRepository medicoRepository;
	@Autowired
	SedeRepository sedeRepository;
	@Autowired
	ContrattoRepository contrattoRepository;

	@Transactional(readOnly = true)
	public Page<ContrattoDTO> findAll(ContrattoCriteria criteria, Pageable pageable) {		
		return contrattoRepository
				.findAll(createSpecification(criteria),pageable)
				.map(ContrattoMapper::toDTO);
	}

	@Transactional(readOnly = true)
	public ContrattoDTO findById(Long id) {
		return contrattoRepository
				.findById(id)
				.map(ContrattoMapper::toDTO)
				.orElseThrow(() -> new BadRequestException("Contratto non presente sul database"));
	}
	
	@Transactional
	public ContrattoDTO save(UpdateContrattoDTO contrattoDTO)  {
		Contratto contratto= ContrattoMapper.toEntity(contrattoDTO);

		contratto.setMedico(medicoRepository.findById(contrattoDTO.getMedicoId())
				.orElseThrow(() -> new BadRequestException("Medico con id %s non presente", contrattoDTO.getMedicoId())));
		contratto.setSede(sedeRepository.findById(contrattoDTO.getSedeId())
				.orElseThrow(() -> new BadRequestException("Sede con id %s non presente", contrattoDTO.getSedeId())));
		
		contrattoRepository.save(contratto);
		return ContrattoMapper.toDTO(contratto);
	}
	
	@Transactional
	public ContrattoDTO update(Long id, UpdateContrattoDTO contratto) {
		Contratto contrattoDB = contrattoRepository
				.findById(id)
				.map((fromDB) -> ContrattoMapper.toEntity(contratto))
				.orElseThrow(() -> new BadRequestException("Contratto non trovato"));
		contrattoDB.setId(id);
		var saved = contrattoRepository.save(contrattoDB);
		return ContrattoMapper.toDTO(saved);
	}
	
	@Transactional
	public void deleteById(Long id) {
		contrattoRepository.deleteById(id);
	}

	private Specification<Contratto> createSpecification(ContrattoCriteria criteria) {
		Specification<Contratto> specification = Specification.where(null);
		if (criteria != null) {
			
			if (criteria.getSedeId() != null) {
					specification = specification.and(
					buildSpecification(
	                criteria.getSedeId(),
	                root -> root.join(Contratto_.sede, JoinType.LEFT).get(Sede_.id)
	            )
	        );
      }			
		}
		return specification;
	}

	public static Specification<Contratto> hasSedeId(Long sedeId){
		return (root, query, criteriaBuilder) -> {
			Join<Contratto, Sede> sede = root.join(Contratto_.SEDE);
			return criteriaBuilder.equal(sede.get(Sede_.ID), sedeId);
		};
	}
}
