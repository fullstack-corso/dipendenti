package it.fullstack.salus.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.domain.Contratto_;
import it.fullstack.salus.domain.Medico;
import it.fullstack.salus.domain.Medico_;
import it.fullstack.salus.domain.Sede_;
import it.fullstack.salus.repository.MedicoRepository;
import it.fullstack.salus.services.dto.MedicoCriteria;
import it.fullstack.salus.services.dto.MedicoDTO;
import it.fullstack.salus.services.dto.UpdateMedicoDTO;
import it.fullstack.salus.services.mapper.MedicoMapper;
import it.fullstack.salus.web.rest.errors.BadRequestException;
import it.fullstack.sdk.core.service.QueryService;
import jakarta.persistence.criteria.JoinType;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MedicoService extends QueryService<Medico>{

	@Autowired
	private MedicoRepository medicoRepository;

	@Transactional(readOnly = true)
	public Page<MedicoDTO> findAll(MedicoCriteria criteria, Pageable page) {
		return medicoRepository.findAll(createSpecification(criteria), page)
				.map(MedicoMapper::toDTO);
	}

	@Transactional(readOnly = true)
	public MedicoDTO findById(Long id) {
		return medicoRepository.findById(id)
				.map(MedicoMapper::toDTO)
				.orElseThrow(() -> new BadRequestException("Medico non trovato"));
	}

	@Transactional
	public MedicoDTO save(UpdateMedicoDTO updateMedicoDTO) {
		Medico medicoToSave = MedicoMapper.toEntity(updateMedicoDTO);
		Medico savedMedico = medicoRepository.save(medicoToSave);
		return MedicoMapper.toDTO(savedMedico);
	}

	@Transactional
	public MedicoDTO update(UpdateMedicoDTO updateMedicoDTO, Long id) {
		medicoRepository.findById(id)
				.orElseThrow(() -> new BadRequestException("Il medical day con id %s non esiste", id));
		Medico medicoToSave = MedicoMapper.toEntity(updateMedicoDTO);
				medicoToSave.setId(id);
		Medico savedMedico = medicoRepository.save(medicoToSave);
		return MedicoMapper.toDTO(savedMedico);
	}

	@Transactional
	public void deleteById(Long id) {
		medicoRepository
				  .findById(id)
				  .orElseThrow(()-> new BadRequestException("Medico con id %s non presente sul database"));
		medicoRepository.deleteById(id);
	}

	private Specification<Medico> createSpecification(MedicoCriteria criteria) {
		Specification<Medico> specification = Specification.where(null);

		if (criteria != null) {
			
			if (criteria.getNome() != null) {
				specification = specification.and(
						buildStringSpecification(
								criteria.getNome(), Medico_.nome));
			}

			if (criteria.getCognome() != null) {
				specification = specification.and(
						buildStringSpecification(
								criteria.getCognome(), Medico_.cognome));
			}

			if (criteria.getSedeId()!= null) {
				specification = specification.and(
						buildSpecification(
								criteria.getSedeId(),
								root -> root
								.join(Medico_.contratti, JoinType.LEFT)
								.join(Contratto_.sede, JoinType.LEFT)
								.get(Sede_.id)));
			}
		}
		return specification;
	}
}