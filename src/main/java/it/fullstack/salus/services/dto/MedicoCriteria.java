package it.fullstack.salus.services.dto;

import it.fullstack.sdk.core.service.filter.LongFilter;
import it.fullstack.sdk.core.service.filter.StringFilter;
import lombok.Data;

@Data
public class MedicoCriteria {
	
	private StringFilter nome;
	private StringFilter cognome;
	private LongFilter sedeId;
	
}
