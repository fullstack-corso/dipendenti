package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.domain.Contratto;
import it.fullstack.salus.domain.enumeration.StatoMedicalDay;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class MedicalDayDTO {

	@NotNull
	private Long id;
	
	@NotNull
	@Future
	private LocalDate data;
	
	@NotNull
	private Contratto contratto;
	
	@NotNull
    private StatoMedicalDay statoMedicalDay;
}
