package it.fullstack.salus.services.dto;

import java.time.LocalTime;

import it.fullstack.salus.domain.Dipendente;
import it.fullstack.salus.domain.MedicalDay;
import it.fullstack.salus.domain.enumeration.StatoVisitaMedica;
import lombok.Data;

@Data
public class VisitaMedicaDTO {
	
	private Long id;
	
	private LocalTime oraVisita;
	
	private MedicalDay medicalDay;
	
	private Dipendente dipendente;
	
	private boolean certificato;
	
	private StatoVisitaMedica statoVisitaMedica;
	
}