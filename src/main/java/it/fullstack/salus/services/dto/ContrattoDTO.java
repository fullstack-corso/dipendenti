package it.fullstack.salus.services.dto;


import java.time.LocalDate;

import it.fullstack.salus.domain.Medico;
import it.fullstack.salus.domain.Sede;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//DTO identico all'entity, da tenere?

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContrattoDTO {

	private Long id;
	private LocalDate dataInizio;
	private LocalDate dataFine;
	private Medico medico;
	private Sede sede;
	
	public boolean isAttivo() {
		return this.dataFine.isAfter(LocalDate.now()) && 
			this.dataInizio.isBefore(LocalDate.now());
	}
}
