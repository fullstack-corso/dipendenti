package it.fullstack.salus.services.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateMedicalDayDTO {

	@NotNull
	@FutureOrPresent
	private LocalDate data;
	
	@NotNull
	private Long contrattoId;
	
	
	private LocalTime orarioInizioGiornata;
	
	private LocalTime orarioFineGiornata;
	
	//TODO su come gestire: minuti o ore?
	private Integer durataMediaVisite;
	
	
}
