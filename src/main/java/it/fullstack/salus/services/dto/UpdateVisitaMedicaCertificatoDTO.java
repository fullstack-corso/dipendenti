package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateVisitaMedicaCertificatoDTO {
	
	@NotNull
	private boolean idoneo;
	private LocalDate rinnovoScadenzaIdoneita;
	private String prescrizione;

}
