package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.StatoVisitaMedica;
import it.fullstack.sdk.core.service.filter.StringFilter;
import lombok.Data;

@Data
public class VisitaMedicaCriteria {
	
	private LocalDate dataMedicalDayInizio;
	private LocalDate dataMedicalDayFine;
	private StatoVisitaMedica statoVisitaMedica;
	private StringFilter idCertificato;

}
