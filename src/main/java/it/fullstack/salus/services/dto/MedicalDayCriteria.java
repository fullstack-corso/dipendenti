package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.StatoMedicalDay;
import it.fullstack.sdk.core.service.Criteria;
import it.fullstack.sdk.core.service.filter.Filter;
import it.fullstack.sdk.core.service.filter.LongFilter;
import it.fullstack.sdk.core.service.filter.RangeFilter;
import it.fullstack.sdk.core.service.filter.StatoMedicalDayFilter;
import it.fullstack.sdk.core.service.filter.StringFilter;
import lombok.Data;

@Data
public class MedicalDayCriteria{
	
	public static class StatoMedicalDayFilter extends Filter<StatoMedicalDay> {

        public StatoMedicalDayFilter() {}

        public StatoMedicalDayFilter(StatoMedicalDayFilter filter) {
            super(filter);
        }
    }
	
	private LongFilter idMedico;
	private LongFilter idSede;
	private RangeFilter<LocalDate> dataGiorno;
	private StatoMedicalDayFilter medicalDayStato;
//	private Filter<StatoMedicalDay> medicalDayStato;
	
	
	
	
}
