package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.Sesso;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateMedicoDTO {
	
	@NotNull
	private String nome;
	
	@NotNull
	private String cognome;
	@NotNull
	private LocalDate dataNascita;
	private String email;
	private String titoloStudi;
	
	@NotNull
	private String codiceFiscale;
	private String numeroTelefono;
	@NotNull
	private Sesso sesso;
	private Boolean societa;

}
