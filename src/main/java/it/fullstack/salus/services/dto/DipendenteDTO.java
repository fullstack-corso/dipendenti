package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.domain.Sede;
import it.fullstack.salus.domain.enumeration.Sesso;
import lombok.Data;

//FIXME discrepanze con updateDTO

@Data
public class DipendenteDTO {
	private Long id;
	private String nome;
	private String cognome;
	private LocalDate dataNascita;
	private String email;
	private LocalDate ultimaVisita;
	private String titoloStudi;
	private String codiceFiscale;
	private String numeroTelefono;
	private Sesso sesso;
	private LocalDate dataPrimoContratto;
	private LocalDate dataAssunzioneDipendente;
	private LocalDate scadenzaVisita;	
	private Sede sede;
}
