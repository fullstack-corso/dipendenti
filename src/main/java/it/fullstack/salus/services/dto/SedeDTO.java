package it.fullstack.salus.services.dto;

import lombok.Data;

@Data
public class SedeDTO {

	private Long id;
	private String denominazione;
	private String via;
	private String citta;
	private String civico;
	private String cap;
	private String provincia;
}
