package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.web.validator.CheckDate;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@CheckDate
public class UpdateContrattoDTO {
	
	private Long id;
	
	@NotNull
	private LocalDate dataInizio;
	
	@NotNull
	@Future
	private LocalDate dataFine;
	
	@NotNull
	private Long medicoId;
	
	@NotNull
	private Long sedeId;
	
	public boolean isAttivo() {
		return this.dataFine.isAfter(LocalDate.now()) && 
			this.dataInizio.isBefore(LocalDate.now());
	}
	
}

	

