package it.fullstack.salus.services.dto;

import java.time.LocalDateTime;

import it.fullstack.salus.domain.enumeration.TipologiaEvento;
import lombok.Data;

@Data
public class EventoCriteria {

	//TODO modifcare l'attributo timestamp così che la specification associata in equal abbia senso
	
	private Long idDipendente;
	private String nome;
	private String cognome;
	private String codiceFiscale;
	private TipologiaEvento nomeEvento;
	private LocalDateTime timestamp;
	private LocalDateTime dataDal;
	private LocalDateTime dataAl;
}
