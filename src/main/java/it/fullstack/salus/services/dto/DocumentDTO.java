package it.fullstack.salus.services.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class DocumentDTO {
	
    private LocalDateTime createdDate;
    private String lastModifiedDate;
    private String id;
    private Long size;
    private String contentType;
    private String originalFilename;

}
