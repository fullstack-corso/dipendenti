package it.fullstack.salus.services.dto;

import java.time.LocalTime;


import it.fullstack.salus.domain.enumeration.StatoVisitaMedica;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateVisitaMedicaDTO {
	
	@NotNull
	private LocalTime oraVisita;
	
	private LocalTime oraFineVisita;
	
	@NotNull
	private Long idMedicalDay;
	
	@NotNull
	private Long idDipendente;
	
	private StatoVisitaMedica statoVisitaMedica;
	
}