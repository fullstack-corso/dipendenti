package it.fullstack.salus.services.dto;

import java.time.LocalDateTime;

import it.fullstack.salus.domain.Dipendente;
import it.fullstack.salus.domain.MedicalDay;
import it.fullstack.salus.domain.enumeration.TipologiaEvento;
import lombok.Data;

@Data
public class EventoDTO {
	//TODO Mostrare a schermo solo il necessario
	private Long id;
	private LocalDateTime timestamp;
	private Dipendente dipendente;
	private MedicalDay medicalDay;
	private TipologiaEvento tipologiaEvento;
}
