package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class VisitaMedicaCertificatoDTO {

	private DocumentDTO documentDTO;
	private String idCertificato;
	private boolean idoneo;
	private LocalDate rinnovoScadenzaIdoneita;
	private String prescrizione;

}
