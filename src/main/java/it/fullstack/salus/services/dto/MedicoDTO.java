package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.Sesso;
import lombok.Data;

@Data
public class MedicoDTO {
	
	
	private Long id;
	private String nome;
	private String cognome;
	private LocalDate dataNascita;
	private String email;
	private String titoloStudi;
	private String codiceFiscale;
	private String numeroTelefono;
	private Sesso sesso;
	private Boolean societa;
}
