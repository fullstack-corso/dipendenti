package it.fullstack.salus.services.dto;

import it.fullstack.sdk.core.service.filter.LocalDateFilter;
import it.fullstack.sdk.core.service.filter.LongFilter;
import it.fullstack.sdk.core.service.filter.StringFilter;
import lombok.Data;

@Data
public class DipendenteCriteria {
	private LongFilter idSede;
	private StringFilter nome;
	private StringFilter cognome;
	private StringFilter codiceFiscale;
	private Integer over;
	private boolean senzaScadenza;
	//private LocalDateFilter scadenzaVisitaIl;
	private LocalDateFilter scadenzaVisitaAl;
	private LocalDateFilter scadenzaVisitaDal;
}
