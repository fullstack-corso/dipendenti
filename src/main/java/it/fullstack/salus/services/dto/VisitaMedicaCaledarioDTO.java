package it.fullstack.salus.services.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class VisitaMedicaCaledarioDTO {
	
	private String nome;
	
	private String cognome;
	
	private LocalDateTime dataOraVisita;

}
