package it.fullstack.salus.services.dto;



import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateSedeDTO {
	
	@NotNull
	private String denominazione;
	private String via;	
	private String citta;
	private String civico;
	private String cap;
	@NotNull
	private String provincia;
}
