package it.fullstack.salus.services.dto;

import it.fullstack.sdk.core.service.filter.LongFilter;
import lombok.Data;

@Data
public class ContrattoCriteria {
	

	private LongFilter sedeId;
}
