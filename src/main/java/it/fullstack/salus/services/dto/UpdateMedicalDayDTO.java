package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateMedicalDayDTO {
	
	@NotNull
	@Future
	private LocalDate data;
	
	@NotNull
	private long contrattoId;
	
	
}
