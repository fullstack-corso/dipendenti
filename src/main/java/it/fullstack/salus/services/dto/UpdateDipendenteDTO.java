package it.fullstack.salus.services.dto;

import java.time.LocalDate;

import it.fullstack.salus.domain.Distacco;
import it.fullstack.salus.domain.Mansione;
import it.fullstack.salus.domain.Sede;
import it.fullstack.salus.domain.enumeration.Sesso;
import it.fullstack.salus.domain.enumeration.StatoLavorativo;
import it.fullstack.salus.domain.enumeration.TipoContrattoDipendente;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateDipendenteDTO {
	//TODO modificare questa classe tenendo SOLO le cose che POSSONO essere cambiate 
	private String nome;
	private String cognome;
	
	@NotNull
	private LocalDate dataNascita;
	
	private String email;
	private String titoloStudi;
	private String codiceFiscale;
	private String numeroTelefono;
	private Sesso sesso;
	private LocalDate dataPrimoContratto;
	private LocalDate dataAssunzioneDipendente;
	private LocalDate ultimaVisita;
	private StatoLavorativo statoAssunzione;
	private Distacco distacco;
	private Sede sede;
	private Mansione mansione;
	private TipoContrattoDipendente contratto;

	
}
