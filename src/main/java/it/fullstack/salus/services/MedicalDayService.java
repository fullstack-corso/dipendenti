package it.fullstack.salus.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.domain.Contratto;
import it.fullstack.salus.domain.Contratto_;
import it.fullstack.salus.domain.MedicalDay;
import it.fullstack.salus.domain.MedicalDay_;
import it.fullstack.salus.domain.Medico;
import it.fullstack.salus.domain.Medico_;
import it.fullstack.salus.domain.Sede;
import it.fullstack.salus.domain.Sede_;
import it.fullstack.salus.repository.ContrattoRepository;
import it.fullstack.salus.repository.MedicalDayRepository;
import it.fullstack.salus.services.dto.MedicalDayCriteria;
import it.fullstack.salus.services.dto.MedicalDayDTO;
import it.fullstack.salus.services.dto.UpdateMedicalDayDTO;
import it.fullstack.salus.services.mapper.MedicalDayMapper;
import it.fullstack.salus.web.rest.errors.BadRequestException;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.metamodel.SingularAttribute;
import lombok.RequiredArgsConstructor;

import it.fullstack.sdk.core.service.QueryService;
import it.fullstack.sdk.core.service.filter.Filter;

@Service
@RequiredArgsConstructor
public class MedicalDayService extends QueryService<MedicalDay>{

  private final MedicalDayRepository medicalDayRepository;
  private final ContrattoRepository contrattoRepository;

	@Transactional(readOnly = true)
	public Page<MedicalDayDTO> findAll(MedicalDayCriteria criteria,Pageable page){
		return medicalDayRepository
				.findAll(createSpecification(criteria),page)
				.map(MedicalDayMapper::toDTO);
	}
	
	@Transactional
	public MedicalDayDTO create(UpdateMedicalDayDTO updateMedicalDayDTO) {
		
		MedicalDay medicalDayToSave = MedicalDayMapper.toEntity(updateMedicalDayDTO);
		
		Contratto contratto = contrattoRepository
				.findById(updateMedicalDayDTO.getContrattoId())
				.orElseThrow(() -> new BadRequestException("Contratto con id %s non presente sul database", updateMedicalDayDTO.getContrattoId()));	
		
		if(!contratto.isAttivo()) {
			throw new BadRequestException("Contratto con id %s non attivo");
		}
		
		medicalDayToSave.setContratto(contratto);
		
		medicalDayToSave.aggiornamentoStato(); 
		
		MedicalDay savedMedicalDay = medicalDayRepository.save(medicalDayToSave);

		return MedicalDayMapper.toDTO(savedMedicalDay);
	}
	
	public MedicalDayDTO update(Long id,  UpdateMedicalDayDTO updateMedicalDayDTO) {
		
		MedicalDay medicalDayDB = medicalDayRepository
				.findById(id)
				.map((fromDB) -> MedicalDayMapper.mergeEntity(updateMedicalDayDTO, fromDB))
				.orElseThrow(() -> new BadRequestException("MedicalDay non trovato"));
		
		medicalDayDB.setId(id);
		var saved=medicalDayRepository.save(medicalDayDB);
		return MedicalDayMapper.toDTO(saved);
	}
	
		//TODO da verificare il funzionamento
//		@Transactional
//		public MedicalDayDTO createFiltrato(CreateMedicalDayDTO createMedicalDayDTO) {
//			MedicalDay medicalDayToSave = MedicalDayMapper.toEntity(createMedicalDayDTO);
//			
//			Contratto contratto = contrattoRepository
//					.findById(createMedicalDayDTO.getContrattoId())
//					.orElseThrow(() -> new BadRequestException("Contratto non presente sul database"));	
//			
//			if(!contratto.isAttivo()) {
//				throw new BadRequestException("Contratto non attivo");
//			}
//			
//			medicalDayToSave.setContratto(contratto);
//			
//			MedicalDay savedMedicalDay = medicalDayRepository.save(medicalDayToSave);
//
//			DipendenteCriteria criteria = new DipendenteCriteria();
//			criteria.setIdSede(contratto.getSede().getId());
//			criteria.setScadenzaVisitaDal(medicalDayToSave.getData());
//			
//			LocalTime orarioDiInizio = createMedicalDayDTO.getOrarioInizioGiornata();
//			LocalTime orarioDiFine = createMedicalDayDTO.getOrarioInizioGiornata().plusMinutes(createMedicalDayDTO.getDurataMediaVisite());
//			
//			 int minutiTotaliFasciaOraria = getDifferenceInMinutes(orarioDiInizio, orarioDiFine);
//
//			Integer visiteDisponibili = (minutiTotaliFasciaOraria / createMedicalDayDTO.getDurataMediaVisite());
//				
//				
//			Pageable page = PageRequest.of(0, visiteDisponibili,Sort.by(Dipendente_.SCADENZA_VISITA).ascending());
//
//			
//			List<Dipendente> dipendenti = dipendenteService.findAll(criteria, page)
//			.map(DipendenteMapper::toEntity)
//			.toList();
//			
////			dipendenti.sort(Comparator.comparing(Dipendente::getScadenzaVisita));
//			
//	       
//			
//			for(Dipendente dip : dipendenti) {
//				VisitaMedica visitaMedica = new VisitaMedica();
//				visitaMedica.setDipendente(dip);
//				visitaMedica.setMedicalDay(medicalDayToSave);
//				visitaMedica.setOraVisita(orarioDiInizio);
//				visitaMedica.setOraFineVisita(orarioDiInizio.plusMinutes(createMedicalDayDTO.getDurataMediaVisite()));
//				visitaMedica.setStatoVisitaMedica(StatoVisitaMedica.INSERITA);
//				visitaMedicaRepository.save(visitaMedica);
//				orarioDiInizio = orarioDiInizio.plusMinutes(createMedicalDayDTO.getDurataMediaVisite());
//	
//			};
//			
//			
////			
////			if(savedMedicalDay.getVisiteMediche().size() == 0) {
////				savedMedicalDay.setStatoMedicalDay(StatoMedicalDay.IN_PRENOTAZIONE);
////			}
//			
//			savedMedicalDay.setStatoMedicalDay(savedMedicalDay.getVisiteMediche().size() == 0 ? StatoMedicalDay.IN_PRENOTAZIONE : StatoMedicalDay.IN_LAVORAZIONE);			
//		
//
//		
//
//		return MedicalDayMapper.toDTO(savedMedicalDay);
//	}
		

//	public static int getDifferenceInMinutes(LocalTime time1, LocalTime time2) {
//        long diffInMinutes = ChronoUnit.MINUTES.between(time1, time2);
//        return (int) diffInMinutes;
//    }
		
  
  @Transactional(readOnly = true)
  public MedicalDay findById(Long id) {
	  MedicalDay medicalDay = medicalDayRepository
		        .findById(id)
		        .orElseThrow(() -> new BadRequestException("MedicalDay con id %s non presente sul database"));
  
	  return medicalDay;
  }	
  
  public void delete(Long id) {
	  MedicalDay medicalDay = medicalDayRepository
			  .findById(id)
			  .orElseThrow(()-> new BadRequestException("MedicalDay con id %s non presente sul database"));
	  if(medicalDay.hasVisite()) {
			   throw new BadRequestException("Impossibile eliminare il medicalDay con id %s se ci sono visite effettuate o non effettuate al suo interno"); 
	   }
	  
	  medicalDayRepository.deleteById(id);
  }
  
	private Specification<MedicalDay> createSpecification(MedicalDayCriteria criteria){
		Specification<MedicalDay> specification = Specification.where(null);
		if(criteria != null) {
	
			if(criteria.getDataGiorno() !=null) {
	
				specification = specification.and(
						buildRangeSpecification(
								 criteria.getDataGiorno(),
								 MedicalDay_.data)
				);
			}
			
			if(criteria.getIdMedico() !=null) {
				specification = specification.and(
						buildSpecification(
							criteria.getIdMedico(),
							root -> root
							.join(MedicalDay_.contratto, JoinType.LEFT)
							.join(Contratto_.medico, JoinType.LEFT)
							.get(Medico_.id)
						)
				);
			}
	
//			if(criteria.getSedeId() != null) {
//				return specification = specification.and(hasSedeId(criteria.getSedeId()));
//			}
			if(criteria.getIdSede() != null) {
				specification = specification.and(
						buildSpecification(
								criteria.getIdSede(),
								root -> root
								.join(MedicalDay_.contratto, JoinType.LEFT)
								.join(Contratto_.sede, JoinType.LEFT)
								.get(Sede_.id)
						)
				);
			}
			
			if(criteria.getMedicalDayStato() !=null) {
				specification = specification.and(
						buildSpecification(
						criteria.getMedicalDayStato(), MedicalDay_.statoMedicalDay)
				);
			}
			
			
	
		}
		return specification;
	}
	
	//Metodo personalizzato per la Enumeration (nuovo Filtro)
//	private Specification<MedicalDay> buildSpecification(StatoMedicalDayFilter medicalDayStato,
//			String statoMedicalDay) {
//		// TODO Auto-generated method stub
//		return buildSpecification(medicalDayStato, root -> root.get(statoMedicalDay));
//	}
	
	

	private static Specification<MedicalDay> hasMedicoId(Long medicoId) {
		return (root, query, criteriaBuilder) -> {
			Join<Contratto, Medico> medico = root
			    .join(MedicalDay_.CONTRATTO)
					.join(Contratto_.MEDICO);
			return criteriaBuilder.equal(medico.get(Medico_.ID), medicoId);
		};
	}

	private static Specification<MedicalDay> hasSedeId(Long sedeId) {
		return (root, query, criteriaBuilder) -> {
			Join<Contratto, Sede> sedeJoin = root
			    .join(MedicalDay_.CONTRATTO)
					.join(Contratto_.SEDE);
			return criteriaBuilder.equal(sedeJoin.get(Sede_.ID), sedeId);
		};
	}

	

	
}
