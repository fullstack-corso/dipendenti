package it.fullstack.salus.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.salus.domain.Contratto;
import it.fullstack.salus.domain.Dipendente;
import it.fullstack.salus.domain.Dipendente_;
import it.fullstack.salus.domain.Sede;
import it.fullstack.salus.domain.Sede_;
import it.fullstack.salus.repository.DipendenteRepository;
import it.fullstack.salus.services.dto.DipendenteCriteria;
import it.fullstack.salus.services.dto.DipendenteDTO;
import it.fullstack.salus.services.dto.UpdateDipendenteDTO;
import it.fullstack.salus.services.mapper.DipendenteMapper;
import it.fullstack.salus.web.rest.errors.BadRequestException;
import it.fullstack.sdk.core.service.QueryService;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DipendenteService extends QueryService<Dipendente> {

	private final DipendenteRepository repositoryDipendente;

	@Transactional(readOnly = true)
	public Page<DipendenteDTO> findAll(DipendenteCriteria criteria, Pageable page) {
		return repositoryDipendente
				.findAll(createSpecification(criteria), page)
				.map(DipendenteMapper::toDTO);
	}

	@Transactional(readOnly = true)
	public Long count(DipendenteCriteria criteria) {
		return repositoryDipendente
				.count(createSpecification(criteria));
	}

	@Transactional(readOnly = true)
	public List<Dipendente> findAll(DipendenteCriteria criteria){
		//TODO da migliorare (dovrebbe ritornare un DTO)
		return repositoryDipendente.findAll(createSpecification(criteria));
	}

	@Transactional(readOnly = true)
	public DipendenteDTO findById(Long id) {
		return repositoryDipendente
				.findById(id)
				.map(DipendenteMapper::toDTO)
				.orElseThrow(() -> new BadRequestException("Id non valido"));
	}

	@Transactional
	public DipendenteDTO save(UpdateDipendenteDTO updateDto) {
		var dipendenteToSave = DipendenteMapper.toEntity(updateDto);
		dipendenteToSave.setScadenzaVisita(LocalDate.now());
		var savedDipendente = repositoryDipendente.save(dipendenteToSave);
		return DipendenteMapper.toDTO(savedDipendente);
	}

	@Transactional
	public DipendenteDTO update(Long id, UpdateDipendenteDTO dip) {
		Dipendente dipendenteDB = repositoryDipendente
				.findById(id)
				.map((fromDB) -> DipendenteMapper.mergeEntity(dip, fromDB))
				.orElseThrow(() -> new BadRequestException("Dipendente non trovato"));
		var saved = repositoryDipendente.save(dipendenteDB);
		return DipendenteMapper.toDTO(saved);
	}

	@Transactional
	public void delete(Long id) {
		repositoryDipendente.deleteById(id);
	}

	@Transactional(readOnly = true)
	public List<Dipendente> findByCriteria(DipendenteCriteria criteria) {
		return repositoryDipendente
				.findAll(createSpecification(criteria));
	}

	@Transactional
	public void resetScadenzaVisita() {
		repositoryDipendente.resetScadenzaVisita();
	}

	private Specification<Dipendente> createSpecification(DipendenteCriteria criteria) {
		Specification<Dipendente> specification = Specification.where(null);

		if (criteria != null) {

			if (criteria.getIdSede() != null) {
		        specification = specification.and(
		            buildSpecification(
		                criteria.getIdSede(),
		                root -> root.join(Dipendente_.sede, JoinType.LEFT).get(Sede_.id)
		            )
		        );
		      }	

			if (criteria.getNome() != null) {
				specification = specification.and(
						buildStringSpecification(criteria.getNome(), Dipendente_.nome));
			}

			if (criteria.getCognome() != null) {
				specification = specification.and(
						buildStringSpecification(criteria.getCognome(), Dipendente_.cognome));
			}

			if (criteria.getCodiceFiscale() != null) {
				specification = specification.and(
						buildStringSpecification(criteria.getCodiceFiscale(), Dipendente_.codiceFiscale));
			}

			if(criteria.getOver()!=null) {
	  			specification = specification.and(over(criteria.getOver()));
	  		}
			

			if(criteria.getScadenzaVisitaAl()!=null )
			{
				specification = specification.and(
						buildRangeSpecification(criteria.getScadenzaVisitaAl(), Dipendente_.scadenzaVisita));
			}
			
			if(criteria.getScadenzaVisitaDal()!=null )
			{
				specification = specification.and(
						buildRangeSpecification(criteria.getScadenzaVisitaDal(), Dipendente_.scadenzaVisita));
			}

			if(criteria.isSenzaScadenza()) {
				specification = Specification.where((root, query, criteriaBuilder) -> 
				criteriaBuilder.isNull(root.get(Dipendente_.scadenzaVisita)));
			}
		}

		return specification;
	}



	public static Specification<Dipendente> hasSedeId(Long sedeId){
		return (root, query, criteriaBuilder) -> {
			Join<Contratto, Sede> sede = root.join(Dipendente_.SEDE);
			return criteriaBuilder.equal(sede.get(Sede_.ID), sedeId);
		};
	}
	
	
	public static Specification<Dipendente> over(int eta) {
		return (root, query, cb) -> {
			LocalDate anniFa = LocalDate.now().minusYears(eta);
			return  cb.lessThanOrEqualTo(root.get(Dipendente_.DATA_NASCITA), anniFa);
		};	

	}
	

}
