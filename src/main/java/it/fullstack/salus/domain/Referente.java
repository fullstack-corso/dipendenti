package it.fullstack.salus.domain;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.OneToMany;

//FIXME il Referente non è una entity, ma solo un profilo diverso, calcelliamolo @Alex
public class Referente extends Persona {
	private Sede sede;
	@OneToMany(mappedBy = "referente")
	private List<Dipendente> dipendenti = new ArrayList<>();
	private List<VisitaMedica> visite = new ArrayList<>();
	private List<CartellaSanitaria> fascicoliSanitari = new ArrayList<>();
}
