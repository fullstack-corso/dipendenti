package it.fullstack.salus.domain;


import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.StatoLavorativo;
import it.fullstack.salus.domain.enumeration.TipoContrattoDipendente;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
//@Accessors(fluent=true)
@EqualsAndHashCode(callSuper = true)
public class Dipendente extends Persona {

	static final int PERIODO_SCADENZA_OVER50=2;
	static final int PERIODO_SCADENZA_DEFAULT=5;
	static final int ETA_LIMITE=50;

	@NotNull
	private LocalDate dataPrimoContratto;
	@NotNull
	private LocalDate dataAssunzioneDipendente;
	
	private LocalDate scadenzaVisita;
	
	private LocalDate ultimaVisita;

	@ManyToOne
	private Distacco distacco;

	@ManyToOne
	private Sede sede;  

	@ManyToOne
	private Mansione mansione;

	@NotNull
	@Enumerated(EnumType.STRING)
	private StatoLavorativo statoAssunzione;

	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoContrattoDipendente contratto;
	


	// attributi ridondati
	//	private boolean visitaPrenotata;
	//TODO fare lo schedular con questo controllo
	//private boolean idoneitaSanitaria;

	public int getIntervalloScadenza() {
		 return this.getDataNascita().isAfter(LocalDate.now().minusYears(ETA_LIMITE)) ? 
				PERIODO_SCADENZA_DEFAULT:
				PERIODO_SCADENZA_OVER50;
				
	}
	
	public Dipendente scadenzaVisita(LocalDate scadenzaVisita) {
		this.scadenzaVisita = scadenzaVisita;
		return this;
	}
}
