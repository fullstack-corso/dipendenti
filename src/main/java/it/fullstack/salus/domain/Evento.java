package it.fullstack.salus.domain;

import java.time.LocalDateTime;

import it.fullstack.salus.domain.enumeration.TipologiaEvento;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Evento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDateTime timestamp;
	
	@ManyToOne
	private Dipendente dipendente;
	
	@ManyToOne
	private MedicalDay medicalDay;
	
	@Enumerated(EnumType.STRING)
	private TipologiaEvento tipologiaEvento;
	
}
