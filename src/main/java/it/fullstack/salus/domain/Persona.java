package it.fullstack.salus.domain;

import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.Sesso;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@MappedSuperclass
@EqualsAndHashCode(of = "id")
@Table(
    uniqueConstraints = @UniqueConstraint(columnNames = "codiceFiscale")
)
abstract class Persona {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String cognome;
	@NotNull
	private LocalDate dataNascita;
	private String email;

	
	@NotNull
	@Column(unique = true)
	private String codiceFiscale;
	

	@NotNull
	@Enumerated(EnumType.STRING)
	private Sesso sesso;

}
