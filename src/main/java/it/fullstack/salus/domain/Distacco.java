package it.fullstack.salus.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import it.fullstack.salus.domain.enumeration.CausaleAssenza;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;
@Data
@Entity
public class Distacco {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDate dataInizio;
	private LocalDate dataFine;
	@Enumerated()
	private CausaleAssenza causa;
	
	@OneToMany(mappedBy = "distacco")
	private List<Dipendente> dipendente = new ArrayList<>();
	
}
