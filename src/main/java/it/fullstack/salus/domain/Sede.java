package it.fullstack.salus.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
public class Sede {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String denominazione;
	
	private String via;
	private String citta;
	private String civico;
	private String cap;
	@NotNull
	private String provincia;
	
	
	@JsonIgnore
	@OneToMany(mappedBy = "sede")
	private List<Dipendente> dipendenti = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "sede")
	private List<Contratto> contratti = new ArrayList<>();
	
	
	public List<Dipendente> getDipendenti() {
	  return dipendenti;
	}
	//fluent interface
	public Sede id(Long id) {
		this.id = id;
		return this;
	}
	
}
