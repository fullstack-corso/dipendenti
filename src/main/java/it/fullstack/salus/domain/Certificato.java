package it.fullstack.salus.domain;

import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.CausaleAssenza;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.Data;

@Entity
@Data
public class Certificato {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private LocalDate dataInizio;
	
	private LocalDate datafine;
	private CausaleAssenza causale;
	
	private boolean idoneo;
	
	@OneToOne
	private VisitaMedica visitaMedica;
	
}
