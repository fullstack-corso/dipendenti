package it.fullstack.salus.domain;

import java.time.LocalDate;

import it.fullstack.salus.domain.enumeration.CausaleAssenza;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity

public class Assenza {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDate dataInizio;
	private LocalDate dataFine;
	
	@Enumerated(EnumType.STRING)
	private CausaleAssenza causale;
	
}
