package it.fullstack.salus.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import it.fullstack.salus.domain.enumeration.StatoVisitaMedica;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
public class VisitaMedica {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	private Dipendente dipendente;
	private LocalTime oraVisita;

	//Attributi di riepilogo visita medica
	
	//TODO implementare logica di aggiornamento scadenza in caso di malattia/prescrizione
	private String idCertificato;
	private boolean idoneo;
	private LocalDate rinnovoScadenzaIdoneita;
	private String prescrizione;
	
	@ManyToOne
	@NotNull
	private MedicalDay medicalDay;
	
	@Enumerated(EnumType.STRING)
	private StatoVisitaMedica statoVisitaMedica;

	public boolean hasCertificato() {
		return idCertificato!=null;
	}
	
	public LocalDateTime getData() {
		return medicalDay.getData().atTime(oraVisita);
	}
}