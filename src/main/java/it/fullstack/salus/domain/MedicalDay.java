package it.fullstack.salus.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.fullstack.salus.domain.enumeration.StatoMedicalDay;
import it.fullstack.salus.domain.enumeration.StatoVisitaMedica;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Data;


@Data
@Entity
public class MedicalDay {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private LocalDate data;

	@JsonIgnoreProperties({"medicalDay", "dipendente"})
	@OneToMany(mappedBy ="medicalDay", fetch = FetchType.EAGER)
	private List<VisitaMedica> visiteMediche = new ArrayList<>();

	@ManyToOne
	private Contratto contratto;

	@Enumerated(EnumType.STRING)
	private StatoMedicalDay statoMedicalDay;


	private void setStatoMedicalDay(StatoMedicalDay statoMedicalDay) {
		
		this.statoMedicalDay = statoMedicalDay;
	}
	// setta lo stato del medicalDay in automatico 
	public StatoMedicalDay aggiornamentoStato() {

			boolean controlloVisite =
					!this.visiteMediche.isEmpty() 
					&& this.visiteMediche
					.stream()
					.allMatch(visitaMedica -> 
					visitaMedica.getStatoVisitaMedica() != StatoVisitaMedica.INSERITA);

			setStatoMedicalDay(controlloVisite ? StatoMedicalDay.COMPLETO : StatoMedicalDay.IN_LAVORAZIONE);

		return this.statoMedicalDay;
	}

	public Boolean hasVisite() {

		return this.visiteMediche
				.stream()
				.anyMatch(visitaMedica -> 
				visitaMedica.getStatoVisitaMedica() != StatoVisitaMedica.INSERITA);
	}	
}
