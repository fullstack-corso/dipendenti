package it.fullstack.salus.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Medico extends Persona{
	
  	private String numeroTelefono;
	private Boolean societa;

	//TODO per ora non serve la navigazione bidirezionale poichè esosa da gestire
	@JsonIgnoreProperties(value="medico")
	@OneToMany(mappedBy = "medico", fetch = FetchType.EAGER)
	private List<Contratto> contratti = new ArrayList<>();
	
//	@OneToMany(mappedBy = "medico")
//	private List<MedicalDay> visitaMedica = new ArrayList<>();
	
}