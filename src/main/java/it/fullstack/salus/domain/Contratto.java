package it.fullstack.salus.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Data;

@Data
@Entity
public class Contratto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private LocalDate dataInizio;
	private LocalDate dataFine;

	@ManyToOne
	private Medico medico;

	@ManyToOne
	private Sede sede;
	
	@PastOrPresent
	private LocalDateTime dataCancellazione;

	public boolean isAttivo() {
		return this.dataFine.isAfter(LocalDate.now()) && 
				this.dataInizio.isBefore(LocalDate.now()) && 
				this.dataCancellazione == null;
	}
}
