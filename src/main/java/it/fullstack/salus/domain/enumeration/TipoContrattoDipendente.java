package it.fullstack.salus.domain.enumeration;

public enum TipoContrattoDipendente {

	INDETERMINATO,
	DETERMINATO,
	APPRENDISTATO,
	STAGE
	
}
