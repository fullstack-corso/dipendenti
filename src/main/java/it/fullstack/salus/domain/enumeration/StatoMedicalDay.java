package it.fullstack.salus.domain.enumeration;

import java.time.LocalDate;

import it.fullstack.sdk.core.service.filter.RangeFilter;

public enum StatoMedicalDay{
	
	IN_LAVORAZIONE,
	COMPLETO
	
}
