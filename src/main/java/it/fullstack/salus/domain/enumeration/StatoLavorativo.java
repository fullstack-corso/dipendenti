package it.fullstack.salus.domain.enumeration;

public enum StatoLavorativo {

	FERIE,
	ATTIVO,
	DIMESSO,
	CONGEDO_PARENTALE
	
}
