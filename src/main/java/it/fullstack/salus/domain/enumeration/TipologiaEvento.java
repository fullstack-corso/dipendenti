package it.fullstack.salus.domain.enumeration;

public enum TipologiaEvento {

	VISITA_MEDICA_EFFETTUATA("ha eseguito la visita medica"),
	VISITA_MEDICA_ANNULLATA("La visita medica è stata annullata"),
	VISITA_MEDICA_CONCLUSA("La visita medica è stata conclusa"),
	VISITA_MEDICA_NON_EFFETTUATA("non ha effetuato la visita medica"),
	VISITA_MEDICA_SCADUTA("visita medica scaduta"),
	VISITA_MEDICA_PRENOTATA("ha prenotato la visita medica"),
	SCADENZA_VISITA_MODIFICATA("fissata la prossima scadenza per la visita medica per"),
	ELIMINATO_CERTIFICATO_MEDICO("eliminato il certificato medico per"),
	ALLEGATO_CERTIFICATO_MEDICO("allegato il certificato medico per"),
	ASSEGNATO_IL_MEDICALDAY("assegnato il medicalday per");
	

	private final String stato;
	

	private TipologiaEvento(String stato) {
		this.stato = stato;
		
	}

	public String getStato() {
		return stato;
	}

}
