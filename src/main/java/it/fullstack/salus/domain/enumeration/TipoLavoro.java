package it.fullstack.salus.domain.enumeration;

public enum TipoLavoro {

	DIPENDENTE,
	AUTONOMO
}
