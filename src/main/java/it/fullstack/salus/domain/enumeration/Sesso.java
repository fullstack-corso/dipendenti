package it.fullstack.salus.domain.enumeration;

public enum Sesso {

	UOMO,
	DONNA,
	ALTRO
	
}
