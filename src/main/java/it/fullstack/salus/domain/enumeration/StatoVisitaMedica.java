package it.fullstack.salus.domain.enumeration;

public enum StatoVisitaMedica {
	
	INSERITA,
	EFFETTUATA,
	NON_EFFETTUATA
}
