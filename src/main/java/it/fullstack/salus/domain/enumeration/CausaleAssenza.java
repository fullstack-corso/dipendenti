package it.fullstack.salus.domain.enumeration;


public enum CausaleAssenza {

	 INFORTUNIO,
	 MALATTIA,
	 MATERNITAANTICIPATA,
	 MATERNITAFACOLTATIVA,
	 MATERNITAOBBLIGATORIA,
	 TERAPIASALVAVITA,
	 CONGEDO

}
