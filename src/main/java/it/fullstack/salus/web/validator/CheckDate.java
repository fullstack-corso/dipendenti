package it.fullstack.salus.web.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.fullstack.salus.services.dto.UpdateContrattoDTO;
import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;


@Constraint (validatedBy = {CheckDate.CheckDateValidator.class})  //implementazione-->interrogherà CheckDateValidator per effettuare la validazione
@Target (ElementType.TYPE)							  //specifica che tipo di annotazione (livello di Classe,Metodo o variabile)
@Retention (RetentionPolicy.RUNTIME)				  //quando scaturire l'annotazione (tipicamente runtime)
public @interface CheckDate  {

	String message() default "Data di Fine contratto deve essere dopo la Data di Inizio Contratto";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
	class CheckDateValidator implements ConstraintValidator<CheckDate, UpdateContrattoDTO>{

		@Override
		public boolean isValid(UpdateContrattoDTO value, ConstraintValidatorContext context) {
			return value.getDataInizio().isBefore(value.getDataFine());
		}	
	}
	
	

}


