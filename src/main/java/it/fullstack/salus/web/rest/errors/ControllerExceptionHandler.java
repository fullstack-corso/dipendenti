package it.fullstack.salus.web.rest.errors;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import jakarta.persistence.EntityNotFoundException;

@ControllerAdvice
@ResponseBody
public class ControllerExceptionHandler {

  @ExceptionHandler(BadRequestException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<ErrorMessage> handlerBadRequestException(BadRequestException ex, WebRequest wr) {
    ErrorMessage msg = new ErrorMessage(
        HttpStatus.BAD_REQUEST.value(), 
        ex.getMessage(), 
        new Date(),
        wr.getDescription(true)
    );
    return new ResponseEntity<ErrorMessage>(msg, HttpStatus.BAD_REQUEST);
 
  }
  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<ErrorMessage> handlerEntityNotFoundException(EntityNotFoundException ex,WebRequest wr){
	  ErrorMessage msg = new ErrorMessage(HttpStatus.NOT_FOUND.value(),
			  ex.getMessage(),
			  new Date(),
			  wr.getDescription(true)
		);
		return new ResponseEntity<ErrorMessage>(msg,HttpStatus.NOT_FOUND);
  }
}
