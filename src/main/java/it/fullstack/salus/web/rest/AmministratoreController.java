package it.fullstack.salus.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.salus.services.DipendenteScheduler;
import it.fullstack.salus.services.DipendenteService;



@RestController
@RequestMapping("/api/admin/")
public class AmministratoreController {

	@Autowired
	DipendenteService service;
	
	@Autowired
	DipendenteScheduler dipendenteScheduler;

	@PostMapping("/resetScadenza")
	public ResponseEntity<String> resetScadenzaVisita(){
		 service.resetScadenzaVisita();
		 dipendenteScheduler.controlloScadenzeVisita();

		return ResponseEntity.ok("Date di scadenza reimpostate con successo per tutti i dipendenti.");

	}
}
