package it.fullstack.salus.web.rest;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import it.fullstack.salus.services.dto.DocumentDTO;
import jakarta.validation.Valid;


@FeignClient(value = "jplaceholder", url = "http://localhost:8082/")
public interface DocumentClient {


	@RequestMapping(method = RequestMethod.GET, value = "api/documenti/{id}", produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
	ResponseEntity<Resource> downloadDocument(@PathVariable final String id);

	@Valid
	@RequestMapping(method = RequestMethod.POST, value = "api/documenti", consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
	ResponseEntity<DocumentDTO> insertDocument(@RequestPart("media")  final MultipartFile media);
	
	@Valid
	@DeleteMapping(value = "api/documenti/{id}")
	ResponseEntity<Void> delete(@PathVariable("id") String id);
}
