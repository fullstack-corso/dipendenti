package it.fullstack.salus.web.rest;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.salus.services.DipendenteService;
import it.fullstack.salus.services.dto.DipendenteCriteria;
import it.fullstack.salus.services.dto.DipendenteDTO;
import it.fullstack.salus.services.dto.UpdateDipendenteDTO;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/dipendente")
public class DipendenteController {
	
	@Autowired
	private DipendenteService serviceDipendente;
	
	@GetMapping
	public ResponseEntity<Page<DipendenteDTO>> findAll(DipendenteCriteria criteria, Pageable page){
		return ResponseEntity
		    .ok(serviceDipendente.findAll(criteria, page));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<DipendenteDTO> findById(@PathVariable Long id){
		return ResponseEntity
		    .ok(serviceDipendente.findById(id));
	}
	
	@GetMapping("/count")
	public ResponseEntity<Long> count(DipendenteCriteria criteria){
		return ResponseEntity
				.ok(serviceDipendente.count(criteria));
	}
	
	
	@PostMapping
	public ResponseEntity<DipendenteDTO> create(@Valid @RequestBody UpdateDipendenteDTO dipendente){
		return ResponseEntity
		    .ok(serviceDipendente.save(dipendente));
	}
	
	@PutMapping("{id}")
	public ResponseEntity<DipendenteDTO> update(@PathVariable Long id, @Valid @RequestBody UpdateDipendenteDTO dipendente){
		return ResponseEntity
		    .ok(serviceDipendente.update(id, dipendente));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		serviceDipendente.delete(id);
		
		return ResponseEntity
		    .ok()
		    .build();
	}
	
}
