package it.fullstack.salus.web.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.salus.services.EventoService;
import it.fullstack.salus.services.dto.EventoCriteria;
import it.fullstack.salus.services.dto.EventoDTO;

@RestController
@RequestMapping("/api/evento")
public class EventoController {

	@Autowired
	private EventoService service;
	
	@GetMapping
	public ResponseEntity<Page<EventoDTO>> findAll(EventoCriteria criteria,Pageable page){
		return ResponseEntity.ok(service.findAll(criteria,page));
		
	}
}
