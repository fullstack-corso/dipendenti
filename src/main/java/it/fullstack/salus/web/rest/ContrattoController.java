package it.fullstack.salus.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.salus.services.ContrattoService;
import it.fullstack.salus.services.dto.ContrattoCriteria;
import it.fullstack.salus.services.dto.ContrattoDTO;
import it.fullstack.salus.services.dto.UpdateContrattoDTO;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/contratto/")
public class ContrattoController {
											
	@Autowired
	ContrattoService service;

	@GetMapping
	public ResponseEntity<Page<ContrattoDTO>> findAll(ContrattoCriteria criteria,Pageable pageable) {
		return	ResponseEntity.ok(service.findAll(criteria,pageable));
	}

	
	@GetMapping("{id}")
	public ResponseEntity<ContrattoDTO> findOne(@PathVariable Long id) {		
		return ResponseEntity.ok(service.findById(id));
	}

	@PostMapping
	public ResponseEntity<ContrattoDTO> create(@Valid @RequestBody UpdateContrattoDTO contratto) {		
		return ResponseEntity
		    .ok(service.save(contratto));
	}

	@PutMapping("{id}")
	public ResponseEntity<ContrattoDTO> update(@PathVariable Long id, @Valid @RequestBody UpdateContrattoDTO contratto) {	
		return ResponseEntity
		    .ok(service.update(id,contratto));
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
	  service.deleteById(id);		
	  
		return ResponseEntity
		    .ok()
		    .build();
	}

}
