package it.fullstack.salus.web.rest;

import java.io.IOException;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.fullstack.salus.services.VisitaMedicaService;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaCertificatoDTO;
import it.fullstack.salus.services.dto.UpdateVisitaMedicaDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCaledarioDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCertificatoDTO;
import it.fullstack.salus.services.dto.VisitaMedicaCriteria;
import it.fullstack.salus.services.dto.VisitaMedicaDTO;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/visita-medica")
public class VisitaMedicaController {

	@Autowired
	VisitaMedicaService visiteMedicheService;

	@Autowired
	DocumentClient doucmentClient;

	@PostMapping
	public ResponseEntity<VisitaMedicaDTO> createVisitaMedica(@Valid @RequestBody UpdateVisitaMedicaDTO visitaMedicaDTO){
		return ResponseEntity.ok(visiteMedicheService.create(visitaMedicaDTO));
	}

	@GetMapping("calendario")
	public ResponseEntity<Page<VisitaMedicaCaledarioDTO>> findAllCalendario(VisitaMedicaCriteria criteria, Pageable page){
		return ResponseEntity
				.ok(visiteMedicheService.findAllCalendario(criteria, page));
	}

	@GetMapping
	public ResponseEntity<Page<VisitaMedicaDTO>> findAll(VisitaMedicaCriteria criteria, Pageable page){
		return ResponseEntity
				.ok(visiteMedicheService.findAll(criteria, page));
	}

	@PutMapping("{id}")
	public ResponseEntity<VisitaMedicaDTO> update(@Valid @RequestBody UpdateVisitaMedicaDTO updateVisitaMedicaDTO, @PathVariable Long id) {
		return ResponseEntity.ok(visiteMedicheService.update(updateVisitaMedicaDTO, id));
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteById(@PathVariable Long id){
		visiteMedicheService.deleteById(id);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/visite/{visitaId}/certificato")
	public ResponseEntity<VisitaMedicaCertificatoDTO> uploadCertificato(
			@RequestParam("file") final MultipartFile file,
			@RequestParam boolean idoneo,
			@RequestParam LocalDate rinnovoScadenzaIdoneita,
			@RequestParam String prescrizione,
			@PathVariable final long visitaId
			) throws IOException {
		if (!file.isEmpty()) {
			//FIXME non mi convince, c'è un modo migliore per gestirlo?
			UpdateVisitaMedicaCertificatoDTO dto = new UpdateVisitaMedicaCertificatoDTO();
			dto.setIdoneo(idoneo);
			dto.setPrescrizione(prescrizione);
			dto.setRinnovoScadenzaIdoneita(rinnovoScadenzaIdoneita);
			return ResponseEntity.ok(visiteMedicheService.insertDocument(dto,visitaId,file));
		}
		return ResponseEntity
				.badRequest()
				.build();
	}

	@GetMapping("/visite/{visitaId}/certificato")
	public ResponseEntity<Resource> downloadDocument(@PathVariable final Long visitaId)  {		 
		return visiteMedicheService.downloadDocument(visitaId);
	}

	
	@DeleteMapping("/visite/{visitaId}/certificato")
	public ResponseEntity<String> deleteCertificati(@PathVariable final Long visitaId )  {
		visiteMedicheService.deleteCertificato(visitaId);
		return ResponseEntity.ok("Certificato eliminato");
	}
}