package it.fullstack.salus.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.salus.services.MedicoService;
import it.fullstack.salus.services.dto.MedicoCriteria;
import it.fullstack.salus.services.dto.MedicoDTO;
import it.fullstack.salus.services.dto.UpdateMedicoDTO;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/medico")
public class MedicoController {
	
	@Autowired
	MedicoService service;
	
	@GetMapping
	public ResponseEntity<Page<MedicoDTO>> findAll(MedicoCriteria criteria, Pageable page) {
		return ResponseEntity.ok(service.findAll(criteria, page));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<MedicoDTO> findById(@PathVariable Long id) {
		return ResponseEntity.ok(service.findById(id));
	}
	
	@PostMapping
	public ResponseEntity<MedicoDTO> save(@Valid @RequestBody UpdateMedicoDTO updateMedicoDTO) {
		return ResponseEntity.ok(service.save(updateMedicoDTO));
	}
	
	@PutMapping("{id}")
	public ResponseEntity<MedicoDTO> update(@Valid @RequestBody UpdateMedicoDTO updateMedicoDTO, @PathVariable Long id) {
		return ResponseEntity.ok(service.update(updateMedicoDTO, id));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteById(@PathVariable Long id) {
		service.deleteById(id);
		return ResponseEntity.ok().build();
	} 
}