package it.fullstack.salus.web.rest;

import static it.fullstack.salus.services.mapper.SedeMapper.toDTO;
import static it.fullstack.salus.services.mapper.SedeMapper.toEntity;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.salus.repository.SedeRepository;
import it.fullstack.salus.services.dto.SedeDTO;
import it.fullstack.salus.services.dto.UpdateSedeDTO;
import it.fullstack.salus.services.mapper.SedeMapper;


import it.fullstack.salus.web.rest.errors.BadRequestException;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/sede")
public class SedeController {

	@Autowired
	private SedeRepository repository;


	@GetMapping
	@Transactional(readOnly = true)
	public ResponseEntity<List<SedeDTO>> findAll() {
		return ResponseEntity
				.ok(repository.findAll()
						.stream()
						.map(SedeMapper::toDTO)
						.collect(Collectors.toList())
				);
	}

	@GetMapping("{id}")
	@Transactional(readOnly = true)
	public ResponseEntity<SedeDTO>findOne(@PathVariable Long id) {
		return ResponseEntity
				.of(repository.findById(id)
						.map(SedeMapper::toDTO));

	}

	@PostMapping
	public ResponseEntity<SedeDTO> create(@Valid @RequestBody UpdateSedeDTO sedeToCreate) {
		var sedeToSave = toEntity(sedeToCreate);
		var savedSede = repository.save(sedeToSave);
		
		return ResponseEntity
				.ok(toDTO(savedSede));
	}

	@PutMapping("{id}")
	public ResponseEntity<SedeDTO> update(@PathVariable Long id, @Valid @RequestBody UpdateSedeDTO sede) {
		var sedeToupdate = toEntity(sede).id(id);
		
		
		// Verifichiamo se già esiste 
		if(!repository.existsById(sedeToupdate.getId())) throw new BadRequestException("La sede non esiste");
		
		var updatedSede = repository.save(sedeToupdate);
		return ResponseEntity
				.ok(toDTO(updatedSede));
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		repository.deleteById(id);
		return ResponseEntity
				.ok()
				.build();
	}

}
