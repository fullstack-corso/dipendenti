package it.fullstack.salus.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.salus.domain.MedicalDay;
import it.fullstack.salus.services.MedicalDayService;
import it.fullstack.salus.services.dto.MedicalDayCriteria;
import it.fullstack.salus.services.dto.MedicalDayDTO;
import it.fullstack.salus.services.dto.UpdateMedicalDayDTO;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/medical-day")
public class MedicaDayController {
	
	@Autowired
	private MedicalDayService service;
	
	@GetMapping
	public ResponseEntity<Page<MedicalDayDTO>> findAll(MedicalDayCriteria criteria, Pageable page){
		return ResponseEntity
		    .ok(service.findAll(criteria, page));
	}
	
	@PostMapping
	public ResponseEntity<MedicalDayDTO> create(@Valid @RequestBody UpdateMedicalDayDTO updateMedicalDayDTO) {
		return ResponseEntity
				.ok(service.create(updateMedicalDayDTO));
	}
	
//	@PostMapping("/filtrato")
//	public ResponseEntity<MedicalDayDTO> createFiltrato(@Valid @RequestBody CreateMedicalDayDTO createMedicalDayDTO) {
//		return ResponseEntity
//				.ok(service.createFiltrato(createMedicalDayDTO));
//	}
	

	@PutMapping("{id}")
	public ResponseEntity<MedicalDayDTO> update(@PathVariable Long id,@Valid @RequestBody UpdateMedicalDayDTO updateMedicalDayDTO) {
		return ResponseEntity
				.ok(service.update(id, updateMedicalDayDTO));
	}
	
	 @GetMapping("{id}")
	  public ResponseEntity<MedicalDay> findById(@PathVariable Long id) {
	    return ResponseEntity
	        .ok(service.findById(id));
	  }
	 
	 @DeleteMapping("{id}")
		public ResponseEntity<Void> delete(@PathVariable Long id){
			service.delete(id);
			
			return ResponseEntity
			    .ok()
			    .build();
		}
	
}
