package it.fullstack.salus.web.rest.errors;


import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//quando si verifica un  errore qui gli mandiamo il messaggio di errore
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage  {

	private int statusCode;
	private String message;
	private Date timestamp;
	private String description;
	
}