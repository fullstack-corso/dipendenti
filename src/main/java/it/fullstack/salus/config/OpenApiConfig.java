package it.fullstack.salus.config;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
public class OpenApiConfig implements WebMvcConfigurer {

    @Value("${fullstack.openapi.dev-url}")
    private String devUrl;

    @Value("${fullstack.openapi.prod-url}")
    private String prodUrl;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://127.0.0.1:8082") // Inserisci qui l'origine consentita
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowedHeaders("*")
                .allowCredentials(true)
                .maxAge(3600);
    }

    @Bean
    public OpenAPI SalusAPI() {
        Server devServer = new Server();
        devServer.setUrl(devUrl);
        devServer.setDescription("Server URL in Development environment");

        Server prodServer = new Server();
        prodServer.setUrl(prodUrl);
        prodServer.setDescription("Server URL in Production environment");

        Contact contact = new Contact();
        contact.setName("Alessandro Giordano");
        contact.setEmail("alessandro-giordano@fullstack-group.it");
        contact.setUrl("https://www.salus.com");

        Info info = new Info()
                .title("Salus Management API")
                .version("1.0")
                .contact(contact)
                .description("This API exposes endpoints to manage Salus' Entities")
                .termsOfService("https://www.salus.com");

        return new OpenAPI().info(info).servers(List.of(prodServer, devServer));
    }
}