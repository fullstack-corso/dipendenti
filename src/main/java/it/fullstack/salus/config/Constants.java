package it.fullstack.salus.config;

import org.springframework.stereotype.Component;

@Component
public class Constants {
	
	  public final static Integer PERIODO_SCADENZA_OVER50 = 2;

	  public final static Integer PERIODO_SCADENZA_DEFAULT = 5;

	  public final static Integer ETA_LIMITE = 50;

}
